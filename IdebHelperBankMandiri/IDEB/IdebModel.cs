using System.Collections.Generic;
using System;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Serialization;
using System.Reflection;

namespace IdebHelper
{
    public class GetOnlyJsonPropertyAttribute : Attribute
    {
    }

    public class CustomContractResolver : DefaultContractResolver
    {

        public bool UseJsonPropertyName { get; }

        public CustomContractResolver(bool useJsonPropertyName)
        {
            UseJsonPropertyName = useJsonPropertyName;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            if (!UseJsonPropertyName)
            {
                property.PropertyName = property.UnderlyingName;
            }

            if (property != null)
            {
                var attributes = property.AttributeProvider.GetAttributes(typeof(GetOnlyJsonPropertyAttribute), true);
                if (attributes != null && attributes.Count > 0)
                {
                    property.ShouldDeserialize = (a) => true;
                    property.ShouldSerialize = (a) => false;
                }
            }


            return property;
        }
    }

    public class JsonDateTime : IsoDateTimeConverter
    {
        public JsonDateTime()
        {
            base.DateTimeFormat = "yyyyMMddHHmmss";
        }
    }

    public class JsonDate : IsoDateTimeConverter
    {
        public JsonDate()
        {
            base.DateTimeFormat = "yyyyMMdd";
        }
    }

    public class Header
    {
        [JsonProperty("userReferenceCode")]
        public string kodeReferensiPengguna { get; set; }
        [JsonProperty("resultDate")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalHasil { get; set; }
        [JsonProperty("inquiryId")]
        public int idPermintaan { get; set; }
        [JsonProperty("inquiryUserId")]
        public int idPenggunaPermintaan { get; set; }
        [JsonProperty("inquiryCreatedBy")]
        public string dibuatOleh { get; set; }
        [JsonProperty("inquiryMemberCode")]
        public string kodeLJKPermintaan { get; set; }
        [JsonProperty("inquiryOfficeCode")]
        public string kodeCabangPermintaan { get; set; }
        [JsonProperty("reportRequestPurposeCode")]
        public string kodeTujuanPermintaan { get; set; }
        [JsonProperty("inquiryDate")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalPermintaan { get; set; }
        [JsonProperty("dataSetTotal")]
        public int totalBagian { get; set; }
        [JsonProperty("dataSetNumber")]
        public int nomorBagian { get; set; }
        [JsonIgnore]
        public bool dataSetTotalMoreThanOne { get; set; }
    }

    public class IndividualKeyword
    {
        [JsonProperty("identityNumberName")]
        public string namaDebitur { get; set; }
        [JsonProperty("identityNumber")]
        public string noIdentitas { get; set; }
        [JsonProperty("gender")]
        public string jenisKelamin { get; set; }
        [JsonProperty("genderDesc")]
        public string jenisKelaminKet { get; set; }
        [JsonProperty("taxId")]
        public string npwp { get; set; }
        [JsonProperty("birthPlace")]
        public string tempatLahir { get; set; }
        [JsonProperty("birthDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalLahir { get; set; }
        [JsonIgnore]
        public string recordStatusFlag { get; set; }
    }

    public class IndividualDebtor : BaseDebtor
    {
        [JsonProperty("identityNumberType")]
        public string identitas { get; set; }
        [JsonProperty("identityNumberTypeDesc")]
        public string identitasKet { get; set; }
        [JsonProperty("identityNumber")]
        public string noIdentitas { get; set; }
        [JsonProperty("gender")]
        public string jenisKelamin { get; set; }
        [JsonProperty("genderDesc")]
        public string jenisKelaminKet { get; set; }
        [JsonProperty("birthPlace")]
        public string tempatLahir { get; set; }
        [JsonProperty("birthDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalLahir { get; set; }
        [JsonProperty("occupation")]
        public string pekerjaan { get; set; }
        [JsonProperty("occupationDesc")]
        public string pekerjaanKet { get; set; }
        [JsonProperty("workCompanyName")]
        public string tempatBekerja { get; set; }
        [JsonProperty("economicSector")]
        public string bidangUsaha { get; set; }
        [JsonProperty("economicSectorDesc")]
        public string bidangUsahaKet { get; set; }
        [JsonProperty("academicDegree")]
        public string kodeGelarDebitur { get; set; }
        [JsonProperty("academicDegreeDesc")]
        public string statusGelarDebitur { get; set; }
    }

    public class CorporateKeyWord
    {
        [JsonProperty("identityNumberName")]
        public string namaBadanUsaha { get; set; }
        [JsonProperty("taxId")]
        public string npwp { get; set; }
        [JsonProperty("estPlace")]
        public string tempatPendirian { get; set; }
        [JsonProperty("estCertDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAktaPendirian { get; set; }
        [JsonIgnore]
        public string recordStatusFlag { get; set; }
    }

    public class CorporateDebtor : BaseDebtor
    {
        [JsonProperty("fullName")]
        public string namaLengkap { get; set; }
        //[JsonProperty("taxId")]
        //public string npwp { get; set; }
        [JsonProperty("companyType")]
        public string bentukBu { get; set; }
        [JsonProperty("companyTypeDesc")]
        public string bentukBuKet { get; set; }
        [JsonProperty("goPublicFlag")]
        public string goPublic { get; set; }
        [JsonProperty("estPlace")]
        public string tempatPendirian { get; set; }
        [JsonProperty("estCertNo")]
        public string noAktaPendirian { get; set; }
        [JsonProperty("estCertDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tglAktaPendirian { get; set; }
        [JsonProperty("latestAddCertNo")]
        public string noAktaTerakhir { get; set; }
        [JsonProperty("latestAddCertDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tglAktaTerakhir { get; set; }
        [JsonProperty("economicSector")]
        public string sektorEkonomi { get; set; }
        [JsonProperty("economicSectorDesc")]
        public string sektorEkonomiKet { get; set; }
        [JsonProperty("ratingAgency")]
        public string pemeringkat { get; set; }
        [JsonProperty("ratingAgencyDesc")]
        public string pemeringkatKet { get; set; }
        [JsonProperty("rating")]
        public string peringkat { get; set; }
        [JsonProperty("ratingDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalPemeringkatan { get; set; }
    }

    public class OffclsSharehldr
    {
        [JsonProperty("identityNumberName")]
        public string namaSesuaiIdentitas { get; set; }
        [JsonProperty("identityNumber")]
        public string nomorIdentitas { get; set; }
        [JsonProperty("gender")]
        public string kodeJenisKelamin { get; set; }
        [JsonProperty("genderDesc")]
        public string jenisKelamin { get; set; }
        [JsonProperty("jobPosition")]
        public string kodePosisiPekerjaan { get; set; }
        [JsonProperty("jobPositionDesc")]
        public string posisiPekerjaan { get; set; }
        [JsonProperty("shareOwnership")]
        public double? prosentaseKepemilikan { get; set; }
        [JsonProperty("address")]
        public string alamat { get; set; }
        [JsonProperty("district")]
        public string kecamatan { get; set; }
        [JsonProperty("city")]
        public string kodeKota { get; set; }
        [JsonProperty("cityDesc")]
        public string kota { get; set; }
        [JsonProperty("subDistrict")]
        public string kelurahan { get; set; }
        [JsonProperty("shareholderStatus")]
        public string kodeStatusPengurusPemilik { get; set; }
        [JsonProperty("shareholderStatusDesc")]
        public string statusPengurusPemilik { get; set; }
    }

    public class OffclsSharehldrGroup
    {
        [JsonProperty("member")]
        public string kodeLJK { get; set; }
        [JsonProperty("memberDesc")]
        public string namaLJK { get; set; }
        [JsonProperty("updatedDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalUpdate { get; set; }
        [JsonProperty("offclsSharehldrs")]
        public List<OffclsSharehldr> pengurusPemilik { get; set; }
    }

    public class FacilitySummary
    {
        [JsonIgnore]
        public double? originalCreditLimit { get; set; }
        [JsonIgnore]
        public double? originalLcLimit { get; set; }
        [JsonIgnore]
        public double? originalBgLimit { get; set; }
        [JsonIgnore]
        public double? originalSecLimit { get; set; }
        [JsonIgnore]
        public double? originalOtherLimit { get; set; }
        [JsonIgnore]
        public double? originalTotalLimit { get; set; }
        [JsonProperty("currentCreditLimit")]
        public double? plafonEfektifKreditPembiayaan { get; set; }
        [JsonProperty("currentLcLimit")]
        public double? plafonEfektifLc { get; set; }
        [JsonProperty("currentBgLimit")]
        public double? plafonEfektifGyd { get; set; }
        [JsonProperty("currentSecLimit")]
        public double? plafonEfektifSec { get; set; }
        [JsonProperty("currentOtherLimit")]
        public double? plafonEfektifLain { get; set; }
        [JsonProperty("currentTotalLimit")]
        public double? plafonEfektifTotal { get; set; }
        [JsonProperty("outstandingPrincipalCredit")]
        public double? bakiDebetKreditPembiayaan { get; set; }
        [JsonProperty("outstandingPrincipalLc")]
        public double? bakiDebetLc { get; set; }
        [JsonProperty("outstandingPrincipalBg")]
        public double? bakiDebetGyd { get; set; }
        [JsonProperty("outstandingPrincipalSec")]
        public double? bakiDebetSec { get; set; }
        [JsonProperty("outstandingPrincipalOther")]
        public double? bakiDebetLain { get; set; }
        [JsonProperty("outstandingPrincipalTotal")]
        public double? bakiDebetTotal { get; set; }
        [JsonProperty("numberOfBank")]
        public int krediturBankUmum { get; set; }
        [JsonProperty("numberOfRuralBank")]
        public int krediturBPRS { get; set; }
        [JsonProperty("numberOfFinancingCompany")]
        public int krediturLp { get; set; }
        [JsonProperty("numberOfOtherCreditor")]
        public int krediturLainnya { get; set; }
        [JsonProperty("worstCollectibilityColCode")]
        public string kualitasTerburuk { get; set; }
        [JsonProperty("worstCollectibilityYearMonth")]
        public string kualitasBulanDataTerburuk { get; set; }
    }

    public class Collateral
    {
        [JsonProperty("collateralTypeDesc")]
        public string jenisAgunanKet { get; set; }
        [JsonProperty("collateralValueMember")]
        public double? nilaiAgunanMenurutLJK { get; set; }
        [JsonProperty("sharedCollPercentage")]
        public double? prosentaseParipasu { get; set; }
        [JsonProperty("updatedDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalUpdate { get; set; }
        [JsonProperty("collateralRegNo")]
        public string nomorAgunan { get; set; }
        [JsonProperty("contractType")]
        public string jenisPengikatan { get; set; }
        [JsonProperty("contractTypeDesc")]
        public string jenisPengikatanKet { get; set; }
        [JsonProperty("contractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalPengikatan { get; set; }
        [JsonProperty("collateralOwnerName")]
        public string namaPemilikAgunan { get; set; }
        [JsonProperty("collateralAddress")]
        public string alamatAgunan { get; set; }
        [JsonProperty("collateralCity")]
        public string kabKotaLokasiAgunan { get; set; }
        [JsonProperty("collateralCityDesc")]
        public string kabKotaLokasiAgunanKet { get; set; }
        [JsonProperty("collateralAppraisalDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalPenilaianPenilaiIndependen { get; set; }
        [JsonProperty("collateralRating")]
        public string peringkatAgunan { get; set; }
        [JsonProperty("ratingAgency")]
        public string kodeLembagaPemeringkat { get; set; }
        [JsonProperty("ratingAgencyDesc")]
        public string lembagaPemeringkat { get; set; }
        [JsonProperty("ownershipDocument")]
        public string buktiKepemilikan { get; set; }
        [JsonProperty("collateralValueTax")]
        public double? nilaiAgunanNjop { get; set; }
        [JsonProperty("collateralValueAppr")]
        public double? nilaiAgunanIndep { get; set; }
        [JsonProperty("collateralAppraiserName")]
        public string namaPenilaiIndep { get; set; }
        [JsonProperty("isInsuredFlag")]
        public string asuransi { get; set; }
        [JsonProperty("description")]
        public string keterangan { get; set; }
        [JsonProperty("memberAppraisalDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tglPenilaianPelapor { get; set; }

        [JsonProperty("createdDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalDibentuk { get; set; }

        [JsonProperty("collateralType")]
        public string jenisAgunan { get; set; }
    }

    public class Guarantor
    {
        [JsonProperty("guarantorName")]
        public string namaPenjamin { get; set; }
        [JsonProperty("identityNumber")]
        public string nomorIdentitas { get; set; }
        [JsonProperty("updatedDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalUpdate { get; set; }
        [JsonProperty("createdDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalBuat { get; set; }
        [JsonProperty("guarantorType")]
        public string kodeJenisPenjamin { get; set; }
        [JsonProperty("guarantorTypeDesc")]
        public string keteranganJenisPenjamin { get; set; }
        [JsonProperty("guarantorAddress")]
        public string alamatPenjamin { get; set; }
        [JsonProperty("description")]
        public string keterangan { get; set; }
    }

    public class Credit : BaseFacility
    {
        [JsonProperty("accountNo")]
        public string noRekening { get; set; }
        [JsonProperty("creditLimitUsage")]
        public double? realisasiBulanBerjalan { get; set; }
        [JsonProperty("creditNature")]
        public string sifatKreditPembiayaan { get; set; }
        [JsonProperty("creditNatureDesc")]
        public string sifatKreditPembiayaanKet { get; set; }
        [JsonProperty("creditStartDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAwalKredit { get; set; }
        [JsonProperty("creditType")]
        public string jenisKreditPembiayaan { get; set; }
        [JsonProperty("creditTypeDesc")]
        public string jenisKreditPembiayaanKet { get; set; }
        [JsonProperty("creditUsage")]
        public string jenisPenggunaan { get; set; }
        [JsonProperty("creditUsageDesc")]
        public string jenisPenggunaanKet { get; set; }
        [JsonProperty("currentCreditLimit")]
        public double? plafon { get; set; }
        [JsonProperty("debtorCategory")]
        public string kategoriDebiturKode { get; set; }
        [JsonProperty("debtorCategoryDesc")]
        public string kategoriDebiturKet { get; set; }
        [JsonProperty("dueDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalJatuhTempo { get; set; }
        [JsonProperty("economicSector")]
        public string sektorEkonomi { get; set; }
        [JsonProperty("economicSectorDesc")]
        public string sektorEkonomiKet { get; set; }
        [JsonProperty("financingScheme")]
        public string akadKreditPembiayaan { get; set; }
        [JsonProperty("financingSchemeDesc")]
        public string akadKreditPembiayaanKet { get; set; }
        [JsonProperty("governmentProgramFlag")]
        public string kreditProgramPemerintah { get; set; }
        [JsonProperty("governmentProgramFlagDesc")]
        public string kreditProgramPemerintahKet { get; set; }
        [JsonProperty("initialContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAwal { get; set; }
        [JsonProperty("initialContractNo")]
        public string noAkadAwal { get; set; }
        [JsonProperty("interestRate")]
        public double? sukuBungaImbalan { get; set; }
        [JsonProperty("interestRateType")]
        public string jenisSukuBungaImbalan { get; set; }
        [JsonProperty("interestRateTypeDesc")]
        public string jenisSukuBungaImbalanKet { get; set; }
        [JsonProperty("isNewFlag")]
        public string frekPerpjganKreditPembiayaan { get; set; }
        [JsonProperty("lastContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAkhir { get; set; }
        [JsonProperty("lastContractNo")]
        public string noAkadAkhir { get; set; }
        [JsonProperty("lateCharges")]
        public double? denda { get; set; }
        [JsonProperty("nonPerformingDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalMacet { get; set; }
        [JsonProperty("nonPerformingReason")]
        public string kodeSebabMacet { get; set; }
        [JsonProperty("nonPerformingReasonDesc")]
        public string sebabMacetKet { get; set; }
        [JsonProperty("oriCurrencyAmount")]
        public double? nilaiDalamMataUangAsal { get; set; }
        [JsonProperty("originalCreditLimit")]
        public double? plafonAwal { get; set; }
        [JsonProperty("outstandingPrincipal")]
        public double? bakiDebet { get; set; }
        [JsonProperty("overdueDays")]
        public string jumlahHariTunggakan { get; set; }
        [JsonProperty("overdueFreq")]
        public string frekuensiTunggakan { get; set; }
        [JsonProperty("overdueInterestAmount")]
        public double? tunggakanBunga { get; set; }
        [JsonProperty("overduePrincipalAmount")]
        public double? tunggakanPokok { get; set; }
        [JsonProperty("projectAmount")]
        public double? nilaiProyek { get; set; }
        [JsonProperty("projectCity")]
        public string lokasiProyek { get; set; }
        [JsonProperty("projectCityDesc")]
        public string lokasiProyekKet { get; set; }
        [JsonProperty("restructureEndDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalRestrukturisasiAkhir { get; set; }
        [JsonProperty("restructureFreq")]
        public string frekuensiRestrukturisasi { get; set; }
        [JsonProperty("restructureMethod")]
        public string kodeCaraRestrukturisasi { get; set; }
        [JsonProperty("restructureMethodDesc")]
        public string restrukturisasiKet { get; set; }
        [JsonProperty("startDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalMulai { get; set; }
        [JsonProperty("collectibility")]
        public string kualitas { get; set; }
        [JsonProperty("collectibilityDesc")]
        public string kualitasKet { get; set; }
    }

    public class Lc : BaseFacility
    {
        [JsonProperty("accountNo")]
        public string noLc { get; set; }
        [JsonProperty("counterPartyBank")]
        public string bankBeneficiary { get; set; }
        [JsonProperty("initialContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAwal { get; set; }
        [JsonProperty("initialContractNo")]
        public string noAkadAwal { get; set; }
        [JsonProperty("lastContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAkhir { get; set; }
        [JsonProperty("lastContractNo")]
        public string noAkadAkhir { get; set; }
        [JsonProperty("lcAmount")]
        public double? lcAmount { get; set; }
        [JsonProperty("lcCollateralAmount")]
        public double? setoraJaminan { get; set; }
        [JsonProperty("lcDefaultDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalWanPrestasi { get; set; }
        [JsonProperty("lcEndDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalJthTempo { get; set; }
        [JsonProperty("lcIssueDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalKeluar { get; set; }
        [JsonProperty("lcLimit")]
        public double? plafon { get; set; }
        [JsonProperty("lcPurpose")]
        public string tujuanLc { get; set; }
        [JsonProperty("lcPurposeDesc")]
        public string tujuanLcKet { get; set; }
        [JsonProperty("lcType")]
        public string jenisLc { get; set; }
        [JsonProperty("lcTypeDesc")]
        public string jenisLcKet { get; set; }
        [JsonProperty("collectibilityType")]
        public string kualitas { get; set; }
        [JsonProperty("collectibilityTypeDesc")]
        public string kualitasKet { get; set; }
    }

    public class BankGuarantee : BaseFacility
    {
        [JsonProperty("accountNo")]
        public string accountNo { get; set; }
        [JsonProperty("bankGuaranteePurpose")]
        public string kodeTujuanGaransi { get; set; }
        [JsonProperty("bankGuaranteePurposeDesc")]
        public string tujuanGaransiKet { get; set; }
        [JsonProperty("bankGuaranteeType")]
        public string jenisGaransi { get; set; }
        [JsonProperty("bankGuaranteeTypeDesc")]
        public string jenisGaransiKet { get; set; }
        [JsonProperty("bgAmount")]
        public double? nominalBg { get; set; }
        [JsonProperty("bgDefaultDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalWanPrestasi { get; set; }
        [JsonProperty("bgEndDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalJatuhTempo { get; set; }
        [JsonProperty("bgIssueDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalDiterbitkan { get; set; }
        [JsonProperty("bgLimit")]
        public double? plafon { get; set; }
        [JsonProperty("collateralAmount")]
        public double? setoranJaminan { get; set; }
        [JsonProperty("guaranteeName")]
        public string namaYangDijamin { get; set; }
        [JsonProperty("initialContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAwal { get; set; }
        [JsonProperty("initialContractNo")]
        public string noAkadAwal { get; set; }
        [JsonProperty("lastContractDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalAkadAkhir { get; set; }
        [JsonProperty("lastContractNo")]
        public string noAkadAkhir { get; set; }
        [JsonProperty("collectibilityType")]
        public string kualitas { get; set; }
        [JsonProperty("collectibilityTypeDesc")]
        public string kualitasKet { get; set; }
    }

    public class Security : BaseFacility
    {
        [JsonProperty("amount")]
        public double? nominalSb { get; set; }
        [JsonProperty("accountNo")]
        public string noSuratBerharga { get; set; }
        [JsonProperty("securityType")]
        public string jenisSuratBerharga { get; set; }
        [JsonProperty("securityTypeDesc")]
        public string jenisSuratBerhargaKet { get; set; }
        [JsonProperty("sovereignRate")]
        public string sovereignRate { get; set; }
        [JsonProperty("isListed")]
        public string listing { get; set; }
        [JsonProperty("rating")]
        public string peringkatSuratBerharga { get; set; }
        [JsonProperty("securityOwnPurpose")]
        public string tujuanKepemilikan { get; set; }
        [JsonProperty("securityOwnPurposeDesc")]
        public string tujuanKepemilikanKet { get; set; }
        [JsonProperty("listingDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalTerbit { get; set; }
        [JsonProperty("dueDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalJatuhTempo { get; set; }
        [JsonProperty("interestRate")]
        public double? sukuBungaImbalan { get; set; }
        [JsonProperty("overdueDays")]
        public int? jumlahHariTunggakan { get; set; }
        [JsonProperty("oriCurrencyAmount")]
        public double? nilaiDalamMataUangAsal { get; set; }
        [JsonProperty("marketValue")]
        public double? nilaiPasar { get; set; }
        [JsonProperty("buyAmount")]
        public double? nilaiPerolehan { get; set; }
        [JsonProperty("overdueAmount")]
        public double? tunggakan { get; set; }
        [JsonProperty("nonPerformingDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalMacet { get; set; }
        [JsonProperty("nonPerformingReason")]
        public string kodeSebabMacet { get; set; }
        [JsonProperty("nonPerformingReasonDesc")]
        public string sebabMacetKet { get; set; }
        [JsonProperty("collectibilityType")]
        public string kualitas { get; set; }
        [JsonProperty("collectibilityTypeDesc")]
        public string kualitasKet { get; set; }
    }

    public class Others : BaseFacility
    {
        [JsonProperty("accountNo")]
        public string noRekening { get; set; }
        [JsonProperty("facilityAmount")]
        public double? nominalJumlahKwajibanIDR { get; set; }
        [JsonProperty("facilityEndDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalJatuhTempo { get; set; }
        [JsonProperty("facilityStartDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalMulai { get; set; }
        [JsonProperty("facilityType")]
        public string kodeJenisFasilitas { get; set; }
        [JsonProperty("facilityTypeDesc")]
        public string jenisFasilitasKet { get; set; }
        [JsonProperty("interestRate")]
        public double? sukuBungaImbalan { get; set; }
        [JsonProperty("nonPerformingDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalMacet { get; set; }
        [JsonProperty("nonPerformingReason")]
        public string kodeSebabMacet { get; set; }
        [JsonProperty("nonPerformingReasonDesc")]
        public string sebabMacetDesc { get; set; }
        [JsonProperty("oriCurrencyAmount")]
        public double? nilaiDalamMataUangAsal { get; set; }
        [JsonProperty("overdueAmount")]
        public double? tunggakan { get; set; }
        [JsonProperty("overdueDays")]
        public string jumlahHariTunggakan { get; set; }
        [JsonProperty("collectibilityType")]
        public string kualitas { get; set; }
        [JsonProperty("collectibilityTypeDesc")]
        public string kualitasKet { get; set; }
    }

    public class Facility
    {
        [JsonProperty("credits")]
        public List<Credit> kreditPembiayan { get; set; }
        [JsonProperty("lcs")]
        public List<Lc> lcs { get; set; }
        [JsonProperty("bankGuarantees")]
        public List<BankGuarantee> garansiYgDiberikan { get; set; }
        [JsonProperty("securities")]
        public List<Security> suratBerharga { get; set; }
        [JsonProperty("others")]
        public List<Others> fasilitasLain { get; set; }
        [JsonIgnore]
        public int totalFacilityNumber { get; set; }
        [JsonIgnore]
        public bool containFacility { get; set; }
    }

    public class Corporate : BaseIdebType
    {
        [JsonProperty("corporateKeyWord")]
        public CorporateKeyWord parameterPencarian { get; set; }
        [JsonProperty("corporateDebtors")]
        public List<CorporateDebtor> dataPokokDebitur { get; set; }
        [JsonProperty("offclsSharehldrGroups")]
        public List<OffclsSharehldrGroup> kelompokPengurusPemilik { get; set; }
    }

    public class Individual : BaseIdebType
    {
        [JsonProperty("individualKeyword")]
        public IndividualKeyword parameterPencarian { get; set; }
        [JsonProperty("individualDebtors")]
        public List<IndividualDebtor> dataPokokDebitur { get; set; }
    }

    public class Ideb
    {
        [JsonProperty("header")]
        public Header header { get; set; }
        [JsonProperty("corporate")]
        public Corporate perusahaan { get; set; }
        [JsonProperty("individual")]
        public Individual individual { get; set; }
    }

    public class Collectibility
    {
        public int seq { get; set; }
        public int month { get; set; }
        public DateTime? Yearmonth { get; set; }
        public string ColCode { get; set; }
        public int? Day { get; set; }
    }

    public class Collectibilities
    {
        public List<Collectibility> collectibility { get; set; }
    }

    public class BaseFacility
    {
        [JsonProperty("createdDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalDibentuk { get; set; }
        [JsonProperty("updatedDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalUpdate { get; set; }
        [JsonProperty("conditionDate")]
        [JsonConverter(typeof(JsonDate))]
        public DateTime? tanggalKondisi { get; set; }
        [JsonProperty("member")]
        public string ljk { get; set; }
        [JsonProperty("memberDesc")]
        public string ljkKet { get; set; }
        [JsonProperty("office")]
        public string cabang { get; set; }
        [JsonProperty("officeDesc")]
        public string cabangKet { get; set; }
        [GetOnlyJsonProperty]
        [JsonProperty("dataYearMonth")]
        public string tahunBulan { get; set; }
        [JsonProperty("year")]
        public string tahun
        {
            get
            {
                return string.IsNullOrEmpty(tahunBulan) ? null : tahunBulan.Substring(0, 4);
            }
            // set
            // {
            //     tahun = string.IsNullOrEmpty(tahunBulan) ? null : tahunBulan.Substring(0, 4);
            // }
        }
        [JsonProperty("month")]
        public string bulan
        {
            get
            {
                return string.IsNullOrEmpty(tahunBulan) ? null : tahunBulan.Substring(4, 2);
            }
            // set
            // {
            //     bulan = string.IsNullOrEmpty(tahunBulan) ? null : tahunBulan.Substring(0, 4);
            // }
        }
        [JsonProperty("condition")]
        public string kodeKondisi { get; set; }
        [JsonProperty("conditionDesc")]
        public string kondisiKet { get; set; }
        [JsonProperty("currencyCode")]
        public string kodeValuta { get; set; }
        [JsonProperty("collectibilityType")]
        public string tipeKualitas { get; set; }
        [JsonProperty("collectibilityTypeDesc")]
        public string tipeKualitasKet { get; set; }
        [JsonProperty("description")]
        public string keterangan { get; set; }
        [JsonIgnore]
        public string mk { get; set; }

        [JsonProperty("month01ColCode")]
        public string tahunBulan01Kol { get; set; }
        [JsonProperty("month01Day")]
        public int? tahunBulan01Ht { get; set; }
        [JsonProperty("month01Yearmonth")]
        public string tahunBulan01 { get; set; }
        [JsonProperty("month02ColCode")]
        public string tahunBulan02Kol { get; set; }
        [JsonProperty("month02Day")]
        public int? tahunBulan02Ht { get; set; }
        [JsonProperty("month02Yearmonth")]
        public string tahunBulan02 { get; set; }
        [JsonProperty("month03ColCode")]
        public string tahunBulan03Kol { get; set; }
        [JsonProperty("month03Day")]
        public int? tahunBulan03Ht { get; set; }
        [JsonProperty("month03Yearmonth")]
        public string tahunBulan03 { get; set; }
        [JsonProperty("month04ColCode")]
        public string tahunBulan04Kol { get; set; }
        [JsonProperty("month04Day")]
        public int? tahunBulan04Ht { get; set; }
        [JsonProperty("month04Yearmonth")]
        public string tahunBulan04 { get; set; }
        [JsonProperty("month05ColCode")]
        public string tahunBulan05Kol { get; set; }
        [JsonProperty("month05Day")]
        public int? tahunBulan05Ht { get; set; }
        [JsonProperty("month05Yearmonth")]
        public string tahunBulan05 { get; set; }
        [JsonProperty("month06ColCode")]
        public string tahunBulan06Kol { get; set; }
        [JsonProperty("month06Day")]
        public int? tahunBulan06Ht { get; set; }
        [JsonProperty("month06Yearmonth")]
        public string tahunBulan06 { get; set; }
        [JsonProperty("month07ColCode")]
        public string tahunBulan07Kol { get; set; }
        [JsonProperty("month07Day")]
        public int? tahunBulan07Ht { get; set; }
        [JsonProperty("month07Yearmonth")]
        public string tahunBulan07 { get; set; }
        [JsonProperty("month08ColCode")]
        public string tahunBulan08Kol { get; set; }
        [JsonProperty("month08Day")]
        public int? tahunBulan08Ht { get; set; }
        [JsonProperty("month08Yearmonth")]
        public string tahunBulan08 { get; set; }
        [JsonProperty("month09ColCode")]
        public string tahunBulan09Kol { get; set; }
        [JsonProperty("month09Day")]
        public int? tahunBulan09Ht { get; set; }
        [JsonProperty("month09Yearmonth")]
        public string tahunBulan09 { get; set; }
        [JsonProperty("month10ColCode")]
        public string tahunBulan10Kol { get; set; }
        [JsonProperty("month10Day")]
        public int? tahunBulan10Ht { get; set; }
        [JsonProperty("month10Yearmonth")]
        public string tahunBulan10 { get; set; }
        [JsonProperty("month11ColCode")]
        public string tahunBulan11Kol { get; set; }
        [JsonProperty("month11Day")]
        public int? tahunBulan11Ht { get; set; }
        [JsonProperty("month11Yearmonth")]
        public string tahunBulan11 { get; set; }
        [JsonProperty("month12ColCode")]
        public string tahunBulan12Kol { get; set; }
        [JsonProperty("month12Day")]
        public int? tahunBulan12Ht { get; set; }
        [JsonProperty("month12Yearmonth")]
        public string tahunBulan12 { get; set; }
        [JsonProperty("month13ColCode")]
        public string tahunBulan13Kol { get; set; }
        [JsonProperty("month13Day")]
        public int? tahunBulan13Ht { get; set; }
        [JsonProperty("month13Yearmonth")]
        public string tahunBulan13 { get; set; }
        [JsonProperty("month14ColCode")]
        public string tahunBulan14Kol { get; set; }
        [JsonProperty("month14Day")]
        public int? tahunBulan14Ht { get; set; }
        [JsonProperty("month14Yearmonth")]
        public string tahunBulan14 { get; set; }
        [JsonProperty("month15ColCode")]
        public string tahunBulan15Kol { get; set; }
        [JsonProperty("month15Day")]
        public int? tahunBulan15Ht { get; set; }
        [JsonProperty("month15Yearmonth")]
        public string tahunBulan15 { get; set; }
        [JsonProperty("month16ColCode")]
        public string tahunBulan16Kol { get; set; }
        [JsonProperty("month16Day")]
        public int? tahunBulan16Ht { get; set; }
        [JsonProperty("month16Yearmonth")]
        public string tahunBulan16 { get; set; }
        [JsonProperty("month17ColCode")]
        public string tahunBulan17Kol { get; set; }
        [JsonProperty("month17Day")]
        public int? tahunBulan17Ht { get; set; }
        [JsonProperty("month17Yearmonth")]
        public string tahunBulan17 { get; set; }
        [JsonProperty("month18ColCode")]
        public string tahunBulan18Kol { get; set; }
        [JsonProperty("month18Day")]
        public int? tahunBulan18Ht { get; set; }
        [JsonProperty("month18Yearmonth")]
        public string tahunBulan18 { get; set; }
        [JsonProperty("month19ColCode")]
        public string tahunBulan19Kol { get; set; }
        [JsonProperty("month19Day")]
        public int? tahunBulan19Ht { get; set; }
        [JsonProperty("month19Yearmonth")]
        public string tahunBulan19 { get; set; }
        [JsonProperty("month20ColCode")]
        public string tahunBulan20Kol { get; set; }
        [JsonProperty("month20Day")]
        public int? tahunBulan20Ht { get; set; }
        [JsonProperty("month20Yearmonth")]
        public string tahunBulan20 { get; set; }
        [JsonProperty("month21ColCode")]
        public string tahunBulan21Kol { get; set; }
        [JsonProperty("month21Day")]
        public int? tahunBulan21Ht { get; set; }
        [JsonProperty("month21Yearmonth")]
        public string tahunBulan21 { get; set; }
        [JsonProperty("month22ColCode")]
        public string tahunBulan22Kol { get; set; }
        [JsonProperty("month22Day")]
        public int? tahunBulan22Ht { get; set; }
        [JsonProperty("month22Yearmonth")]
        public string tahunBulan22 { get; set; }
        [JsonProperty("month23ColCode")]
        public string tahunBulan23Kol { get; set; }
        [JsonProperty("month23Day")]
        public int? tahunBulan23Ht { get; set; }
        [JsonProperty("month23Yearmonth")]
        public string tahunBulan23 { get; set; }
        [JsonProperty("month24ColCode")]
        public string tahunBulan24Kol { get; set; }
        [JsonProperty("month24Day")]
        public int? tahunBulan24Ht { get; set; }
        [JsonProperty("month24Yearmonth")]
        public string tahunBulan24 { get; set; }

        [JsonProperty("collaterals")]
        public List<Collateral> agunan { get; set; }
        [JsonProperty("guarantors")]
        public List<Guarantor> penjamin { get; set; }

    }

    

    public class BaseDebtor
    {
        [JsonProperty("createdDatetime")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalDibentuk { get; set; }
        [JsonProperty("updatedDatetime")]
        //[JsonConverter(typeof(JsonDate))]
        //public DateTime? tanggalUpdate { get; set; }
        public string tanggalUpdate { get; set; }
        [JsonProperty("identityNumberName")]
        public string namaDebitur { get; set; }
        [JsonProperty("taxId")]
        public string npwp { get; set; }
        [JsonProperty("address")]
        public string alamat { get; set; }
        [JsonProperty("subDistrict")]
        public string kelurahan { get; set; }
        [JsonProperty("district")]
        public string kecamatan { get; set; }
        [JsonProperty("city")]
        public string kabKota { get; set; }
        [JsonProperty("cityDesc")]
        public string kabKotaKet { get; set; }
        [JsonProperty("postalCode")]
        public string kodePos { get; set; }
        [JsonProperty("country")]
        public string negara { get; set; }
        [JsonProperty("countryDesc")]
        public string negaraKet { get; set; }
        [JsonProperty("member")]
        public string pelapor { get; set; }
        [JsonProperty("memberDesc")]
        public string pelaporKet { get; set; }

    }

    public class BaseIdebType
    {
        [JsonProperty("reportNumber")]
        public string nomorLaporan { get; set; }
        [JsonProperty("latestDataYearMonth")]
        public string posisiDataTerakhir { get; set; }
        [JsonProperty("requestDate")]
        [JsonConverter(typeof(JsonDateTime))]
        public DateTime? tanggalPermintaan { get; set; }

        [JsonProperty("facilitySummary")]
        public FacilitySummary ringkasanFasilitas { get; set; }
        [JsonProperty("facility")]
        public Facility fasilitas { get; set; }


    }
}
