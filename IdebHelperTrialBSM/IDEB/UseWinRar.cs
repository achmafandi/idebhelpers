﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IdebHelper
{
    class UseWinRar
    {
        private string rarExeFile = null;//WinRar.exe filepath
        private bool useAble = false;//Flag WinRar is available

        public UseWinRar()//
        {
            rarExeFile = getRarExe();
            useAble = !string.IsNullOrEmpty(rarExeFile);// if WinRar.exe filepath isn't null，it shows that is available
        }

        public static string getRarExe()//get WinRar filepath in your computer disk
        {
            string rarExe = null;
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\WinRAR.exe");
            if (regKey == null)
            {
                return null;
            }
            rarExe = regKey.GetValue("").ToString();
            regKey.Close();
            return rarExe;
        }

        public bool exeRarCmd(string cmd)// excute a winrar order
        {
            if (!useAble)
            {
                return false;
            }
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo(rarExeFile);
            startInfo.FileName = rarExeFile;
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.EnableRaisingEvents = false;
            startInfo.Arguments = cmd;

            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            while (!process.HasExited && process.Responding)
            {
                Thread.Sleep(100);
            }
            return true;
        }

        public bool unZipAll(string zipFile, string targetDirectory)
        {
            if (!File.Exists(zipFile))
            {
                return false;
            }
            string zipCmd = string.Format("x -o+ \"{0}\" \"{1}\" -y -ibck", zipFile, targetDirectory);
            //Background decompression compressed files to the specified directory of all files 
            exeRarCmd(zipCmd);
            return true;
        }

        public bool unZipToCurrentDirectory(string zipFile)
        {
            if (!File.Exists(zipFile))
            {
                return false;
            }
            FileInfo fileInfo = new FileInfo(zipFile);
            return unZipAll(zipFile, fileInfo.DirectoryName);
        }

        public bool unZipToDirectory(string zipFile, string destPath)
        {
            if (!File.Exists(zipFile))
            {
                return false;
            }
            FileInfo fileInfo = new FileInfo(zipFile);
            return unZipAll(zipFile, destPath);
        }
    }

}
