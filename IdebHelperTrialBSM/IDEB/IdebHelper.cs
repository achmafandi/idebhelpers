﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using DMS.Tools;
using DMS.Framework;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Linq;
using Ionic.Zip;
using System.Collections.Generic;
using Newtonsoft.Json;
using EvoPdf.HtmlToPdf;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;
using System.Diagnostics;

namespace IdebHelper
{
    public class Tools
    {
        private static int chunkSize = 524304;
        static public string extractIdeb(string pwds, FileInfo idebFile)
        {
            return extractIdeb(new string[] { pwds }, idebFile);
        }

        static public string extractIdeb(string[] pwds, FileInfo idebFile)
        {
            //trial
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20210501)
            {
                throw new Exception("Trial periode expired.");
            }
            string dirOut = null;
            byte[] file = File.ReadAllBytes(idebFile.FullName);

            foreach (var pwd in pwds)
            {
                try
                {
                    #region extract cara lama
                    RijndaelManaged rijndaelCipher = new RijndaelManaged();
                    rijndaelCipher.Mode = CipherMode.CBC;
                    rijndaelCipher.Padding = PaddingMode.PKCS7;

                    rijndaelCipher.KeySize = 0x80;
                    rijndaelCipher.BlockSize = 0x80;
                    byte[] encryptedData = file;
                    byte[] pwdBytes = Encoding.UTF8.GetBytes(pwd.PadRight(16, 'z'));
                    byte[] keyBytes = new byte[0x10];
                    int len = pwdBytes.Length;
                    if (len > keyBytes.Length)
                    {
                        len = keyBytes.Length;
                    }
                    Array.Copy(pwdBytes, keyBytes, len);
                    rijndaelCipher.Key = keyBytes;
                    rijndaelCipher.IV = keyBytes;
                    byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
                    byte[] sub = plainText.Skip(10).Take(plainText.Length).ToArray();

                    // 20191225, new ideb version
                    string stringText = Encoding.UTF8.GetString(plainText);
                    if (stringText.ToLower().Contains("minver="))
                        sub = sub.Skip(21).Take(sub.Length).ToArray();
                    #endregion

                    #region extract cara baru
                    //byte[][] encChunks = chunkArray(file, chunkSize);
                    //MemoryStream plainOs = new MemoryStream();
                    //foreach (byte[] encChunk in encChunks)
                    //{
                    //    byte[] decChunk = aesEncryption128Bit(pwd, new MemoryStream(encChunk));
                    //    plainOs.Write(decChunk, 0, decChunk.Length);
                    //}

                    //byte[] decrypted = plainOs.ToArray();
                    //byte[] sub = decrypted.Skip(10).Take(decrypted.Length).ToArray();
                    //string stringText = Encoding.UTF8.GetString(decrypted);
                    //if (stringText.ToLower().Contains("minver="))
                    //    sub = sub.Skip(21).Take(sub.Length).ToArray();
                    #endregion extract cara baru

                    dirOut = Path.Combine(idebFile.DirectoryName, idebFile.Name.Substring(0, idebFile.Name.Length - 5));
                    if (Directory.Exists(dirOut))
                        Directory.Delete(dirOut, true);

                    using (ZipFile zip = ZipFile.Read(sub))
                    {
                        foreach (ZipEntry ee in zip)
                        {
                            //try
                            //{
                                ee.ExtractWithPassword(dirOut, ExtractExistingFileAction.OverwriteSilently, pwd);
                            //}
                            //catch { }
                        }
                    }

                    return dirOut;
                }
                catch
                {

                }

            }

            return dirOut;
        }
        
        public static byte[][] chunkArray(byte[] array, int chunkSize)
        {
            double num = (double)array.Length / chunkSize;
            int numOfChunks = int.Parse(Math.Ceiling(num).ToString());
            byte[][] output = new byte[numOfChunks][];
            for (int i = 0; i < numOfChunks; i++)
            {
                int start = i * chunkSize;
                int length = Math.Min(array.Length - start, chunkSize);

                byte[] temp = new byte[length];
                Array.Copy(array, start, temp, 0, length);
                output[i] = temp;
            }
            return output;
        }

        private static byte[] aesEncryption128Bit(string password, MemoryStream inputStream)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            rijndaelCipher.Mode = CipherMode.CBC;
            rijndaelCipher.Padding = PaddingMode.PKCS7;

            rijndaelCipher.KeySize = 0x80;
            rijndaelCipher.BlockSize = 0x80;
            byte[] pwdBytes = Encoding.UTF8.GetBytes(password.PadRight(16, 'z'));
            byte[] keyBytes = new byte[0x10];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }
            Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = keyBytes;

            byte[] chunkData = rijndaelCipher.CreateDecryptor().TransformFinalBlock(inputStream.ToArray(), 0, inputStream.ToArray().Length);

            return chunkData;
        }

        static public string idebToPdfEvo(string html, string footer, string outpath)
        {
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "Vn1ndmVldmdkdmV4Y3ZlZ3hnZHhvb29v";
            // save the PDF bytes in a file on disk
            StringBuilder sb = new StringBuilder();

            string pdfFile = Path.Combine(outpath, Guid.NewGuid() + ".pdf");

            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.LeftMargin = 18;
            pdfConverter.PdfDocumentOptions.RightMargin = 18;
            pdfConverter.PdfDocumentOptions.TopMargin = 18;
            pdfConverter.PdfDocumentOptions.BottomMargin = 30;

            string htmlfooter = footer;
            if (htmlfooter != "")
            {
                pdfConverter.PdfFooterOptions.FooterHeight = 30;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;

                //write html footer
                HtmlToPdfArea HtmlFooterArea = new HtmlToPdfArea(htmlfooter, null);
                pdfConverter.PdfFooterOptions.AddHtmlToPdfArea(HtmlFooterArea);

                //write the page number
                pdfConverter.PdfFooterOptions.TextArea = new EvoPdf.HtmlToPdf.TextArea(0, 20, "halaman &p; dari &P; ",
                new System.Drawing.Font(new System.Drawing.FontFamily("Century Gothic"), 5, System.Drawing.GraphicsUnit.Point));
                pdfConverter.PdfFooterOptions.TextArea.EmbedTextFont = true;
                pdfConverter.PdfFooterOptions.TextArea.ForeColor = System.Drawing.Color.FromArgb(75, 71, 60);
                pdfConverter.PdfFooterOptions.TextArea.TextAlign = HorizontalTextAlign.Right;
            }

            pdfConverter.SavePdfFromHtmlStringToFile(html, pdfFile);

            return pdfFile;
        }

        static public string idebToPdfEvoBased64(Ideb ideb, string imgpath, bool printfooter = true)
        {
            string html = idebToHtml(ideb, imgpath);
            string footer = ""; 
            if (printfooter==true) footer = idebToHtmlFooter(ideb);
            return idebToPdfEvoBased64(html, footer);
        }

        static public string idebToPdfEvoBased64(string html, string footer)
        {
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "Vn1ndmVldmdkdmV4Y3ZlZ3hnZHhvb29v";
            // save the PDF bytes in a file on disk
            StringBuilder sb = new StringBuilder();

            //string pdfFile = Path.Combine(outpath, Guid.NewGuid() + ".pdf");

            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.LeftMargin = 18;
            pdfConverter.PdfDocumentOptions.RightMargin = 18;
            pdfConverter.PdfDocumentOptions.TopMargin = 18;
            pdfConverter.PdfDocumentOptions.BottomMargin = 30;

            string htmlfooter = footer;
            if (htmlfooter != "")
            {
                pdfConverter.PdfFooterOptions.FooterHeight = 30;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;

                //write html footer
                HtmlToPdfArea HtmlFooterArea = new HtmlToPdfArea(htmlfooter, null);
                pdfConverter.PdfFooterOptions.AddHtmlToPdfArea(HtmlFooterArea);

                //write the page number
                pdfConverter.PdfFooterOptions.TextArea = new EvoPdf.HtmlToPdf.TextArea(0, 20, "halaman &p; dari &P; ",
                new System.Drawing.Font(new System.Drawing.FontFamily("Century Gothic"), 5, System.Drawing.GraphicsUnit.Point));
                pdfConverter.PdfFooterOptions.TextArea.EmbedTextFont = true;
                pdfConverter.PdfFooterOptions.TextArea.ForeColor = System.Drawing.Color.FromArgb(75, 71, 60);
                pdfConverter.PdfFooterOptions.TextArea.TextAlign = HorizontalTextAlign.Right;
            }

            //pdfConverter.SavePdfFromHtmlStringToFile(html, pdfFile);
            MemoryStream outPdfStream = new MemoryStream();
            pdfConverter.ConversionDelay = 0;
            //pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
            pdfConverter.SavePdfFromHtmlStringToStream(html, outPdfStream);

            byte[] bytes = outPdfStream.ToArray();
            string base64 = Convert.ToBase64String(bytes);

            return base64;
        }

        static public string idebToHtml(Ideb ideb, string imgPath)
        {
            return idebToHtml(ideb, imgPath, false);
        }

        static public string idebToHtml(Ideb ideb, string imgPath, bool addFooter)
        {
            FacilitySummary facilitySummary = new FacilitySummary();
            Facility facility = new Facility();
            if (ideb.individual != null)
            {
                facilitySummary = ideb.individual.ringkasanFasilitas;
                facility = ideb.individual.fasilitas;
            }
            else if (ideb.perusahaan != null)
            {
                facilitySummary = ideb.perusahaan.ringkasanFasilitas;
                facility = ideb.perusahaan.fasilitas;
            }

            return IdebHTML.GetHTMLString(ideb.header, ideb.individual, ideb.perusahaan, facilitySummary, facility, imgPath, addFooter);
        }

        static public string idebToHtmlFooter(Ideb ideb)
        {
            return IdebHTML.GetHTMLFooterString(ideb);
        }

        static public string idebToDataTable(string pwd, FileInfo idebFile, DbConnection conn)
        {
            string id = Guid.NewGuid().ToString();
            string folderExtract = null;
            string status = "Failed";
            string msg = "";
            string dirErr = Path.Combine(idebFile.DirectoryName, "Error", DateTime.Now.ToString("yyyyMMdd"));
            string dirSuc = Path.Combine(idebFile.DirectoryName, "Success", DateTime.Now.ToString("yyyyMMdd"));
            try
            {
                folderExtract = extractIdeb(pwd, idebFile);
                if (string.IsNullOrEmpty(folderExtract))
                {
                    status = "Failed";
                    msg = "Failed to exctact ideb file. check ideb viewer password setting!";
                }
                else
                    status = "Success";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            #region IDEB_FILE
            byte[] bytes = File.ReadAllBytes(idebFile.FullName);
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", id);
            staticFramework.saveNVC(Fields, "FILENAME", idebFile.Name);
            staticFramework.saveNVC(Fields, "FILEDATE", DateTime.Now);
            staticFramework.saveNVC(Fields, "FILEHOST", idebFile.CreationTime);
            staticFramework.saveNVC(Fields, "FILEPATH", idebFile.DirectoryName);
            staticFramework.saveNVC(Fields, "FILESTATUS", status);
            staticFramework.saveNVC(Fields, "FILEMSG", msg);
            staticFramework.save(Fields, Keys, "IDEB_FILE", conn);

            using (SqlConnection con = new SqlConnection(conn.ConnString))
            {
                string query = "UPDATE IDEB_FILE SET FileIdEB = @FileIdEB WHERE FileId = @FileId";

                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@FileId", id);
                    cmd.Parameters.AddWithValue("@FileIdEB", bytes);
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }

            if (!string.IsNullOrEmpty(msg))
            {
                if (!Directory.Exists(dirErr))
                    Directory.CreateDirectory(dirErr);
                File.Copy(idebFile.FullName, Path.Combine(dirErr, idebFile.Name), true);
                File.Delete(idebFile.FullName);
                try
                {
                    Directory.Delete(folderExtract, true);
                }
                catch { }

                throw new Exception(msg);
            }
            #endregion

            //LOOP EACH FILE EXTRACTED
            foreach (string fn in Directory.GetFiles(folderExtract))
            {
                string user_ref_code = null;
                string idx = Guid.NewGuid().ToString();
                FileInfo fnx = new FileInfo(fn);
                Ideb ideb = deserializeJson(fnx);

                Keys = new NameValueCollection();
                Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", id);
                staticFramework.saveNVC(Keys, "FileDetailId", idx);
                staticFramework.saveNVC(Fields, "FILENAME", fnx.Name);
                staticFramework.saveNVC(Fields, "FILEJSON", File.ReadAllText(fnx.FullName));
                //staticFramework.saveNVC(Fields, "FILEJSON", JsonConvert.SerializeObject(ideb));
                staticFramework.save(Fields, Keys, "IDEB_FILEDETAIL", conn);

                idebHeaderToDataTable(ideb.header, id, idx, conn);
                user_ref_code = ideb.header.kodeReferensiPengguna;

                if (ideb.individual != null)
                {
                    idebIndividualToDataTable(ideb.individual, id, idx, conn);
                    idebIndividualKeyWordToDataTable(ideb.individual.parameterPencarian, id, idx, conn);
                    idebIndividualDebtorToDataTable(ideb.individual.dataPokokDebitur, id, idx, conn);
                    idebFacilitySummaryToDataTable(ideb.individual.ringkasanFasilitas, id, idx, conn);
                    idebFacilityToDataTable(ideb.individual.fasilitas, id, idx, conn);
                }
                else if (ideb.perusahaan != null)
                {
                    idebCorporateToDataTable(ideb.perusahaan, id, idx, conn);
                    idebCorporateKeyWordToDataTable(ideb.perusahaan.parameterPencarian, id, idx, conn);
                    idebCorporateDebtorToDataTable(ideb.perusahaan.dataPokokDebitur, id, idx, conn);
                    idebOffclsSharehldrGroupToDataTable(ideb.perusahaan.kelompokPengurusPemilik, id, idx, conn);
                    idebFacilitySummaryToDataTable(ideb.perusahaan.ringkasanFasilitas, id, idx, conn);
                    idebFacilityToDataTable(ideb.perusahaan.fasilitas, id, idx, conn);
                }
            }

            //clean up folder
            if (!Directory.Exists(dirSuc))
                Directory.CreateDirectory(dirSuc);
            File.Copy(idebFile.FullName, Path.Combine(dirSuc, idebFile.Name), true);
            File.Delete(idebFile.FullName);
            Directory.Delete(folderExtract, true);

            return id;
        }

        static public string idebToIextract(string pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath)
        {
            return idebToIextract(pwd, idebFile, conn, imgpath, replicatePath, false, false, true);
        }

        static public string idebToIextract(string pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath, bool exportPdf, bool exportHtml)
        {
            return idebToIextract(new string[] { pwd }, idebFile, conn, imgpath, replicatePath, exportPdf, exportHtml, true);
        }

        static public string idebToIextract(string pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath, bool exportPdf, bool exportHtml, bool printfooter)
        {
            return idebToIextract(new string[] { pwd }, idebFile, conn, imgpath, replicatePath, exportPdf, exportHtml, printfooter);
        }

        static public string idebToIextract(string[] pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath)
        {
            return idebToIextract(pwd, idebFile, conn, imgpath, replicatePath, false, false, true);
        }

        static public string idebToIextract(string[] pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath, bool exportPdf, bool exportHtml)
        {
            return idebToIextract(pwd, idebFile, conn, imgpath, replicatePath, exportPdf, exportHtml, true);
        }

        static public string idebToIextract(string[] pwd, FileInfo idebFile, DbConnection conn, string imgpath, string replicatePath, bool exportPdf, bool exportHtml, bool printfooter)
        {
            string folderExtract = null;
            string status = "Failed";
            string msg = "";
            string dirTem = Path.Combine(idebFile.DirectoryName, "Run");
            string dirErr = Path.Combine(idebFile.DirectoryName, "Error", DateTime.Now.ToString("yyyyMMdd"));
            string dirSuc = Path.Combine(idebFile.DirectoryName, "Success", DateTime.Now.ToString("yyyyMMdd"));
            string dirReplicateErr = "";
            string dirReplicateSuc = "";
            FileInfo idebFileTemp = new FileInfo(Path.Combine(dirTem, idebFile.Name));

            if (!Directory.Exists(dirTem))
                Directory.CreateDirectory(dirTem);

            if (idebFileTemp.Exists)
                idebFileTemp.Delete();

            File.Move(idebFile.FullName, idebFileTemp.FullName);

            if (!string.IsNullOrEmpty(replicatePath))
            {
                dirReplicateErr = Path.Combine(replicatePath, "Error", DateTime.Now.ToString("yyyyMMdd"));
                dirReplicateSuc = Path.Combine(replicatePath, "Success", DateTime.Now.ToString("yyyyMMdd"));
            }

            // log file
            string trn_ideb_id = "";
            //string trn_ideb_detail_id = "";

            #region trn_idebs
            string query =
@"insert into trn_idebs 
(
    created_by,
    created_datetime,
    updated_by,
    updated_datetime,
    delete_flag,
    cfg_ideb_extractor_id,
    cfg_source_dir_id,
    filename,
    message,
    status_flag
) 
values 
(
    @created_by,
    @created_datetime,
    @updated_by,
    @updated_datetime,
    @delete_flag,
    @cfg_ideb_extractor_id,
    @cfg_source_dir_id,
    @filename,
    @message,
    @status_flag
)
select SCOPE_IDENTITY()
";
            using (SqlConnection con = new SqlConnection(conn.ConnString))
            {

                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@created_by", "1");
                    cmd.Parameters.AddWithValue("@created_datetime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@updated_by", "1");
                    cmd.Parameters.AddWithValue("@updated_datetime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@delete_flag", "N");
                    cmd.Parameters.AddWithValue("@cfg_ideb_extractor_id", "1");
                    cmd.Parameters.AddWithValue("@cfg_source_dir_id", "1");
                    cmd.Parameters.AddWithValue("@filename", idebFileTemp.Name);
                    cmd.Parameters.AddWithValue("@message", msg);
                    cmd.Parameters.AddWithValue("@status_flag", status == "Success" ? "S" : "E");
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        trn_ideb_id = reader[0].ToString();
                    }
                }
            }
            #endregion

            // extract file
            try
            {
                folderExtract = extractIdeb(pwd, idebFileTemp);
                if (string.IsNullOrEmpty(folderExtract) || !Directory.Exists(folderExtract))
                {
                    status = "Failed";
                    msg = "Failed to extract ideb file!";
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            if (!string.IsNullOrEmpty(msg))
            {
                // update into error
                query = "update trn_idebs set status_flag = @status_flag, message = @message where trn_ideb_id = @trn_ideb_id";
                using (SqlConnection con = new SqlConnection(conn.ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@status_flag", "E");
                        cmd.Parameters.AddWithValue("@message", msg);
                        cmd.Parameters.AddWithValue("@trn_ideb_id", trn_ideb_id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                if (!Directory.Exists(dirErr))
                    Directory.CreateDirectory(dirErr);
                File.Copy(idebFileTemp.FullName, Path.Combine(dirErr, idebFileTemp.Name), true);
                if (!string.IsNullOrEmpty(dirReplicateErr))
                {
                    if (!Directory.Exists(dirReplicateErr))
                        Directory.CreateDirectory(dirReplicateErr);
                    File.Copy(idebFileTemp.FullName, Path.Combine(dirReplicateErr, idebFileTemp.Name), true);
                }

                // move back file 
                if (msg == "PDF Failed!" || msg == "Failed to extract ideb file!" || msg.ToLower().Contains("deadlock"))
                {
                    string querycek = @"select count(*) from trn_idebs
                    where convert(varchar, created_datetime, 103) = @created_datetime
                    and filename = @filename and status_flag = @status_flag";
                    using (SqlConnection con = new SqlConnection(conn.ConnString))
                    {
                        using (SqlCommand cmd = new SqlCommand(querycek))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@created_datetime", DateTime.Now.ToString("dd/MM/yyyy"));
                            cmd.Parameters.AddWithValue("@filename", idebFileTemp.Name);
                            cmd.Parameters.AddWithValue("@status_flag", "E");
                            con.Open();
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                int rowcnt = int.Parse(reader[0].ToString());
                                if (rowcnt < 5) File.Copy(idebFileTemp.FullName, idebFile.FullName);
                            }
                        }
                    }
                }
                
                return null;
            }

            //LOOP EACH FILE EXTRACTED
            try
            {
                DirectoryInfo dix = new DirectoryInfo(folderExtract);
                Parallel.ForEach(dix.GetFiles(),
                new ParallelOptions { MaxDegreeOfParallelism = 16 },
                fnx =>
                {
                    //foreach (string fn in Directory.GetFiles(folderExtract))
                    //{
                    try
                    {
                        string idx = Guid.NewGuid().ToString();
                        string trn_ideb_detail_id = null;
                        //FileInfo fnx = null;
                        //File.WriteAllText("D:\\Demo\\CBAS\\CBASIdebUploaderService\\bin\\Debug\\logz\\LogIdebHelper.txt",
                        //    File.ReadAllText(fnx.FullName));
                        Ideb ideb = null;
                        //try
                        //{
                            //fnx = new FileInfo(fn);
                            ideb = deserializeJson(fnx);
                        //}
                        //catch
                        //{
                        //    //continue;
                        //}
                       
                        #region trn_ideb_details
                        string query1 =
        @"insert into trn_ideb_details 
(
    created_by,
    created_datetime,
    updated_by,
    updated_datetime,
    delete_flag,
    debtor_type_flag,
    filename,
    h_data_set_number,
    h_data_set_total,
    h_inquiry_created_by,
    h_inquiry_date,
    h_inquiry_id,
    h_inquiry_member_code,
    h_inquiry_office_code,
    h_inquiry_user_id,
    h_report_request_purpose_code,
    h_result_date,
    h_user_reference_code,
    ic_latest_data_year_month,
    ic_report_number,
    ic_request_date,
    trn_ideb_id
) 
values 
(
    @created_by,
    @created_datetime,
    @updated_by,
    @updated_datetime,
    @delete_flag,
    @debtor_type_flag,
    @filename,
    @h_data_set_number,
    @h_data_set_total,
    @h_inquiry_created_by,
    @h_inquiry_date,
    @h_inquiry_id,
    @h_inquiry_member_code,
    @h_inquiry_office_code,
    @h_inquiry_user_id,
    @h_report_request_purpose_code,
    @h_result_date,
    @h_user_reference_code,
    @ic_latest_data_year_month,
    @ic_report_number,
    @ic_request_date,
    @trn_ideb_id
)
select SCOPE_IDENTITY()
";

                        using (SqlConnection con = new SqlConnection(conn.ConnString))
                        {
                            using (SqlCommand cmd = new SqlCommand(query1))
                            {
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@created_by", "1");
                                cmd.Parameters.AddWithValue("@created_datetime", DateTime.Now);
                                cmd.Parameters.AddWithValue("@updated_by", "1");
                                cmd.Parameters.AddWithValue("@updated_datetime", DateTime.Now);
                                cmd.Parameters.AddWithValue("@delete_flag", "N");

                                cmd.Parameters.AddWithValue("@debtor_type_flag", "1");
                                cmd.Parameters.AddWithValue("@filename", fnx.Name);
                                cmd.Parameters.AddWithValue("@h_data_set_number", ideb.header.nomorBagian);
                                cmd.Parameters.AddWithValue("@h_data_set_total", ideb.header.totalBagian);
                                cmd.Parameters.AddWithValue("@h_inquiry_created_by", ideb.header.dibuatOleh);
                                cmd.Parameters.AddWithValue("@h_inquiry_date", ideb.header.tanggalPermintaan);
                                cmd.Parameters.AddWithValue("@h_inquiry_id", ideb.header.idPermintaan);
                                cmd.Parameters.AddWithValue("@h_inquiry_member_code", ideb.header.kodeLJKPermintaan);
                                cmd.Parameters.AddWithValue("@h_inquiry_office_code", ideb.header.kodeCabangPermintaan);
                                cmd.Parameters.AddWithValue("@h_inquiry_user_id", ideb.header.idPenggunaPermintaan);
                                cmd.Parameters.AddWithValue("@h_report_request_purpose_code", ideb.header.kodeTujuanPermintaan);
                                cmd.Parameters.AddWithValue("@h_result_date", ideb.header.tanggalHasil);
                                cmd.Parameters.AddWithValue("@h_user_reference_code", ideb.header.kodeReferensiPengguna);
                                cmd.Parameters.AddWithValue("@ic_latest_data_year_month", ideb.individual == null ? ideb.perusahaan.posisiDataTerakhir : ideb.individual.posisiDataTerakhir);
                                cmd.Parameters.AddWithValue("@ic_report_number", ideb.individual == null ? ideb.perusahaan.nomorLaporan : ideb.individual.nomorLaporan);
                                cmd.Parameters.AddWithValue("@ic_request_date", ideb.individual == null ? ideb.perusahaan.tanggalPermintaan.Value.ToString("yyyyMMddhhmmss") : ideb.individual.tanggalPermintaan.Value.ToString("yyyyMMddhhmmss"));
                                cmd.Parameters.AddWithValue("@trn_ideb_id", trn_ideb_id);
                                con.Open();
                                SqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    trn_ideb_detail_id = reader[0].ToString();
                                }
                            }
                        }
                        #endregion

                        #region trn_ideb_detail_attrs
                        Ideb ideb2 = fixdatetime(ideb);
                        var jsonser = JsonConvert.SerializeObject(ideb2, new JsonSerializerSettings { ContractResolver = new CustomContractResolver(false) });
                        var ideb_content_ina = jsonser.Replace("krediturBPRS", "krediturBPR_S");
                        var ideb_pdf = exportPdf ? Convert.FromBase64String(idebToPdfEvoBased64(ideb2, imgpath, printfooter)) : null;
                        var ideb_html = exportHtml ? idebToHtml(ideb2, imgpath, true) : "";

                        string query2 =
    @"insert into trn_ideb_detail_attrs 
(
    trn_ideb_detail_id,
    ideb_content,
    ideb_content_ina,
    ideb_pdf,
    ideb_html
) 
values 
(
    @trn_ideb_detail_id,
    @ideb_content,
    @ideb_content_ina,
    @ideb_pdf,
    @ideb_html
)";
                        string query_nopdf =
    @"insert into trn_ideb_detail_attrs 
(
    trn_ideb_detail_id,
    ideb_content,
    ideb_content_ina,
    ideb_html
) 
values 
(
    @trn_ideb_detail_id,
    @ideb_content,
    @ideb_content_ina,
    @ideb_html
)";
                        if (!exportPdf) query2 = query_nopdf;
                        using (SqlConnection con = new SqlConnection(conn.ConnString))
                        {
                            using (SqlCommand cmd = new SqlCommand(query2))
                            {
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@trn_ideb_detail_id", trn_ideb_detail_id);
                                cmd.Parameters.AddWithValue("@ideb_content", File.ReadAllText(fnx.FullName));
                                cmd.Parameters.AddWithValue("@ideb_content_ina", ideb_content_ina);
                                if (exportPdf) cmd.Parameters.AddWithValue("@ideb_pdf", ideb_pdf);
                                cmd.Parameters.AddWithValue("@ideb_html", ideb_html);
                                con.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }
                        #endregion

                        // update into success
                        string query3 = "update trn_idebs set status_flag = @status_flag, message = @message where trn_ideb_id = @trn_ideb_id";
                        using (SqlConnection con = new SqlConnection(conn.ConnString))
                        {
                            using (SqlCommand cmd = new SqlCommand(query3))
                            {
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@status_flag", "S");
                                cmd.Parameters.AddWithValue("@message", msg);
                                cmd.Parameters.AddWithValue("@trn_ideb_id", trn_ideb_id);
                                con.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }

                        //}
                    } catch (Exception exs)
                    {
                        query = "update trn_idebs set status_flag = @status_flag, message = @message where trn_ideb_id = @trn_ideb_id";
                        using (SqlConnection con = new SqlConnection(conn.ConnString))
                        {
                            using (SqlCommand cmd = new SqlCommand(query))
                            {
                                var st = new StackTrace(exs, true);
                                var LineNumber = st.GetFrame(st.FrameCount - 1).GetFileLineNumber();
                                string errmsg = exs.ToString() + ". line: " + LineNumber.ToString();
                                if (exs.Message.IndexOf("Last Query:") > 0)
                                    errmsg = errmsg.Substring(0, errmsg.IndexOf("Last Query:"));

                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@status_flag", "E");
                                cmd.Parameters.AddWithValue("@message", errmsg);
                                cmd.Parameters.AddWithValue("@trn_ideb_id", trn_ideb_id);
                                con.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                msg = "ERROR | Loop FolderExtract | "+ex.Message;

            }

                //clean up folder
                if (!Directory.Exists(dirSuc))
                    Directory.CreateDirectory(dirSuc);
                File.Copy(idebFileTemp.FullName, Path.Combine(dirSuc, idebFileTemp.Name), true);

                if (!string.IsNullOrEmpty(dirReplicateSuc))
                {
                    if (!Directory.Exists(dirReplicateSuc))
                        Directory.CreateDirectory(dirReplicateSuc);
                    File.Copy(idebFileTemp.FullName, Path.Combine(dirReplicateSuc, idebFileTemp.Name), true);
                }

            return trn_ideb_id;
        }

        static public Ideb deserializeJson(FileInfo jsonFile)
        {
            var dezerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CustomContractResolver(true)
            };

            var json = File.ReadAllText(jsonFile.FullName);
            Ideb ideb = JsonConvert.DeserializeObject<Ideb>(json, dezerializerSettings);

            //yearmonth based on opendate, misal buka 08, yearmonth08 dianggap yg pertama, yearmonth01 = jan tahun berikutnya
            //recreate collect table
            if (ideb.individual != null)
            {
                //DateTime dtData = DateTime.ParseExact(ideb.individual.posisiDataTerakhir + "01", "yyyyMMdd", null);
                DateTime dtData = DateTime.ParseExact(ideb.individual.tanggalPermintaan.Value.AddMonths(-1).ToString("yyyyMM") + "01", "yyyyMMdd", null);
                ideb.individual.fasilitas.kreditPembiayan = modifCollectCredit(ideb.individual.fasilitas.kreditPembiayan, dtData);
                ideb.individual.fasilitas.lcs = modifCollectLcs(ideb.individual.fasilitas.lcs, dtData);
                ideb.individual.fasilitas.garansiYgDiberikan = modifCollectBankGuarantee(ideb.individual.fasilitas.garansiYgDiberikan, dtData);
                ideb.individual.fasilitas.suratBerharga = modifCollectSecurity(ideb.individual.fasilitas.suratBerharga, dtData);
                ideb.individual.fasilitas.fasilitasLain = modifCollectOthers(ideb.individual.fasilitas.fasilitasLain, dtData);
            }
            else if (ideb.perusahaan != null)
            {
                //DateTime dtData = DateTime.ParseExact(ideb.perusahaan.posisiDataTerakhir + "01", "yyyyMMdd", null);
                DateTime dtData = DateTime.ParseExact(ideb.perusahaan.tanggalPermintaan.Value.AddMonths(-1).ToString("yyyyMM") + "01", "yyyyMMdd", null);
                ideb.perusahaan.fasilitas.kreditPembiayan = modifCollectCredit(ideb.perusahaan.fasilitas.kreditPembiayan, dtData);
                ideb.perusahaan.fasilitas.lcs = modifCollectLcs(ideb.perusahaan.fasilitas.lcs, dtData);
                ideb.perusahaan.fasilitas.garansiYgDiberikan = modifCollectBankGuarantee(ideb.perusahaan.fasilitas.garansiYgDiberikan, dtData);
                ideb.perusahaan.fasilitas.suratBerharga = modifCollectSecurity(ideb.perusahaan.fasilitas.suratBerharga, dtData);
                ideb.perusahaan.fasilitas.fasilitasLain = modifCollectOthers(ideb.perusahaan.fasilitas.fasilitasLain, dtData);
            }

            return ideb;
        }

        static public Ideb deserializeJson(string json)
        {
            var dezerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CustomContractResolver(true)
            };

            Ideb ideb = JsonConvert.DeserializeObject<Ideb>(json, dezerializerSettings);

            //yearmonth based on opendate, misal buka 08, yearmonth08 dianggap yg pertama, yearmonth01 = jan tahun berikutnya
            //recreate collect table
            if (ideb.individual != null)
            {
                //DateTime dtData = DateTime.ParseExact(ideb.individual.posisiDataTerakhir + "01", "yyyyMMdd", null);
                DateTime dtData = DateTime.ParseExact(ideb.individual.tanggalPermintaan.Value.AddMonths(-1).ToString("yyyyMM") + "01", "yyyyMMdd", null);
                ideb.individual.fasilitas.kreditPembiayan = modifCollectCredit(ideb.individual.fasilitas.kreditPembiayan, dtData);
                ideb.individual.fasilitas.lcs = modifCollectLcs(ideb.individual.fasilitas.lcs, dtData);
                ideb.individual.fasilitas.garansiYgDiberikan = modifCollectBankGuarantee(ideb.individual.fasilitas.garansiYgDiberikan, dtData);
                ideb.individual.fasilitas.suratBerharga = modifCollectSecurity(ideb.individual.fasilitas.suratBerharga, dtData);
                ideb.individual.fasilitas.fasilitasLain = modifCollectOthers(ideb.individual.fasilitas.fasilitasLain, dtData);
            }
            else if (ideb.perusahaan != null)
            {
                //DateTime dtData = DateTime.ParseExact(ideb.perusahaan.posisiDataTerakhir + "01", "yyyyMMdd", null);
                DateTime dtData = DateTime.ParseExact(ideb.perusahaan.tanggalPermintaan.Value.AddMonths(-1).ToString("yyyyMM") + "01", "yyyyMMdd", null);
                ideb.perusahaan.fasilitas.kreditPembiayan = modifCollectCredit(ideb.perusahaan.fasilitas.kreditPembiayan, dtData);
                ideb.perusahaan.fasilitas.lcs = modifCollectLcs(ideb.perusahaan.fasilitas.lcs, dtData);
                ideb.perusahaan.fasilitas.garansiYgDiberikan = modifCollectBankGuarantee(ideb.perusahaan.fasilitas.garansiYgDiberikan, dtData);
                ideb.perusahaan.fasilitas.suratBerharga = modifCollectSecurity(ideb.perusahaan.fasilitas.suratBerharga, dtData);
                ideb.perusahaan.fasilitas.fasilitasLain = modifCollectOthers(ideb.perusahaan.fasilitas.fasilitasLain, dtData);
            }

            return ideb;
        }

        static public string licenseCheck(string ip)
        {
            //
            return "";
        }

        static private List<Collectibility> facilityToCollect(BaseFacility facility)
        {
            var collect = new List<Collectibility>();

            collect.Add(new Collectibility() { seq = 0, month = 01, Yearmonth = null, ColCode = facility.tahunBulan01Kol, Day = facility.tahunBulan01Ht });
            collect.Add(new Collectibility() { seq = 0, month = 02, Yearmonth = null, ColCode = facility.tahunBulan02Kol, Day = facility.tahunBulan02Ht });
            collect.Add(new Collectibility() { seq = 0, month = 03, Yearmonth = null, ColCode = facility.tahunBulan03Kol, Day = facility.tahunBulan03Ht });
            collect.Add(new Collectibility() { seq = 0, month = 04, Yearmonth = null, ColCode = facility.tahunBulan04Kol, Day = facility.tahunBulan04Ht });
            collect.Add(new Collectibility() { seq = 0, month = 05, Yearmonth = null, ColCode = facility.tahunBulan05Kol, Day = facility.tahunBulan05Ht });
            collect.Add(new Collectibility() { seq = 0, month = 06, Yearmonth = null, ColCode = facility.tahunBulan06Kol, Day = facility.tahunBulan06Ht });
            collect.Add(new Collectibility() { seq = 0, month = 07, Yearmonth = null, ColCode = facility.tahunBulan07Kol, Day = facility.tahunBulan07Ht });
            collect.Add(new Collectibility() { seq = 0, month = 08, Yearmonth = null, ColCode = facility.tahunBulan08Kol, Day = facility.tahunBulan08Ht });
            collect.Add(new Collectibility() { seq = 0, month = 09, Yearmonth = null, ColCode = facility.tahunBulan09Kol, Day = facility.tahunBulan09Ht });
            collect.Add(new Collectibility() { seq = 0, month = 10, Yearmonth = null, ColCode = facility.tahunBulan10Kol, Day = facility.tahunBulan10Ht });
            collect.Add(new Collectibility() { seq = 0, month = 11, Yearmonth = null, ColCode = facility.tahunBulan11Kol, Day = facility.tahunBulan11Ht });
            collect.Add(new Collectibility() { seq = 0, month = 12, Yearmonth = null, ColCode = facility.tahunBulan12Kol, Day = facility.tahunBulan12Ht });

            // new
            collect.Add(new Collectibility() { seq = 1, month = 01, Yearmonth = null, ColCode = facility.tahunBulan13Kol, Day = facility.tahunBulan13Ht });
            collect.Add(new Collectibility() { seq = 1, month = 02, Yearmonth = null, ColCode = facility.tahunBulan14Kol, Day = facility.tahunBulan14Ht });
            collect.Add(new Collectibility() { seq = 1, month = 03, Yearmonth = null, ColCode = facility.tahunBulan15Kol, Day = facility.tahunBulan15Ht });
            collect.Add(new Collectibility() { seq = 1, month = 04, Yearmonth = null, ColCode = facility.tahunBulan16Kol, Day = facility.tahunBulan16Ht });
            collect.Add(new Collectibility() { seq = 1, month = 05, Yearmonth = null, ColCode = facility.tahunBulan17Kol, Day = facility.tahunBulan17Ht });
            collect.Add(new Collectibility() { seq = 1, month = 06, Yearmonth = null, ColCode = facility.tahunBulan18Kol, Day = facility.tahunBulan18Ht });
            collect.Add(new Collectibility() { seq = 1, month = 07, Yearmonth = null, ColCode = facility.tahunBulan19Kol, Day = facility.tahunBulan19Ht });
            collect.Add(new Collectibility() { seq = 1, month = 08, Yearmonth = null, ColCode = facility.tahunBulan20Kol, Day = facility.tahunBulan20Ht });
            collect.Add(new Collectibility() { seq = 1, month = 09, Yearmonth = null, ColCode = facility.tahunBulan21Kol, Day = facility.tahunBulan21Ht });
            collect.Add(new Collectibility() { seq = 1, month = 10, Yearmonth = null, ColCode = facility.tahunBulan22Kol, Day = facility.tahunBulan22Ht });
            collect.Add(new Collectibility() { seq = 1, month = 11, Yearmonth = null, ColCode = facility.tahunBulan23Kol, Day = facility.tahunBulan23Ht });
            collect.Add(new Collectibility() { seq = 1, month = 12, Yearmonth = null, ColCode = facility.tahunBulan24Kol, Day = facility.tahunBulan24Ht });

            return collect.Where(x => !string.IsNullOrEmpty(x.ColCode)).ToList();
        }

        static private List<Collectibility> facilityToCollectAktif(BaseFacility facility, DateTime dtDataLatest)
        {
            var collect = new List<Collectibility>();

            // ??
            collect.Add(new Collectibility() { seq = 24, month = 12, Yearmonth = null, ColCode = facility.tahunBulan24Kol, Day = facility.tahunBulan24Ht });

            collect.Add(new Collectibility() { seq = 23, month = 11, Yearmonth = null, ColCode = facility.tahunBulan23Kol, Day = facility.tahunBulan23Ht });
            collect.Add(new Collectibility() { seq = 22, month = 10, Yearmonth = null, ColCode = facility.tahunBulan22Kol, Day = facility.tahunBulan22Ht });
            collect.Add(new Collectibility() { seq = 21, month = 09, Yearmonth = null, ColCode = facility.tahunBulan21Kol, Day = facility.tahunBulan21Ht });
            collect.Add(new Collectibility() { seq = 20, month = 08, Yearmonth = null, ColCode = facility.tahunBulan20Kol, Day = facility.tahunBulan20Ht });
            collect.Add(new Collectibility() { seq = 19, month = 07, Yearmonth = null, ColCode = facility.tahunBulan19Kol, Day = facility.tahunBulan19Ht });
            collect.Add(new Collectibility() { seq = 18, month = 06, Yearmonth = null, ColCode = facility.tahunBulan18Kol, Day = facility.tahunBulan18Ht });
            collect.Add(new Collectibility() { seq = 17, month = 05, Yearmonth = null, ColCode = facility.tahunBulan17Kol, Day = facility.tahunBulan17Ht });
            collect.Add(new Collectibility() { seq = 16, month = 04, Yearmonth = null, ColCode = facility.tahunBulan16Kol, Day = facility.tahunBulan16Ht });
            collect.Add(new Collectibility() { seq = 15, month = 03, Yearmonth = null, ColCode = facility.tahunBulan15Kol, Day = facility.tahunBulan15Ht });
            collect.Add(new Collectibility() { seq = 14, month = 02, Yearmonth = null, ColCode = facility.tahunBulan14Kol, Day = facility.tahunBulan14Ht });
            collect.Add(new Collectibility() { seq = 13, month = 01, Yearmonth = null, ColCode = facility.tahunBulan13Kol, Day = facility.tahunBulan13Ht });

            collect.Add(new Collectibility() { seq = 12, month = 12, Yearmonth = null, ColCode = facility.tahunBulan12Kol, Day = facility.tahunBulan12Ht });
            collect.Add(new Collectibility() { seq = 11, month = 11, Yearmonth = null, ColCode = facility.tahunBulan11Kol, Day = facility.tahunBulan11Ht });
            collect.Add(new Collectibility() { seq = 10, month = 10, Yearmonth = null, ColCode = facility.tahunBulan10Kol, Day = facility.tahunBulan10Ht });
            collect.Add(new Collectibility() { seq = 9, month = 09, Yearmonth = null, ColCode = facility.tahunBulan09Kol, Day = facility.tahunBulan09Ht });
            collect.Add(new Collectibility() { seq = 8, month = 08, Yearmonth = null, ColCode = facility.tahunBulan08Kol, Day = facility.tahunBulan08Ht });
            collect.Add(new Collectibility() { seq = 7, month = 07, Yearmonth = null, ColCode = facility.tahunBulan07Kol, Day = facility.tahunBulan07Ht });
            collect.Add(new Collectibility() { seq = 6, month = 06, Yearmonth = null, ColCode = facility.tahunBulan06Kol, Day = facility.tahunBulan06Ht });
            collect.Add(new Collectibility() { seq = 5, month = 05, Yearmonth = null, ColCode = facility.tahunBulan05Kol, Day = facility.tahunBulan05Ht });
            collect.Add(new Collectibility() { seq = 4, month = 04, Yearmonth = null, ColCode = facility.tahunBulan04Kol, Day = facility.tahunBulan04Ht });
            collect.Add(new Collectibility() { seq = 3, month = 03, Yearmonth = null, ColCode = facility.tahunBulan03Kol, Day = facility.tahunBulan03Ht });
            collect.Add(new Collectibility() { seq = 2, month = 02, Yearmonth = null, ColCode = facility.tahunBulan02Kol, Day = facility.tahunBulan02Ht });
            collect.Add(new Collectibility() { seq = 1, month = 01, Yearmonth = null, ColCode = facility.tahunBulan01Kol, Day = facility.tahunBulan01Ht });

            // assume on dec 20, 24 fill be fill with last collect
            // else remove 24 and put at the end of list
            var to24 = DateTime.ParseExact("202012" + "01", "yyyyMMdd", null);
            if (dtDataLatest < to24)
            {
                collect.RemoveAll(x => x.seq == 24);
                collect.Add(new Collectibility() { seq = 24, month = 12, Yearmonth = null, ColCode = facility.tahunBulan24Kol, Day = facility.tahunBulan24Ht });
            }

            DateTime dtCreditPosition = DateTime.ParseExact(facility.tahunBulan + "01", "yyyyMMdd", null);

            // grab first seq
            //var _seq = collect.Where(x => x.month == dtDataLatest.Month && !string.IsNullOrEmpty(x.ColCode)).Select(x => x.seq).FirstOrDefault();
            var _seq = collect.Where(x => x.month == dtCreditPosition.Month && !string.IsNullOrEmpty(x.ColCode)).Select(x => x.seq).FirstOrDefault();

            // construct collect table based on dtDataLatest
            var collectFinal = new List<Collectibility>();
            for (int i = 0; i < 24; i++)
            {
                collectFinal.Add(new Collectibility()
                {
                    seq = i,
                    month = dtDataLatest.AddMonths(i * -1).Month,
                    Yearmonth = dtDataLatest.AddMonths(i * -1)
                });
            }

            // add value
            foreach (var item in collectFinal)
            {
                var _collect = collect.Where(x => !string.IsNullOrEmpty(x.ColCode) && x.seq == _seq).FirstOrDefault();
                if (_collect != null)
                {
                    if (_collect.month == item.month)
                    {
                        collect.Remove(_collect);
                        _seq--;

                        // reach end of seq, set to 24
                        if (_seq == 0)
                            _seq = 24;

                        item.ColCode = _collect.ColCode;
                        item.Day = _collect.Day;
                    }
                }
            }
            return collectFinal.ToList();
        }

        static private List<Collectibility> facilityToCollectLunas(BaseFacility facility, DateTime dtDataLatest)
        {
            var collect = new List<Collectibility>();

            // ??
            collect.Add(new Collectibility() { seq = 24, month = 24, Yearmonth = null, ColCode = facility.tahunBulan24Kol, Day = facility.tahunBulan24Ht });
            collect.Add(new Collectibility() { seq = 23, month = 23, Yearmonth = null, ColCode = facility.tahunBulan23Kol, Day = facility.tahunBulan23Ht });
            collect.Add(new Collectibility() { seq = 22, month = 22, Yearmonth = null, ColCode = facility.tahunBulan22Kol, Day = facility.tahunBulan22Ht });
            collect.Add(new Collectibility() { seq = 21, month = 21, Yearmonth = null, ColCode = facility.tahunBulan21Kol, Day = facility.tahunBulan21Ht });
            collect.Add(new Collectibility() { seq = 20, month = 20, Yearmonth = null, ColCode = facility.tahunBulan20Kol, Day = facility.tahunBulan20Ht });
            collect.Add(new Collectibility() { seq = 19, month = 19, Yearmonth = null, ColCode = facility.tahunBulan19Kol, Day = facility.tahunBulan19Ht });
            collect.Add(new Collectibility() { seq = 18, month = 18, Yearmonth = null, ColCode = facility.tahunBulan18Kol, Day = facility.tahunBulan18Ht });
            collect.Add(new Collectibility() { seq = 17, month = 17, Yearmonth = null, ColCode = facility.tahunBulan17Kol, Day = facility.tahunBulan17Ht });
            collect.Add(new Collectibility() { seq = 16, month = 16, Yearmonth = null, ColCode = facility.tahunBulan16Kol, Day = facility.tahunBulan16Ht });
            collect.Add(new Collectibility() { seq = 15, month = 15, Yearmonth = null, ColCode = facility.tahunBulan15Kol, Day = facility.tahunBulan15Ht });
            collect.Add(new Collectibility() { seq = 14, month = 14, Yearmonth = null, ColCode = facility.tahunBulan14Kol, Day = facility.tahunBulan14Ht });
            collect.Add(new Collectibility() { seq = 13, month = 13, Yearmonth = null, ColCode = facility.tahunBulan13Kol, Day = facility.tahunBulan13Ht });

            collect.Add(new Collectibility() { seq = 12, month = 12, Yearmonth = null, ColCode = facility.tahunBulan12Kol, Day = facility.tahunBulan12Ht });
            collect.Add(new Collectibility() { seq = 11, month = 11, Yearmonth = null, ColCode = facility.tahunBulan11Kol, Day = facility.tahunBulan11Ht });
            collect.Add(new Collectibility() { seq = 10, month = 10, Yearmonth = null, ColCode = facility.tahunBulan10Kol, Day = facility.tahunBulan10Ht });
            collect.Add(new Collectibility() { seq = 9, month = 9, Yearmonth = null, ColCode = facility.tahunBulan09Kol, Day = facility.tahunBulan09Ht });
            collect.Add(new Collectibility() { seq = 8, month = 8, Yearmonth = null, ColCode = facility.tahunBulan08Kol, Day = facility.tahunBulan08Ht });
            collect.Add(new Collectibility() { seq = 7, month = 7, Yearmonth = null, ColCode = facility.tahunBulan07Kol, Day = facility.tahunBulan07Ht });
            collect.Add(new Collectibility() { seq = 6, month = 6, Yearmonth = null, ColCode = facility.tahunBulan06Kol, Day = facility.tahunBulan06Ht });
            collect.Add(new Collectibility() { seq = 5, month = 5, Yearmonth = null, ColCode = facility.tahunBulan05Kol, Day = facility.tahunBulan05Ht });
            collect.Add(new Collectibility() { seq = 4, month = 4, Yearmonth = null, ColCode = facility.tahunBulan04Kol, Day = facility.tahunBulan04Ht });
            collect.Add(new Collectibility() { seq = 3, month = 3, Yearmonth = null, ColCode = facility.tahunBulan03Kol, Day = facility.tahunBulan03Ht });
            collect.Add(new Collectibility() { seq = 2, month = 2, Yearmonth = null, ColCode = facility.tahunBulan02Kol, Day = facility.tahunBulan02Ht });
            collect.Add(new Collectibility() { seq = 1, month = 1, Yearmonth = null, ColCode = facility.tahunBulan01Kol, Day = facility.tahunBulan01Ht });

            DateTime dtCreditPosition = DateTime.ParseExact(facility.tahunBulan + "01", "yyyyMMdd", null);

            // grab first seq
            //var _seq = collect.Where(x => x.month == dtDataLatest.Month && !string.IsNullOrEmpty(x.ColCode)).Select(x => x.seq).FirstOrDefault();
            //var _seq = collect.Where(x => x.month == dtCreditPosition.Month && !string.IsNullOrEmpty(x.ColCode)).Select(x => x.seq).FirstOrDefault();
            int _seq = -1;
            try
            {
                _seq = collect.Where(x => x.month != 24 && !string.IsNullOrEmpty(x.ColCode)).Max(x => x.seq);
                //_seq = collect.Where(x => !string.IsNullOrEmpty(x.ColCode)).Max(x => x.seq);
            }
            catch { }

            // construct collect table based on dtDataLatest
            var collectFinal = new List<Collectibility>();
            bool finishAddValue = false;
            for (int i = 0; i < 24; i++)
            {
                collectFinal.Add(new Collectibility()
                {
                    seq = i,
                    month = dtDataLatest.AddMonths(i * -1).Month,
                    Yearmonth = dtDataLatest.AddMonths(i * -1)
                });

                if (_seq == 0 && !finishAddValue)
                {
                    collectFinal[i].ColCode = collect.Where(x => x.seq == 24).FirstOrDefault().ColCode;
                    collectFinal[i].Day = collect.Where(x => x.seq == 24).FirstOrDefault().Day;
                    finishAddValue = true;
                }

                if (dtDataLatest.AddMonths(i * -1).ToString("yyyyMM") == dtCreditPosition.ToString("yyyyMM")
                    && _seq > 0 && !finishAddValue)
                {
                    if (!String.IsNullOrEmpty(collect.Where(x => x.seq == _seq).FirstOrDefault().ColCode))
                    {
                        collectFinal[i].ColCode = collect.Where(x => x.seq == _seq).FirstOrDefault().ColCode;
                        collectFinal[i].Day = collect.Where(x => x.seq == _seq).FirstOrDefault().Day;
                        _seq--;
                        dtCreditPosition = dtCreditPosition.AddMonths(-1);
                    }
                }

            }

            return collectFinal.ToList();
        }

        static private List<Collectibility> facilityToCollect2021(BaseFacility facility, DateTime dtDataLatest)
        {
            var collect = new List<Collectibility>();

            // ??
            collect.Add(new Collectibility() { seq = 1, month = 1, Yearmonth = null, ColCode = facility.tahunBulan01Kol, Day = facility.tahunBulan01Ht });
            collect.Add(new Collectibility() { seq = 2, month = 2, Yearmonth = null, ColCode = facility.tahunBulan02Kol, Day = facility.tahunBulan02Ht });
            collect.Add(new Collectibility() { seq = 3, month = 3, Yearmonth = null, ColCode = facility.tahunBulan03Kol, Day = facility.tahunBulan03Ht });
            collect.Add(new Collectibility() { seq = 4, month = 4, Yearmonth = null, ColCode = facility.tahunBulan04Kol, Day = facility.tahunBulan04Ht });
            collect.Add(new Collectibility() { seq = 5, month = 5, Yearmonth = null, ColCode = facility.tahunBulan05Kol, Day = facility.tahunBulan05Ht });
            collect.Add(new Collectibility() { seq = 6, month = 6, Yearmonth = null, ColCode = facility.tahunBulan06Kol, Day = facility.tahunBulan06Ht });
            collect.Add(new Collectibility() { seq = 7, month = 7, Yearmonth = null, ColCode = facility.tahunBulan07Kol, Day = facility.tahunBulan07Ht });
            collect.Add(new Collectibility() { seq = 8, month = 8, Yearmonth = null, ColCode = facility.tahunBulan08Kol, Day = facility.tahunBulan08Ht });
            collect.Add(new Collectibility() { seq = 9, month = 9, Yearmonth = null, ColCode = facility.tahunBulan09Kol, Day = facility.tahunBulan09Ht });
            collect.Add(new Collectibility() { seq = 10, month = 10, Yearmonth = null, ColCode = facility.tahunBulan10Kol, Day = facility.tahunBulan10Ht });
            collect.Add(new Collectibility() { seq = 11, month = 11, Yearmonth = null, ColCode = facility.tahunBulan11Kol, Day = facility.tahunBulan11Ht });
            collect.Add(new Collectibility() { seq = 12, month = 12, Yearmonth = null, ColCode = facility.tahunBulan12Kol, Day = facility.tahunBulan12Ht });

            collect.Add(new Collectibility() { seq = 13, month = 13, Yearmonth = null, ColCode = facility.tahunBulan13Kol, Day = facility.tahunBulan13Ht });
            collect.Add(new Collectibility() { seq = 14, month = 14, Yearmonth = null, ColCode = facility.tahunBulan14Kol, Day = facility.tahunBulan14Ht });
            collect.Add(new Collectibility() { seq = 15, month = 15, Yearmonth = null, ColCode = facility.tahunBulan15Kol, Day = facility.tahunBulan15Ht });
            collect.Add(new Collectibility() { seq = 16, month = 16, Yearmonth = null, ColCode = facility.tahunBulan16Kol, Day = facility.tahunBulan16Ht });
            collect.Add(new Collectibility() { seq = 17, month = 17, Yearmonth = null, ColCode = facility.tahunBulan17Kol, Day = facility.tahunBulan17Ht });
            collect.Add(new Collectibility() { seq = 18, month = 18, Yearmonth = null, ColCode = facility.tahunBulan18Kol, Day = facility.tahunBulan18Ht });
            collect.Add(new Collectibility() { seq = 19, month = 19, Yearmonth = null, ColCode = facility.tahunBulan19Kol, Day = facility.tahunBulan19Ht });
            collect.Add(new Collectibility() { seq = 20, month = 20, Yearmonth = null, ColCode = facility.tahunBulan20Kol, Day = facility.tahunBulan20Ht });
            collect.Add(new Collectibility() { seq = 21, month = 21, Yearmonth = null, ColCode = facility.tahunBulan21Kol, Day = facility.tahunBulan21Ht });
            collect.Add(new Collectibility() { seq = 22, month = 22, Yearmonth = null, ColCode = facility.tahunBulan22Kol, Day = facility.tahunBulan22Ht });
            collect.Add(new Collectibility() { seq = 23, month = 23, Yearmonth = null, ColCode = facility.tahunBulan23Kol, Day = facility.tahunBulan23Ht });
            collect.Add(new Collectibility() { seq = 24, month = 24, Yearmonth = null, ColCode = facility.tahunBulan24Kol, Day = facility.tahunBulan24Ht });

            int tahun = int.Parse(facility.tahunBulan.Substring(0, 4));
            int bulan = int.Parse(facility.tahunBulan.Substring(4, 2));
            bool tahunganjil = (tahun % 2 == 1) ? true : false;
            int jml_seq_atas = tahunganjil ? bulan : bulan + 12;
            int jml_seq_bawah = 24 - jml_seq_atas;
            int seq = 0;
            for (int i = jml_seq_atas - 1; i >= 0; i--)
            {
                collect[i].month = 24 - seq;
                seq++;
            }
            seq = 1;
            for (int i = jml_seq_atas; i < 24; i++)
            {
                collect[i].month = seq;
                seq++;
            }

            DateTime dtCreditPosition = DateTime.ParseExact(facility.tahunBulan + "01", "yyyyMMdd", null);
            DateTime dtDataLatest2 = dtDataLatest;
            int _seq = 24;
            var collectFinal = new List<Collectibility>();
            for (int i = 0; i < 24; i++)
            {
                collectFinal.Add(new Collectibility()
                {
                    seq = i,
                    month = dtDataLatest.AddMonths(i * -1).Month,
                    Yearmonth = dtDataLatest.AddMonths(i * -1),
                });

                if (dtCreditPosition.ToString("yyyyMM") == dtDataLatest2.ToString("yyyyMM"))
                {
                    collectFinal[i].ColCode = collect.Where(x => x.month == _seq).FirstOrDefault().ColCode;
                    collectFinal[i].Day = collect.Where(x => x.month == _seq).FirstOrDefault().Day;
                    dtCreditPosition = dtCreditPosition.AddMonths(-1);
                    _seq--;
                }
                dtDataLatest2 = dtDataLatest2.AddMonths(-1);
            }
            return collectFinal.ToList();
        }

        static private BaseFacility collectToFacility(List<Collectibility> colls)
        {
            var facility = new BaseFacility();
            //insert result
            //24
            facility.tahunBulan24 = colls[0].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan24Kol = colls[0].ColCode;
            facility.tahunBulan24Ht = colls[0].Day;
            //23
            facility.tahunBulan23 = colls[1].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan23Kol = colls[1].ColCode;
            facility.tahunBulan23Ht = colls[1].Day;
            //22
            facility.tahunBulan22 = colls[2].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan22Kol = colls[2].ColCode;
            facility.tahunBulan22Ht = colls[2].Day;
            //21
            facility.tahunBulan21 = colls[3].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan21Kol = colls[3].ColCode;
            facility.tahunBulan21Ht = colls[3].Day;
            //20
            facility.tahunBulan20 = colls[4].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan20Kol = colls[4].ColCode;
            facility.tahunBulan20Ht = colls[4].Day;
            //19
            facility.tahunBulan19 = colls[5].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan19Kol = colls[5].ColCode;
            facility.tahunBulan19Ht = colls[5].Day;
            //18
            facility.tahunBulan18 = colls[6].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan18Kol = colls[6].ColCode;
            facility.tahunBulan18Ht = colls[6].Day;
            //17
            facility.tahunBulan17 = colls[7].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan17Kol = colls[7].ColCode;
            facility.tahunBulan17Ht = colls[7].Day;
            //16
            facility.tahunBulan16 = colls[8].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan16Kol = colls[8].ColCode;
            facility.tahunBulan16Ht = colls[8].Day;
            //15
            facility.tahunBulan15 = colls[9].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan15Kol = colls[9].ColCode;
            facility.tahunBulan15Ht = colls[9].Day;
            //14
            facility.tahunBulan14 = colls[10].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan14Kol = colls[10].ColCode;
            facility.tahunBulan14Ht = colls[10].Day;
            //13
            facility.tahunBulan13 = colls[11].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan13Kol = colls[11].ColCode;
            facility.tahunBulan13Ht = colls[11].Day;
            //12
            facility.tahunBulan12 = colls[12].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan12Kol = colls[12].ColCode;
            facility.tahunBulan12Ht = colls[12].Day;
            //11
            facility.tahunBulan11 = colls[13].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan11Kol = colls[13].ColCode;
            facility.tahunBulan11Ht = colls[13].Day;
            //10
            facility.tahunBulan10 = colls[14].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan10Kol = colls[14].ColCode;
            facility.tahunBulan10Ht = colls[14].Day;
            //09
            facility.tahunBulan09 = colls[15].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan09Kol = colls[15].ColCode;
            facility.tahunBulan09Ht = colls[15].Day;
            //08
            facility.tahunBulan08 = colls[16].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan08Kol = colls[16].ColCode;
            facility.tahunBulan08Ht = colls[16].Day;
            //07
            facility.tahunBulan07 = colls[17].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan07Kol = colls[17].ColCode;
            facility.tahunBulan07Ht = colls[17].Day;
            //06
            facility.tahunBulan06 = colls[18].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan06Kol = colls[18].ColCode;
            facility.tahunBulan06Ht = colls[18].Day;
            //05
            facility.tahunBulan05 = colls[19].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan05Kol = colls[19].ColCode;
            facility.tahunBulan05Ht = colls[19].Day;
            //04
            facility.tahunBulan04 = colls[20].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan04Kol = colls[20].ColCode;
            facility.tahunBulan04Ht = colls[20].Day;
            //03
            facility.tahunBulan03 = colls[21].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan03Kol = colls[21].ColCode;
            facility.tahunBulan03Ht = colls[21].Day;
            //02
            facility.tahunBulan02 = colls[22].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan02Kol = colls[22].ColCode;
            facility.tahunBulan02Ht = colls[22].Day;
            //01
            facility.tahunBulan01 = colls[23].Yearmonth.Value.ToString("yyyyMM");
            facility.tahunBulan01Kol = colls[23].ColCode;
            facility.tahunBulan01Ht = colls[23].Day;

            return facility;
        }

        static private List<Collectibility> collectHandler(List<Collectibility> cols, DateTime dtDataLatest, DateTime dtCreditPositiont)
        {
            var colect = new List<Collectibility>();

            // remove empty
            cols.RemoveAll(x => string.IsNullOrEmpty(x.ColCode));

            // create dimension
            var seq = 0;
            for (int i = 0; i < 24; i++)
            {
                seq = dtDataLatest.AddMonths(i * -1).Month;
                var seqExists = colect.Where(x => x.seq == seq);

                // already exists
                if (seqExists.Count() > 0)
                    seq = seq + 12;

                colect.Add(new Collectibility() { seq = seq, month = dtDataLatest.AddMonths(i * -1).Month, Yearmonth = dtDataLatest.AddMonths(i * -1) });
            }

            // insert value, match between month in yearmonth with variable name. 
            // eg. tahunBulan01Kol = jan in last 12 month, tahunBulan13Kol = jan in first 12 month
            foreach (var col in colect)
            {
                if (col.Yearmonth <= dtCreditPositiont)
                {
                    var data = cols.Where(x => x.month == col.month).LastOrDefault();
                    cols.Remove(data);
                    if (data != null)
                    {
                        col.ColCode = data.ColCode;
                        col.Day = data.Day;
                    }
                }
            }

            return colect;
        }

        static void CopyCollect(object source, object destination)
        {
            foreach (PropertyInfo propA in source.GetType().GetProperties())
            {
                if (propA.Name.ToLower().Contains("tahunbulan0"))
                {
                    PropertyInfo propB = destination.GetType().GetProperty(propA.Name);
                    propB.SetValue(destination, propA.GetValue(source, null), null);
                }
                else if (propA.Name.ToLower().Contains("tahunbulan1"))
                {
                    PropertyInfo propB = destination.GetType().GetProperty(propA.Name);
                    propB.SetValue(destination, propA.GetValue(source, null), null);
                }
                else if (propA.Name.ToLower().Contains("tahunbulan2"))
                {
                    PropertyInfo propB = destination.GetType().GetProperty(propA.Name);
                    propB.SetValue(destination, propA.GetValue(source, null), null);
                }
            }
        }

        static private List<Credit> modifCollectCredit(List<Credit> credits, DateTime dtDataLatest)
        {
            foreach (var item in credits)
            {
                if (!string.IsNullOrEmpty(item.tahunBulan))
                {
                    //data antara item.dataYearMonth > 12 bulan kebelakang
                    DateTime dtCreditPosition = DateTime.ParseExact(item.tahunBulan + "01", "yyyyMMdd", null);
                    DateTime dtCreditOpen = item.tanggalMulai.Value;

                    // create list collect
                    //List<Collectibility> col;
                    //if (item.kodeKondisi == "02" || item.kodeKondisi == "08")
                    //    col = facilityToCollectLunas(item, dtDataLatest);
                    //else
                    //    col = facilityToCollectAktif(item, dtDataLatest);

                    List<Collectibility> col;
                    //if (int.Parse(item.tahunBulan) >= 202012)
                        col = facilityToCollect2021(item, dtDataLatest);
                    //else
                    //{
                    //    if (item.kodeKondisi == "02" || item.kodeKondisi == "05" || item.kodeKondisi == "06" || item.kodeKondisi == "07" ||
                    //        item.kodeKondisi == "08" || item.kodeKondisi == "09" || item.kodeKondisi == "12")
                    //        col = facilityToCollectLunas(item, dtDataLatest);
                    //    else
                    //        col = facilityToCollectAktif(item, dtDataLatest);
                    //}
                    //List<Collectibility> col = facilityToCollect(item);
                    // reorder collect
                    //col = collectHandler(col, dtDataLatest, dtCreditPosition);

                    // transform into basefacility
                    var cols = collectToFacility(col);

                    // copy result
                    CopyCollect(cols, item);
                }
            }

            return credits;
        }

        static private List<Others> modifCollectOthers(List<Others> others, DateTime dtDataLatest)
        {
            foreach (var item in others)
            {
                if (!string.IsNullOrEmpty(item.tahunBulan))
                {
                    //data antara item.dataYearMonth > 12 bulan kebelakang
                    DateTime dtCreditPosition = DateTime.ParseExact(item.tahunBulan + "01", "yyyyMMdd", null);
                    DateTime dtCreditOpen = item.tanggalMulai.Value;

                    // create list collect
                    //List<Collectibility> col;
                    //if (item.kodeKondisi != "00")
                    //    col = facilityToCollectLunas(item, dtDataLatest);
                    //else
                    //    col = facilityToCollectAktif(item, dtDataLatest);

                    List<Collectibility> col;
                    //if (int.Parse(item.tahunBulan) >= 202012)
                        col = facilityToCollect2021(item, dtDataLatest);
                    //else
                    //{
                    //    if (item.kodeKondisi == "02" || item.kodeKondisi == "05" || item.kodeKondisi == "06" || item.kodeKondisi == "07" ||
                    //        item.kodeKondisi == "08" || item.kodeKondisi == "09" || item.kodeKondisi == "12")
                    //        col = facilityToCollectLunas(item, dtDataLatest);
                    //    else
                    //        col = facilityToCollectAktif(item, dtDataLatest);
                    //}

                    //List<Collectibility> col = facilityToCollect(item);
                    // reorder collect
                    //col = collectHandler(col, dtDataLatest, dtCreditPosition);

                    // transform into basefacility
                    var cols = collectToFacility(col);

                    // copy result
                    CopyCollect(cols, item);
                }

            }

            return others;
        }

        static private List<Lc> modifCollectLcs(List<Lc> lcs, DateTime dtDataLatest)
        {
            foreach (var item in lcs)
            {
                if (!string.IsNullOrEmpty(item.tahunBulan))
                {
                    //data antara credit.dataYearMonth > 12 bulan kebelakang
                    DateTime dtCreditPosition = DateTime.ParseExact(item.tahunBulan + "01", "yyyyMMdd", null);
                    DateTime dtCreditOpen = item.tanggalKeluar.Value;

                    // create list collect
                    //List<Collectibility> col;
                    //if (item.kodeKondisi != "00")
                    //    col = facilityToCollectLunas(item, dtDataLatest);
                    //else
                    //    col = facilityToCollectAktif(item, dtDataLatest);

                    List<Collectibility> col;
                    //if (int.Parse(item.tahunBulan) >= 202012)
                        col = facilityToCollect2021(item, dtDataLatest);
                    //else
                    //{
                    //    if (item.kodeKondisi == "02" || item.kodeKondisi == "05" || item.kodeKondisi == "06" || item.kodeKondisi == "07" ||
                    //        item.kodeKondisi == "08" || item.kodeKondisi == "09" || item.kodeKondisi == "12")
                    //        col = facilityToCollectLunas(item, dtDataLatest);
                    //    else
                    //        col = facilityToCollectAktif(item, dtDataLatest);
                    //}

                    //List<Collectibility> col = facilityToCollect(item);
                    // reorder collect
                    //col = collectHandler(col, dtDataLatest, dtCreditPosition);

                    // transform into basefacility
                    var cols = collectToFacility(col);

                    // copy result
                    CopyCollect(cols, item);
                }

            }

            return lcs;
        }

        static private List<BankGuarantee> modifCollectBankGuarantee(List<BankGuarantee> bgs, DateTime dtDataLatest)
        {
            foreach (var item in bgs)
            {
                if (!string.IsNullOrEmpty(item.tahunBulan))
                {
                    //data antara credit.dataYearMonth > 12 bulan kebelakang
                    DateTime dtCreditPosition = DateTime.ParseExact(item.tahunBulan + "01", "yyyyMMdd", null);
                    DateTime dtCreditOpen = item.tanggalDiterbitkan.Value;

                    // create list collect
                    //List<Collectibility> col;
                    //if (item.kodeKondisi != "00")
                    //    col = facilityToCollectLunas(item, dtDataLatest);
                    //else
                    //    col = facilityToCollectAktif(item, dtDataLatest);

                    List<Collectibility> col;
                    //if (int.Parse(item.tahunBulan) >= 202012)
                        col = facilityToCollect2021(item, dtDataLatest);
                    //else
                    //{
                    //    if (item.kodeKondisi == "02" || item.kodeKondisi == "05" || item.kodeKondisi == "06" || item.kodeKondisi == "07" ||
                    //        item.kodeKondisi == "08" || item.kodeKondisi == "09" || item.kodeKondisi == "12")
                    //        col = facilityToCollectLunas(item, dtDataLatest);
                    //    else
                    //        col = facilityToCollectAktif(item, dtDataLatest);
                    //}

                    //List<Collectibility> col = facilityToCollect(item);
                    // reorder collect
                    //col = collectHandler(col, dtDataLatest, dtCreditPosition);

                    // transform into basefacility
                    var cols = collectToFacility(col);

                    // copy result
                    CopyCollect(cols, item);
                }

            }

            return bgs;
        }

        static private List<Security> modifCollectSecurity(List<Security> securities, DateTime dtDataLatest)
        {
            foreach (var item in securities)
            {
                if (!string.IsNullOrEmpty(item.tahunBulan))
                {
                    //data antara credit.dataYearMonth > 12 bulan kebelakang
                    DateTime dtCreditPosition = DateTime.ParseExact(item.tahunBulan + "01", "yyyyMMdd", null);
                    DateTime dtCreditOpen = item.tanggalTerbit.Value;

                    // create list collect
                    //List<Collectibility> col;
                    //if (item.kodeKondisi != "00")
                    //    col = facilityToCollectLunas(item, dtDataLatest);
                    //else
                    //    col = facilityToCollectAktif(item, dtDataLatest);

                    List<Collectibility> col;
                    //if (int.Parse(item.tahunBulan) >= 202012)
                        col = facilityToCollect2021(item, dtDataLatest);
                    //else
                    //{
                    //    if (item.kodeKondisi == "02" || item.kodeKondisi == "05" || item.kodeKondisi == "06" || item.kodeKondisi == "07" ||
                    //        item.kodeKondisi == "08" || item.kodeKondisi == "09" || item.kodeKondisi == "12")
                    //        col = facilityToCollectLunas(item, dtDataLatest);
                    //    else
                    //        col = facilityToCollectAktif(item, dtDataLatest);
                    //}

                    //List<Collectibility> col = facilityToCollect(item);
                    // reorder collect
                    //col = collectHandler(col, dtDataLatest, dtCreditPosition);

                    // transform into basefacility
                    var cols = collectToFacility(col);

                    // copy result
                    CopyCollect(cols, item);
                }

            }

            return securities;
        }

        static private void idebHeaderToDataTable(Header header, string FileId, string FileDetailId, DbConnection conn)
        {
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Fields, "kodeReferensiPengguna", header.kodeReferensiPengguna);
            staticFramework.saveNVC(Fields, "tanggalHasil", header.tanggalHasil);
            staticFramework.saveNVC(Fields, "idPermintaan", header.idPermintaan);
            staticFramework.saveNVC(Fields, "idPenggunaPermintaan", header.idPenggunaPermintaan);
            staticFramework.saveNVC(Fields, "dibuatOleh", header.dibuatOleh);
            staticFramework.saveNVC(Fields, "kodeLJKPermintaan", header.kodeLJKPermintaan);
            staticFramework.saveNVC(Fields, "kodeCabangPermintaan", header.kodeCabangPermintaan);
            staticFramework.saveNVC(Fields, "kodeTujuanPermintaan", header.kodeTujuanPermintaan);
            staticFramework.saveNVC(Fields, "tanggalPermintaan", header.tanggalPermintaan);
            staticFramework.saveNVC(Fields, "totalBagian", header.totalBagian);
            staticFramework.saveNVC(Fields, "nomorBagian", header.nomorBagian);
            staticFramework.save(Fields, Keys, "ideb_header", conn);
        }

        static private void idebIndividualToDataTable(Individual individual, string FileId, string FileDetailId, DbConnection conn)
        {
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Fields, "nomorLaporan", individual.nomorLaporan);
            staticFramework.saveNVC(Fields, "posisiDataTerakhir", individual.posisiDataTerakhir);
            staticFramework.saveNVC(Fields, "tanggalPermintaan", individual.tanggalPermintaan);
            staticFramework.save(Fields, Keys, "ideb_detail", conn);
        }

        static private void idebCorporateToDataTable(Corporate corporate, string FileId, string FileDetailId, DbConnection conn)
        {
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Fields, "nomorLaporan", corporate.nomorLaporan);
            staticFramework.saveNVC(Fields, "posisiDataTerakhir", corporate.posisiDataTerakhir);
            staticFramework.saveNVC(Fields, "tanggalPermintaan", corporate.tanggalPermintaan);
            staticFramework.save(Fields, Keys, "ideb_detail", conn);
        }

        static private void idebIndividualKeyWordToDataTable(IndividualKeyword individualKeyWord, string FileId, string FileDetailId, DbConnection conn)
        {
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Fields, "namaDebitur", individualKeyWord.namaDebitur);
            staticFramework.saveNVC(Fields, "noIdentitas", individualKeyWord.noIdentitas);
            staticFramework.saveNVC(Fields, "jenisKelamin", individualKeyWord.jenisKelamin);
            staticFramework.saveNVC(Fields, "jenisKelaminKet", individualKeyWord.jenisKelaminKet);
            staticFramework.saveNVC(Fields, "npwp", individualKeyWord.npwp);
            staticFramework.saveNVC(Fields, "tempatLahir", individualKeyWord.tempatLahir);
            staticFramework.saveNVC(Fields, "tanggalLahir", individualKeyWord.tanggalLahir);
            staticFramework.save(Fields, Keys, "ideb_keywords", conn);
        }

        static private void idebCorporateKeyWordToDataTable(CorporateKeyWord corporateKeyWord, string FileId, string FileDetailId, DbConnection conn)
        {
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Fields, "namaDebitur", corporateKeyWord.namaBadanUsaha);
            staticFramework.saveNVC(Fields, "npwp", corporateKeyWord.npwp);
            staticFramework.saveNVC(Fields, "tempatPendirian", corporateKeyWord.tempatPendirian);
            staticFramework.saveNVC(Fields, "tanggalAktaPendirian", corporateKeyWord.tanggalAktaPendirian);
            staticFramework.save(Fields, Keys, "ideb_keywords", conn);
        }

        static private void idebIndividualDebtorToDataTable(List<IndividualDebtor> individualDebtors, string FileId, string FileDetailId, DbConnection conn)
        {
            foreach (var individualDebtor in individualDebtors)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "DebtorId", id);
                staticFramework.saveNVC(Fields, "namaDebitur", individualDebtor.namaDebitur);
                staticFramework.saveNVC(Fields, "identitas", individualDebtor.identitas);
                staticFramework.saveNVC(Fields, "identitasKet", individualDebtor.identitasKet);
                staticFramework.saveNVC(Fields, "noIdentitas", individualDebtor.noIdentitas);
                staticFramework.saveNVC(Fields, "npwp", individualDebtor.npwp);
                staticFramework.saveNVC(Fields, "jenisKelamin", individualDebtor.jenisKelamin);
                staticFramework.saveNVC(Fields, "jenisKelaminKet", individualDebtor.jenisKelaminKet);
                staticFramework.saveNVC(Fields, "tempatLahir", individualDebtor.tempatLahir);
                staticFramework.saveNVC(Fields, "tanggalLahir", individualDebtor.tanggalLahir);
                staticFramework.saveNVC(Fields, "pelapor", individualDebtor.pelapor);
                staticFramework.saveNVC(Fields, "pelaporKet", individualDebtor.pelaporKet);
                staticFramework.saveNVC(Fields, "tanggalUpdate", individualDebtor.tanggalUpdate);
                staticFramework.saveNVC(Fields, "alamat", individualDebtor.alamat);
                staticFramework.saveNVC(Fields, "kelurahan", individualDebtor.kelurahan);
                staticFramework.saveNVC(Fields, "kecamatan", individualDebtor.kecamatan);
                staticFramework.saveNVC(Fields, "kabKota", individualDebtor.kabKota);
                staticFramework.saveNVC(Fields, "kabKotaKet", individualDebtor.kabKotaKet);
                staticFramework.saveNVC(Fields, "kodePos", individualDebtor.kodePos);
                staticFramework.saveNVC(Fields, "negara", individualDebtor.negara);
                staticFramework.saveNVC(Fields, "negaraKet", individualDebtor.negaraKet);
                staticFramework.saveNVC(Fields, "pekerjaan", individualDebtor.pekerjaan);
                staticFramework.saveNVC(Fields, "pekerjaanKet", individualDebtor.pekerjaanKet);
                staticFramework.saveNVC(Fields, "tempatBekerja", individualDebtor.tempatBekerja);
                staticFramework.saveNVC(Fields, "bidangUsaha", individualDebtor.bidangUsaha);
                staticFramework.saveNVC(Fields, "bidangUsahaKet", individualDebtor.bidangUsahaKet);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", individualDebtor.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "kodeGelarDebitur", individualDebtor.kodeGelarDebitur);
                staticFramework.saveNVC(Fields, "statusGelarDebitur", individualDebtor.statusGelarDebitur);
                staticFramework.save(Fields, Keys, "ideb_datapokokdebitur", conn);
            }
        }

        static private void idebCorporateDebtorToDataTable(List<CorporateDebtor> corporateDebtors, string FileId, string FileDetailId, DbConnection conn)
        {
            foreach (var corporateDebtor in corporateDebtors)
            {
                string id = Guid.NewGuid().ToString();

                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "DebtorId", id);
                staticFramework.saveNVC(Fields, "namaDebitur", corporateDebtor.namaDebitur);
                staticFramework.saveNVC(Fields, "namaLengkap", corporateDebtor.namaLengkap);
                staticFramework.saveNVC(Fields, "npwp", corporateDebtor.npwp);
                staticFramework.saveNVC(Fields, "bentukBu", corporateDebtor.bentukBu);
                staticFramework.saveNVC(Fields, "bentukBuKet", corporateDebtor.bentukBuKet);
                staticFramework.saveNVC(Fields, "goPublic", corporateDebtor.goPublic);
                staticFramework.saveNVC(Fields, "tempatPendirian", corporateDebtor.tempatPendirian);
                staticFramework.saveNVC(Fields, "noAktaPendirian", corporateDebtor.noAktaPendirian);
                staticFramework.saveNVC(Fields, "tglAktaPendirian", corporateDebtor.tglAktaPendirian);
                staticFramework.saveNVC(Fields, "pelapor", corporateDebtor.pelapor);
                staticFramework.saveNVC(Fields, "pelaporKet", corporateDebtor.pelaporKet);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", corporateDebtor.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "tanggalUpdate", corporateDebtor.tanggalUpdate);
                staticFramework.saveNVC(Fields, "alamat", corporateDebtor.alamat);
                staticFramework.saveNVC(Fields, "kelurahan", corporateDebtor.kelurahan);
                staticFramework.saveNVC(Fields, "kecamatan", corporateDebtor.kecamatan);
                staticFramework.saveNVC(Fields, "kabKota", corporateDebtor.kabKota);
                staticFramework.saveNVC(Fields, "kabKotaKet", corporateDebtor.kabKotaKet);
                staticFramework.saveNVC(Fields, "kodePos", corporateDebtor.kodePos);
                staticFramework.saveNVC(Fields, "negara", corporateDebtor.negara);
                staticFramework.saveNVC(Fields, "negaraKet", corporateDebtor.negaraKet);
                staticFramework.saveNVC(Fields, "noAktaTerakhir", corporateDebtor.noAktaTerakhir);
                staticFramework.saveNVC(Fields, "tglAktaTerakhir", corporateDebtor.tglAktaTerakhir);
                staticFramework.saveNVC(Fields, "sektorEkonomi", corporateDebtor.sektorEkonomi);
                staticFramework.saveNVC(Fields, "sektorEkonomiKet", corporateDebtor.sektorEkonomiKet);
                staticFramework.saveNVC(Fields, "pemeringkat", corporateDebtor.pemeringkat);
                staticFramework.saveNVC(Fields, "pemeringkatKet", corporateDebtor.pemeringkatKet);
                staticFramework.saveNVC(Fields, "peringkat", corporateDebtor.peringkat);
                staticFramework.saveNVC(Fields, "tanggalPemeringkatan", corporateDebtor.tanggalPemeringkatan);
                staticFramework.save(Fields, Keys, "ideb_datapokokdebitur", conn);

            }
        }

        static private void idebOffclsSharehldrGroupToDataTable(List<OffclsSharehldrGroup> offclsSharehldrGroups, string FileId, string FileDetailId, DbConnection conn)
        {
            foreach (var offclsSharehldrGroup in offclsSharehldrGroups)
            {
                foreach (var offclsSharehldr in offclsSharehldrGroup.pengurusPemilik)
                {
                    string id = Guid.NewGuid().ToString();

                    NameValueCollection Keys = new NameValueCollection();
                    NameValueCollection Fields = new NameValueCollection();
                    staticFramework.saveNVC(Keys, "FileId", FileId);
                    staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                    staticFramework.saveNVC(Keys, "ShareHolderId", id);

                    staticFramework.saveNVC(Fields, "kodeLJK", offclsSharehldrGroup.kodeLJK);
                    staticFramework.saveNVC(Fields, "namaLJK", offclsSharehldrGroup.namaLJK);
                    staticFramework.saveNVC(Fields, "tanggalUpdate", offclsSharehldrGroup.tanggalUpdate);

                    staticFramework.saveNVC(Fields, "namaSesuaiIdentitas", offclsSharehldr.namaSesuaiIdentitas);
                    staticFramework.saveNVC(Fields, "nomorIdentitas", offclsSharehldr.nomorIdentitas);
                    staticFramework.saveNVC(Fields, "kodeJenisKelamin", offclsSharehldr.kodeJenisKelamin);
                    staticFramework.saveNVC(Fields, "jenisKelamin", offclsSharehldr.jenisKelamin);
                    staticFramework.saveNVC(Fields, "kodePosisiPekerjaan", offclsSharehldr.kodePosisiPekerjaan);
                    staticFramework.saveNVC(Fields, "posisiPekerjaan", offclsSharehldr.posisiPekerjaan);
                    staticFramework.saveNVC(Fields, "prosentaseKepemilikan", offclsSharehldr.prosentaseKepemilikan);
                    staticFramework.saveNVC(Fields, "alamat", offclsSharehldr.alamat);
                    staticFramework.saveNVC(Fields, "kecamatan", offclsSharehldr.kecamatan);
                    staticFramework.saveNVC(Fields, "kodeKota", offclsSharehldr.kodeKota);
                    staticFramework.saveNVC(Fields, "kota", offclsSharehldr.kota);
                    staticFramework.saveNVC(Fields, "kelurahan", offclsSharehldr.kelurahan);
                    staticFramework.saveNVC(Fields, "kodeStatusPengurusPemilik", offclsSharehldr.kodeStatusPengurusPemilik);
                    staticFramework.saveNVC(Fields, "statusPengurusPemilik", offclsSharehldr.statusPengurusPemilik);

                    staticFramework.save(Fields, Keys, "ideb_penguruspemilik", conn);
                }
            }
        }

        static private void idebFacilitySummaryToDataTable(FacilitySummary facilitySummary, string FileId, string FileDetailId, DbConnection conn)
        {
            string id = Guid.NewGuid().ToString();
            NameValueCollection Keys = new NameValueCollection();
            NameValueCollection Fields = new NameValueCollection();
            staticFramework.saveNVC(Keys, "FileId", FileId);
            staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
            staticFramework.saveNVC(Keys, "FacilitySummaryId", id);

            staticFramework.saveNVC(Fields, "plafonEfektifKreditPembiayaan", facilitySummary.plafonEfektifKreditPembiayaan);
            staticFramework.saveNVC(Fields, "plafonEfektifLc", facilitySummary.plafonEfektifLc);
            staticFramework.saveNVC(Fields, "plafonEfektifGyd", facilitySummary.plafonEfektifGyd);
            staticFramework.saveNVC(Fields, "plafonEfektifSec", facilitySummary.plafonEfektifSec);
            staticFramework.saveNVC(Fields, "plafonEfektifLain", facilitySummary.plafonEfektifLain);
            staticFramework.saveNVC(Fields, "plafonEfektifTotal", facilitySummary.plafonEfektifTotal);
            staticFramework.saveNVC(Fields, "bakiDebetKreditPembiayaan", facilitySummary.bakiDebetKreditPembiayaan);
            staticFramework.saveNVC(Fields, "bakiDebetLc", facilitySummary.bakiDebetLc);
            staticFramework.saveNVC(Fields, "bakiDebetGyd", facilitySummary.bakiDebetGyd);
            staticFramework.saveNVC(Fields, "bakiDebetSec", facilitySummary.bakiDebetSec);
            staticFramework.saveNVC(Fields, "bakiDebetLain", facilitySummary.bakiDebetLain);
            staticFramework.saveNVC(Fields, "bakiDebetTotal", facilitySummary.bakiDebetTotal);
            staticFramework.saveNVC(Fields, "krediturBankUmum", facilitySummary.krediturBankUmum);
            staticFramework.saveNVC(Fields, "krediturBPRS", facilitySummary.krediturBPRS);
            staticFramework.saveNVC(Fields, "krediturLp", facilitySummary.krediturLp);
            staticFramework.saveNVC(Fields, "krediturLainnya", facilitySummary.krediturLainnya);
            staticFramework.saveNVC(Fields, "kualitasTerburuk", facilitySummary.kualitasTerburuk);
            staticFramework.saveNVC(Fields, "kualitasBulanDataTerburuk", facilitySummary.kualitasBulanDataTerburuk);

            staticFramework.save(Fields, Keys, "ideb_facilitysummary", conn);
        }

        static private void idebFacilityToDataTable(Facility facilities, string FileId, string FileDetailId, DbConnection conn)
        {
            #region credit
            foreach (var credit in facilities.kreditPembiayan)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", id);

                staticFramework.saveNVC(Fields, "noRekening", credit.noRekening);
                staticFramework.saveNVC(Fields, "kualitas", credit.kualitas);
                staticFramework.saveNVC(Fields, "kualitasKet", credit.kualitasKet);
                staticFramework.saveNVC(Fields, "kondisi", credit.kodeKondisi);
                staticFramework.saveNVC(Fields, "tanggalKondisi", credit.tanggalKondisi);
                staticFramework.saveNVC(Fields, "kondisiKet", credit.kondisiKet);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", credit.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "realisasiBulanBerjalan", credit.realisasiBulanBerjalan);
                staticFramework.saveNVC(Fields, "sifatKreditPembiayaan", credit.sifatKreditPembiayaan);
                staticFramework.saveNVC(Fields, "sifatKreditPembiayaanKet", credit.sifatKreditPembiayaanKet);
                staticFramework.saveNVC(Fields, "tanggalAwalKredit", credit.tanggalAwalKredit);
                staticFramework.saveNVC(Fields, "jenisKreditPembiayaan", credit.jenisKreditPembiayaan);
                staticFramework.saveNVC(Fields, "jenisKreditPembiayaanKet", credit.jenisKreditPembiayaanKet);
                staticFramework.saveNVC(Fields, "jenisPenggunaan", credit.jenisPenggunaan);
                staticFramework.saveNVC(Fields, "jenisPenggunaanKet", credit.jenisPenggunaanKet);
                staticFramework.saveNVC(Fields, "valutaKode", credit.kodeValuta);
                staticFramework.saveNVC(Fields, "plafon", credit.plafon);
                staticFramework.saveNVC(Fields, "tahunBulan", credit.tahunBulan);
                staticFramework.saveNVC(Fields, "kategoriDebiturKode", credit.kategoriDebiturKode);
                staticFramework.saveNVC(Fields, "kategoriDebiturKet", credit.kategoriDebiturKet);
                staticFramework.saveNVC(Fields, "keterangan", credit.keterangan);
                staticFramework.saveNVC(Fields, "tanggalJatuhTempo", credit.tanggalJatuhTempo);
                staticFramework.saveNVC(Fields, "sektorEkonomi", credit.sektorEkonomi);
                staticFramework.saveNVC(Fields, "sektorEkonomiKet", credit.sektorEkonomiKet);
                staticFramework.saveNVC(Fields, "akadKreditPembiayaan", credit.akadKreditPembiayaan);
                staticFramework.saveNVC(Fields, "akadKreditPembiayaanKet", credit.akadKreditPembiayaanKet);
                staticFramework.saveNVC(Fields, "kreditProgramPemerintah", credit.kreditProgramPemerintah);
                staticFramework.saveNVC(Fields, "kreditProgramPemerintahKet", credit.kreditProgramPemerintahKet);
                staticFramework.saveNVC(Fields, "tanggalAkadAwal", credit.tanggalAkadAwal);
                staticFramework.saveNVC(Fields, "noAkadAwal", credit.noAkadAwal);
                staticFramework.saveNVC(Fields, "sukuBungaImbalan", credit.sukuBungaImbalan);
                staticFramework.saveNVC(Fields, "jenisSukuBungaImbalan", credit.jenisSukuBungaImbalan);
                staticFramework.saveNVC(Fields, "jenisSukuBungaImbalanKet", credit.jenisSukuBungaImbalanKet);
                staticFramework.saveNVC(Fields, "frekPerpjganKreditPembiayaan", credit.frekPerpjganKreditPembiayaan);
                staticFramework.saveNVC(Fields, "tanggalAkadAkhir", credit.tanggalAkadAkhir);
                staticFramework.saveNVC(Fields, "noAkadAkhir", credit.noAkadAkhir);
                staticFramework.saveNVC(Fields, "denda", credit.denda);
                staticFramework.saveNVC(Fields, "ljk", credit.ljk);
                staticFramework.saveNVC(Fields, "ljkKet", credit.ljkKet);
                staticFramework.saveNVC(Fields, "bulan", credit.bulan);
                staticFramework.saveNVC(Fields, "tanggalMacet", credit.tanggalMacet);
                staticFramework.saveNVC(Fields, "kodeSebabMacet", credit.kodeSebabMacet);
                staticFramework.saveNVC(Fields, "sebabMacetKet", credit.sebabMacetKet);
                staticFramework.saveNVC(Fields, "cabang", credit.cabang);
                staticFramework.saveNVC(Fields, "cabangKet", credit.cabangKet);
                staticFramework.saveNVC(Fields, "nilaiDalamMataUangAsal", credit.nilaiDalamMataUangAsal);
                staticFramework.saveNVC(Fields, "plafonAwal", credit.plafonAwal);
                staticFramework.saveNVC(Fields, "bakiDebet", credit.bakiDebet);
                staticFramework.saveNVC(Fields, "jumlahHariTunggakan", credit.jumlahHariTunggakan);
                staticFramework.saveNVC(Fields, "frekuensiTunggakan", credit.frekuensiTunggakan);
                staticFramework.saveNVC(Fields, "tunggakanBunga", credit.tunggakanBunga);
                staticFramework.saveNVC(Fields, "tunggakanPokok", credit.tunggakanPokok);
                staticFramework.saveNVC(Fields, "nilaiProyek", credit.nilaiProyek);
                staticFramework.saveNVC(Fields, "lokasiProyek", credit.lokasiProyek);
                staticFramework.saveNVC(Fields, "lokasiProyekKet", credit.lokasiProyekKet);
                staticFramework.saveNVC(Fields, "tanggalRestrukturisasiAkhir", credit.tanggalRestrukturisasiAkhir);
                staticFramework.saveNVC(Fields, "frekuensiRestrukturisasi", credit.frekuensiRestrukturisasi);
                staticFramework.saveNVC(Fields, "kodeCaraRestrukturisasi", credit.kodeCaraRestrukturisasi);
                staticFramework.saveNVC(Fields, "restrukturisasiKet", credit.restrukturisasiKet);
                staticFramework.saveNVC(Fields, "tanggalMulai", credit.tanggalMulai);
                staticFramework.saveNVC(Fields, "tanggalUpdate", credit.tanggalUpdate);
                staticFramework.saveNVC(Fields, "tahun", credit.tahun);
                staticFramework.saveNVC(Fields, "tahunBulan01Kol", credit.tahunBulan01Kol);
                staticFramework.saveNVC(Fields, "tahunBulan01Ht", credit.tahunBulan01Ht);
                staticFramework.saveNVC(Fields, "tahunBulan01", credit.tahunBulan01);
                staticFramework.saveNVC(Fields, "tahunBulan02Kol", credit.tahunBulan02Kol);
                staticFramework.saveNVC(Fields, "tahunBulan02Ht", credit.tahunBulan02Ht);
                staticFramework.saveNVC(Fields, "tahunBulan02", credit.tahunBulan02);
                staticFramework.saveNVC(Fields, "tahunBulan03Kol", credit.tahunBulan03Kol);
                staticFramework.saveNVC(Fields, "tahunBulan03Ht", credit.tahunBulan03Ht);
                staticFramework.saveNVC(Fields, "tahunBulan03", credit.tahunBulan03);
                staticFramework.saveNVC(Fields, "tahunBulan04Kol", credit.tahunBulan04Kol);
                staticFramework.saveNVC(Fields, "tahunBulan04Ht", credit.tahunBulan04Ht);
                staticFramework.saveNVC(Fields, "tahunBulan04", credit.tahunBulan04);
                staticFramework.saveNVC(Fields, "tahunBulan05Kol", credit.tahunBulan05Kol);
                staticFramework.saveNVC(Fields, "tahunBulan05Ht", credit.tahunBulan05Ht);
                staticFramework.saveNVC(Fields, "tahunBulan05", credit.tahunBulan05);
                staticFramework.saveNVC(Fields, "tahunBulan06Kol", credit.tahunBulan06Kol);
                staticFramework.saveNVC(Fields, "tahunBulan06Ht", credit.tahunBulan06Ht);
                staticFramework.saveNVC(Fields, "tahunBulan06", credit.tahunBulan06);
                staticFramework.saveNVC(Fields, "tahunBulan07Kol", credit.tahunBulan07Kol);
                staticFramework.saveNVC(Fields, "tahunBulan07Ht", credit.tahunBulan07Ht);
                staticFramework.saveNVC(Fields, "tahunBulan07", credit.tahunBulan07);
                staticFramework.saveNVC(Fields, "tahunBulan08Kol", credit.tahunBulan08Kol);
                staticFramework.saveNVC(Fields, "tahunBulan08Ht", credit.tahunBulan08Ht);
                staticFramework.saveNVC(Fields, "tahunBulan08", credit.tahunBulan08);
                staticFramework.saveNVC(Fields, "tahunBulan09Kol", credit.tahunBulan09Kol);
                staticFramework.saveNVC(Fields, "tahunBulan09Ht", credit.tahunBulan09Ht);
                staticFramework.saveNVC(Fields, "tahunBulan09", credit.tahunBulan09);
                staticFramework.saveNVC(Fields, "tahunBulan10Kol", credit.tahunBulan10Kol);
                staticFramework.saveNVC(Fields, "tahunBulan10Ht", credit.tahunBulan10Ht);
                staticFramework.saveNVC(Fields, "tahunBulan10", credit.tahunBulan10);
                staticFramework.saveNVC(Fields, "tahunBulan11Kol", credit.tahunBulan11Kol);
                staticFramework.saveNVC(Fields, "tahunBulan11Ht", credit.tahunBulan11Ht);
                staticFramework.saveNVC(Fields, "tahunBulan11", credit.tahunBulan11);
                staticFramework.saveNVC(Fields, "tahunBulan12Kol", credit.tahunBulan12Kol);
                staticFramework.saveNVC(Fields, "tahunBulan12Ht", credit.tahunBulan12Ht);
                staticFramework.saveNVC(Fields, "tahunBulan12", credit.tahunBulan12);

                staticFramework.save(Fields, Keys, "ideb_kredit", conn);

                if (credit.agunan != null)
                    idebCollateralToDataTable(credit.agunan, FileId, FileDetailId, id, conn);
                if (credit.penjamin != null)
                    idebGuarantorToDataTable(credit.penjamin, FileId, FileDetailId, id, conn);
            }
            #endregion

            #region lc
            foreach (var lc in facilities.lcs)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", id);

                staticFramework.saveNVC(Fields, "noLc", lc.noLc);
                staticFramework.saveNVC(Fields, "kualitas", lc.kualitas);
                staticFramework.saveNVC(Fields, "kualitasKet", lc.kualitasKet);
                staticFramework.saveNVC(Fields, "kondisi", lc.kodeKondisi);
                staticFramework.saveNVC(Fields, "tanggalKondisi", lc.tanggalKondisi);
                staticFramework.saveNVC(Fields, "kondisiKet", lc.kondisiKet);
                staticFramework.saveNVC(Fields, "bankBeneficiary", lc.bankBeneficiary);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", lc.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "valuta", lc.kodeValuta);
                staticFramework.saveNVC(Fields, "tahunBulan", lc.tahunBulan);
                staticFramework.saveNVC(Fields, "bulan", lc.bulan);
                staticFramework.saveNVC(Fields, "tahun", lc.tahun);
                staticFramework.saveNVC(Fields, "keterangan", lc.keterangan);
                staticFramework.saveNVC(Fields, "tanggalAkadAwal", lc.tanggalAkadAwal);
                staticFramework.saveNVC(Fields, "noAkadAwal", lc.noAkadAwal);
                staticFramework.saveNVC(Fields, "tanggalAkadAkhir", lc.tanggalAkadAkhir);
                staticFramework.saveNVC(Fields, "noAkadAkhir", lc.noAkadAkhir);
                staticFramework.saveNVC(Fields, "lcAmount", lc.lcAmount);
                staticFramework.saveNVC(Fields, "setoraJaminan", lc.setoraJaminan);
                staticFramework.saveNVC(Fields, "tanggalWanPrestasi", lc.tanggalWanPrestasi);
                staticFramework.saveNVC(Fields, "tanggalJthTempo", lc.tanggalJthTempo);
                staticFramework.saveNVC(Fields, "tanggalKeluar", lc.tanggalKeluar);
                staticFramework.saveNVC(Fields, "plafon", lc.plafon);
                staticFramework.saveNVC(Fields, "tujuanLc", lc.tujuanLc);
                staticFramework.saveNVC(Fields, "tujuanLcKet", lc.tujuanLcKet);
                staticFramework.saveNVC(Fields, "jenisLc", lc.jenisLc);
                staticFramework.saveNVC(Fields, "jenisLcKet", lc.jenisLcKet);
                staticFramework.saveNVC(Fields, "ljk", lc.ljk);
                staticFramework.saveNVC(Fields, "ljkKet", lc.ljkKet);
                staticFramework.saveNVC(Fields, "tahunBulan01Kol", lc.tahunBulan01Kol);
                staticFramework.saveNVC(Fields, "tahunBulan01Ht", lc.tahunBulan01Ht);
                staticFramework.saveNVC(Fields, "tahunBulan01", lc.tahunBulan01);
                staticFramework.saveNVC(Fields, "tahunBulan02Kol", lc.tahunBulan02Kol);
                staticFramework.saveNVC(Fields, "tahunBulan02Ht", lc.tahunBulan02Ht);
                staticFramework.saveNVC(Fields, "tahunBulan02", lc.tahunBulan02);
                staticFramework.saveNVC(Fields, "tahunBulan03Kol", lc.tahunBulan03Kol);
                staticFramework.saveNVC(Fields, "tahunBulan03Ht", lc.tahunBulan03Ht);
                staticFramework.saveNVC(Fields, "tahunBulan03", lc.tahunBulan03);
                staticFramework.saveNVC(Fields, "tahunBulan04Kol", lc.tahunBulan04Kol);
                staticFramework.saveNVC(Fields, "tahunBulan04Ht", lc.tahunBulan04Ht);
                staticFramework.saveNVC(Fields, "tahunBulan04", lc.tahunBulan04);
                staticFramework.saveNVC(Fields, "tahunBulan05Kol", lc.tahunBulan05Kol);
                staticFramework.saveNVC(Fields, "tahunBulan05Ht", lc.tahunBulan05Ht);
                staticFramework.saveNVC(Fields, "tahunBulan05", lc.tahunBulan05);
                staticFramework.saveNVC(Fields, "tahunBulan06Kol", lc.tahunBulan06Kol);
                staticFramework.saveNVC(Fields, "tahunBulan06Ht", lc.tahunBulan06Ht);
                staticFramework.saveNVC(Fields, "tahunBulan06", lc.tahunBulan06);
                staticFramework.saveNVC(Fields, "tahunBulan07Kol", lc.tahunBulan07Kol);
                staticFramework.saveNVC(Fields, "tahunBulan07Ht", lc.tahunBulan07Ht);
                staticFramework.saveNVC(Fields, "tahunBulan07", lc.tahunBulan07);
                staticFramework.saveNVC(Fields, "tahunBulan08Kol", lc.tahunBulan08Kol);
                staticFramework.saveNVC(Fields, "tahunBulan08Ht", lc.tahunBulan08Ht);
                staticFramework.saveNVC(Fields, "tahunBulan08", lc.tahunBulan08);
                staticFramework.saveNVC(Fields, "tahunBulan09Kol", lc.tahunBulan09Kol);
                staticFramework.saveNVC(Fields, "tahunBulan09Ht", lc.tahunBulan09Ht);
                staticFramework.saveNVC(Fields, "tahunBulan09", lc.tahunBulan09);
                staticFramework.saveNVC(Fields, "tahunBulan10Kol", lc.tahunBulan10Kol);
                staticFramework.saveNVC(Fields, "tahunBulan10Ht", lc.tahunBulan10Ht);
                staticFramework.saveNVC(Fields, "tahunBulan10", lc.tahunBulan10);
                staticFramework.saveNVC(Fields, "tahunBulan11Kol", lc.tahunBulan11Kol);
                staticFramework.saveNVC(Fields, "tahunBulan11Ht", lc.tahunBulan11Ht);
                staticFramework.saveNVC(Fields, "tahunBulan11", lc.tahunBulan11);
                staticFramework.saveNVC(Fields, "tahunBulan12Kol", lc.tahunBulan12Kol);
                staticFramework.saveNVC(Fields, "tahunBulan12Ht", lc.tahunBulan12Ht);
                staticFramework.saveNVC(Fields, "tahunBulan12", lc.tahunBulan12);
                staticFramework.saveNVC(Fields, "cabang", lc.cabang);
                staticFramework.saveNVC(Fields, "cabangKet", lc.cabangKet);
                staticFramework.saveNVC(Fields, "tanggalUpdate", lc.tanggalUpdate);

                staticFramework.save(Fields, Keys, "ideb_lc", conn);

                if (lc.agunan != null)
                    idebCollateralToDataTable(lc.agunan, FileId, FileDetailId, id, conn);
                if (lc.penjamin != null)
                    idebGuarantorToDataTable(lc.penjamin, FileId, FileDetailId, id, conn);

            }
            #endregion

            #region bg
            foreach (var bg in facilities.garansiYgDiberikan)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", id);

                staticFramework.saveNVC(Fields, "accountNo", bg.accountNo);
                staticFramework.saveNVC(Fields, "kodeTujuanGaransi", bg.kodeTujuanGaransi);
                staticFramework.saveNVC(Fields, "tujuanGaransiKet", bg.tujuanGaransiKet);
                staticFramework.saveNVC(Fields, "jenisGaransi", bg.jenisGaransi);
                staticFramework.saveNVC(Fields, "jenisGaransiKet", bg.jenisGaransiKet);
                staticFramework.saveNVC(Fields, "nominalBg", bg.nominalBg);
                staticFramework.saveNVC(Fields, "tanggalWanPrestasi", bg.tanggalWanPrestasi);
                staticFramework.saveNVC(Fields, "tanggalJatuhTempo", bg.tanggalJatuhTempo);
                staticFramework.saveNVC(Fields, "tanggalDiterbitkan", bg.tanggalDiterbitkan);
                staticFramework.saveNVC(Fields, "plafon", bg.plafon);
                staticFramework.saveNVC(Fields, "setoranJaminan", bg.setoranJaminan);
                staticFramework.saveNVC(Fields, "kualitas", bg.kualitas);
                staticFramework.saveNVC(Fields, "kualitasKet", bg.kualitasKet);
                staticFramework.saveNVC(Fields, "kodeKondisi", bg.kodeKondisi);
                staticFramework.saveNVC(Fields, "tanggalKondisi", bg.tanggalKondisi);
                staticFramework.saveNVC(Fields, "kondisiKet", bg.kondisiKet);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", bg.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "kodeValuta", bg.kodeValuta);
                staticFramework.saveNVC(Fields, "tahunBulan", bg.tahunBulan);
                staticFramework.saveNVC(Fields, "bulan", bg.bulan);
                staticFramework.saveNVC(Fields, "tahun", bg.tahun);
                staticFramework.saveNVC(Fields, "keterangan", bg.keterangan);
                staticFramework.saveNVC(Fields, "namaYangDijamin", bg.namaYangDijamin);
                staticFramework.saveNVC(Fields, "tanggalAkadAwal", bg.tanggalAkadAwal);
                staticFramework.saveNVC(Fields, "noAkadAwal", bg.noAkadAwal);
                staticFramework.saveNVC(Fields, "tanggalAkadAkhir", bg.tanggalAkadAkhir);
                staticFramework.saveNVC(Fields, "noAkadAkhir", bg.noAkadAkhir);
                staticFramework.saveNVC(Fields, "ljk", bg.ljk);
                staticFramework.saveNVC(Fields, "ljkKet", bg.ljkKet);
                staticFramework.saveNVC(Fields, "tahunBulan01Kol", bg.tahunBulan01Kol);
                staticFramework.saveNVC(Fields, "tahunBulan01Ht", bg.tahunBulan01Ht);
                staticFramework.saveNVC(Fields, "tahunBulan01", bg.tahunBulan01);
                staticFramework.saveNVC(Fields, "tahunBulan02Kol", bg.tahunBulan02Kol);
                staticFramework.saveNVC(Fields, "tahunBulan02Ht", bg.tahunBulan02Ht);
                staticFramework.saveNVC(Fields, "tahunBulan02", bg.tahunBulan02);
                staticFramework.saveNVC(Fields, "tahunBulan03Kol", bg.tahunBulan03Kol);
                staticFramework.saveNVC(Fields, "tahunBulan03Ht", bg.tahunBulan03Ht);
                staticFramework.saveNVC(Fields, "tahunBulan03", bg.tahunBulan03);
                staticFramework.saveNVC(Fields, "tahunBulan04Kol", bg.tahunBulan04Kol);
                staticFramework.saveNVC(Fields, "tahunBulan04Ht", bg.tahunBulan04Ht);
                staticFramework.saveNVC(Fields, "tahunBulan04", bg.tahunBulan04);
                staticFramework.saveNVC(Fields, "tahunBulan05Kol", bg.tahunBulan05Kol);
                staticFramework.saveNVC(Fields, "tahunBulan05Ht", bg.tahunBulan05Ht);
                staticFramework.saveNVC(Fields, "tahunBulan05", bg.tahunBulan05);
                staticFramework.saveNVC(Fields, "tahunBulan06Kol", bg.tahunBulan06Kol);
                staticFramework.saveNVC(Fields, "tahunBulan06Ht", bg.tahunBulan06Ht);
                staticFramework.saveNVC(Fields, "tahunBulan06", bg.tahunBulan06);
                staticFramework.saveNVC(Fields, "tahunBulan07Kol", bg.tahunBulan07Kol);
                staticFramework.saveNVC(Fields, "tahunBulan07Ht", bg.tahunBulan07Ht);
                staticFramework.saveNVC(Fields, "tahunBulan07", bg.tahunBulan07);
                staticFramework.saveNVC(Fields, "tahunBulan08Kol", bg.tahunBulan08Kol);
                staticFramework.saveNVC(Fields, "tahunBulan08Ht", bg.tahunBulan08Ht);
                staticFramework.saveNVC(Fields, "tahunBulan08", bg.tahunBulan08);
                staticFramework.saveNVC(Fields, "tahunBulan09Kol", bg.tahunBulan09Kol);
                staticFramework.saveNVC(Fields, "tahunBulan09Ht", bg.tahunBulan09Ht);
                staticFramework.saveNVC(Fields, "tahunBulan09", bg.tahunBulan09);
                staticFramework.saveNVC(Fields, "tahunBulan10Kol", bg.tahunBulan10Kol);
                staticFramework.saveNVC(Fields, "tahunBulan10Ht", bg.tahunBulan10Ht);
                staticFramework.saveNVC(Fields, "tahunBulan10", bg.tahunBulan10);
                staticFramework.saveNVC(Fields, "tahunBulan11Kol", bg.tahunBulan11Kol);
                staticFramework.saveNVC(Fields, "tahunBulan11Ht", bg.tahunBulan11Ht);
                staticFramework.saveNVC(Fields, "tahunBulan11", bg.tahunBulan11);
                staticFramework.saveNVC(Fields, "tahunBulan12Kol", bg.tahunBulan12Kol);
                staticFramework.saveNVC(Fields, "tahunBulan12Ht", bg.tahunBulan12Ht);
                staticFramework.saveNVC(Fields, "tahunBulan12", bg.tahunBulan12);
                staticFramework.saveNVC(Fields, "cabang", bg.cabang);
                staticFramework.saveNVC(Fields, "cabangKet", bg.cabangKet);
                staticFramework.saveNVC(Fields, "tanggalUpdate", bg.tanggalUpdate);

                staticFramework.save(Fields, Keys, "ideb_bankgaransi", conn);

                if (bg.agunan != null)
                    idebCollateralToDataTable(bg.agunan, FileId, FileDetailId, id, conn);
                if (bg.penjamin != null)
                    idebGuarantorToDataTable(bg.penjamin, FileId, FileDetailId, id, conn);

            }
            #endregion

            #region sb
            foreach (var sb in facilities.suratBerharga)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", id);

                staticFramework.saveNVC(Fields, "nominalSb", sb.nominalSb);
                staticFramework.saveNVC(Fields, "noSuratBerharga", sb.noSuratBerharga);
                staticFramework.saveNVC(Fields, "jenisSuratBerharga", sb.jenisSuratBerharga);
                staticFramework.saveNVC(Fields, "jenisSuratBerhargaKet", sb.jenisSuratBerhargaKet);
                staticFramework.saveNVC(Fields, "sovereignRate", sb.sovereignRate);
                staticFramework.saveNVC(Fields, "listing", sb.listing);
                staticFramework.saveNVC(Fields, "peringkatSuratBerharga", sb.peringkatSuratBerharga);
                staticFramework.saveNVC(Fields, "tujuanKepemilikan", sb.tujuanKepemilikan);
                staticFramework.saveNVC(Fields, "tujuanKepemilikanKet", sb.tujuanKepemilikanKet);
                staticFramework.saveNVC(Fields, "tanggalTerbit", sb.tanggalTerbit);
                staticFramework.saveNVC(Fields, "tanggalJatuhTempo", sb.tanggalJatuhTempo);
                staticFramework.saveNVC(Fields, "sukuBungaImbalan", sb.sukuBungaImbalan);
                staticFramework.saveNVC(Fields, "kodeValuta", sb.kodeValuta);
                staticFramework.saveNVC(Fields, "kualitas", sb.kualitas);
                staticFramework.saveNVC(Fields, "kualitasKet", sb.kualitasKet);
                staticFramework.saveNVC(Fields, "jumlahHariTunggakan", sb.jumlahHariTunggakan);
                staticFramework.saveNVC(Fields, "nilaiDalamMataUangAsal", sb.nilaiDalamMataUangAsal);
                staticFramework.saveNVC(Fields, "nilaiPasar", sb.nilaiPasar);
                staticFramework.saveNVC(Fields, "nilaiPerolehan", sb.nilaiPerolehan);
                staticFramework.saveNVC(Fields, "tunggakan", sb.tunggakan);
                staticFramework.saveNVC(Fields, "tanggalMacet", sb.tanggalMacet);
                staticFramework.saveNVC(Fields, "kodeSebabMacet", sb.kodeSebabMacet);
                staticFramework.saveNVC(Fields, "sebabMacetKet", sb.sebabMacetKet);
                staticFramework.saveNVC(Fields, "kondisi", sb.kodeKondisi);
                staticFramework.saveNVC(Fields, "kondisiKet", sb.kondisiKet);
                staticFramework.saveNVC(Fields, "tanggalKondisi", sb.tanggalKondisi);
                staticFramework.saveNVC(Fields, "keterangan", sb.keterangan);
                staticFramework.saveNVC(Fields, "ljk", sb.ljk);
                staticFramework.saveNVC(Fields, "ljkKet", sb.ljkKet);
                staticFramework.saveNVC(Fields, "cabang", sb.cabang);
                staticFramework.saveNVC(Fields, "cabangKet", sb.cabangKet);
                staticFramework.saveNVC(Fields, "tanggalUpdate", sb.tanggalUpdate);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", sb.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "tahunBulan", sb.tahunBulan);
                staticFramework.saveNVC(Fields, "bulan", sb.bulan);
                staticFramework.saveNVC(Fields, "tahun", sb.tahun);
                staticFramework.saveNVC(Fields, "tahunBulan01Kol", sb.tahunBulan01Kol);
                staticFramework.saveNVC(Fields, "tahunBulan01Ht", sb.tahunBulan01Ht);
                staticFramework.saveNVC(Fields, "tahunBulan01", sb.tahunBulan01);
                staticFramework.saveNVC(Fields, "tahunBulan02Kol", sb.tahunBulan02Kol);
                staticFramework.saveNVC(Fields, "tahunBulan02Ht", sb.tahunBulan02Ht);
                staticFramework.saveNVC(Fields, "tahunBulan02", sb.tahunBulan02);
                staticFramework.saveNVC(Fields, "tahunBulan03Kol", sb.tahunBulan03Kol);
                staticFramework.saveNVC(Fields, "tahunBulan03Ht", sb.tahunBulan03Ht);
                staticFramework.saveNVC(Fields, "tahunBulan03", sb.tahunBulan03);
                staticFramework.saveNVC(Fields, "tahunBulan04Kol", sb.tahunBulan04Kol);
                staticFramework.saveNVC(Fields, "tahunBulan04Ht", sb.tahunBulan04Ht);
                staticFramework.saveNVC(Fields, "tahunBulan04", sb.tahunBulan04);
                staticFramework.saveNVC(Fields, "tahunBulan05Kol", sb.tahunBulan05Kol);
                staticFramework.saveNVC(Fields, "tahunBulan05Ht", sb.tahunBulan05Ht);
                staticFramework.saveNVC(Fields, "tahunBulan05", sb.tahunBulan05);
                staticFramework.saveNVC(Fields, "tahunBulan06Kol", sb.tahunBulan06Kol);
                staticFramework.saveNVC(Fields, "tahunBulan06Ht", sb.tahunBulan06Ht);
                staticFramework.saveNVC(Fields, "tahunBulan06", sb.tahunBulan06);
                staticFramework.saveNVC(Fields, "tahunBulan07Kol", sb.tahunBulan07Kol);
                staticFramework.saveNVC(Fields, "tahunBulan07Ht", sb.tahunBulan07Ht);
                staticFramework.saveNVC(Fields, "tahunBulan07", sb.tahunBulan07);
                staticFramework.saveNVC(Fields, "tahunBulan08Kol", sb.tahunBulan08Kol);
                staticFramework.saveNVC(Fields, "tahunBulan08Ht", sb.tahunBulan08Ht);
                staticFramework.saveNVC(Fields, "tahunBulan08", sb.tahunBulan08);
                staticFramework.saveNVC(Fields, "tahunBulan09Kol", sb.tahunBulan09Kol);
                staticFramework.saveNVC(Fields, "tahunBulan09Ht", sb.tahunBulan09Ht);
                staticFramework.saveNVC(Fields, "tahunBulan09", sb.tahunBulan09);
                staticFramework.saveNVC(Fields, "tahunBulan10Kol", sb.tahunBulan10Kol);
                staticFramework.saveNVC(Fields, "tahunBulan10Ht", sb.tahunBulan10Ht);
                staticFramework.saveNVC(Fields, "tahunBulan10", sb.tahunBulan10);
                staticFramework.saveNVC(Fields, "tahunBulan11Kol", sb.tahunBulan11Kol);
                staticFramework.saveNVC(Fields, "tahunBulan11Ht", sb.tahunBulan11Ht);
                staticFramework.saveNVC(Fields, "tahunBulan11", sb.tahunBulan11);
                staticFramework.saveNVC(Fields, "tahunBulan12Kol", sb.tahunBulan12Kol);
                staticFramework.saveNVC(Fields, "tahunBulan12Ht", sb.tahunBulan12Ht);
                staticFramework.saveNVC(Fields, "tahunBulan12", sb.tahunBulan12);

                staticFramework.save(Fields, Keys, "ideb_suratberharga", conn);

                if (sb.agunan != null)
                    idebCollateralToDataTable(sb.agunan, FileId, FileDetailId, id, conn);
                if (sb.penjamin != null)
                    idebGuarantorToDataTable(sb.penjamin, FileId, FileDetailId, id, conn);

            }
            #endregion

            #region other
            foreach (var other in facilities.fasilitasLain)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", id);

                staticFramework.saveNVC(Fields, "noRekening", other.noRekening);
                staticFramework.saveNVC(Fields, "kualitas", other.kualitas);
                staticFramework.saveNVC(Fields, "kualitasKet", other.kualitasKet);
                staticFramework.saveNVC(Fields, "kodeKondisi", other.kodeKondisi);
                staticFramework.saveNVC(Fields, "kondisiKet", other.kondisiKet);
                staticFramework.saveNVC(Fields, "tanggalKondisi", other.tanggalKondisi);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", other.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "kodeValuta", other.kodeValuta);
                staticFramework.saveNVC(Fields, "tahunBulan", other.tahunBulan);
                staticFramework.saveNVC(Fields, "bulan", other.bulan);
                staticFramework.saveNVC(Fields, "tahun", other.tahun);
                staticFramework.saveNVC(Fields, "keterangan", other.keterangan);
                staticFramework.saveNVC(Fields, "nominalJumlahKwajibanIDR", other.nominalJumlahKwajibanIDR);
                staticFramework.saveNVC(Fields, "tanggalJatuhTempo", other.tanggalJatuhTempo);
                staticFramework.saveNVC(Fields, "tanggalMulai", other.tanggalMulai);
                staticFramework.saveNVC(Fields, "kodeJenisFasilitas", other.kodeJenisFasilitas);
                staticFramework.saveNVC(Fields, "jenisFasilitasKet", other.jenisFasilitasKet);
                staticFramework.saveNVC(Fields, "sukuBungaImbalan", other.sukuBungaImbalan);
                staticFramework.saveNVC(Fields, "ljk", other.ljk);
                staticFramework.saveNVC(Fields, "ljkKet", other.ljkKet);
                staticFramework.saveNVC(Fields, "tahunBulan01Kol", other.tahunBulan01Kol);
                staticFramework.saveNVC(Fields, "tahunBulan01Ht", other.tahunBulan01Ht);
                staticFramework.saveNVC(Fields, "tahunBulan01", other.tahunBulan01);
                staticFramework.saveNVC(Fields, "tahunBulan02Kol", other.tahunBulan02Kol);
                staticFramework.saveNVC(Fields, "tahunBulan02Ht", other.tahunBulan02Ht);
                staticFramework.saveNVC(Fields, "tahunBulan02", other.tahunBulan02);
                staticFramework.saveNVC(Fields, "tahunBulan03Kol", other.tahunBulan03Kol);
                staticFramework.saveNVC(Fields, "tahunBulan03Ht", other.tahunBulan03Ht);
                staticFramework.saveNVC(Fields, "tahunBulan03", other.tahunBulan03);
                staticFramework.saveNVC(Fields, "tahunBulan04Kol", other.tahunBulan04Kol);
                staticFramework.saveNVC(Fields, "tahunBulan04Ht", other.tahunBulan04Ht);
                staticFramework.saveNVC(Fields, "tahunBulan04", other.tahunBulan04);
                staticFramework.saveNVC(Fields, "tahunBulan05Kol", other.tahunBulan05Kol);
                staticFramework.saveNVC(Fields, "tahunBulan05Ht", other.tahunBulan05Ht);
                staticFramework.saveNVC(Fields, "tahunBulan05", other.tahunBulan05);
                staticFramework.saveNVC(Fields, "tahunBulan06Kol", other.tahunBulan06Kol);
                staticFramework.saveNVC(Fields, "tahunBulan06Ht", other.tahunBulan06Ht);
                staticFramework.saveNVC(Fields, "tahunBulan06", other.tahunBulan06);
                staticFramework.saveNVC(Fields, "tahunBulan07Kol", other.tahunBulan07Kol);
                staticFramework.saveNVC(Fields, "tahunBulan07Ht", other.tahunBulan07Ht);
                staticFramework.saveNVC(Fields, "tahunBulan07", other.tahunBulan07);
                staticFramework.saveNVC(Fields, "tahunBulan08Kol", other.tahunBulan08Kol);
                staticFramework.saveNVC(Fields, "tahunBulan08Ht", other.tahunBulan08Ht);
                staticFramework.saveNVC(Fields, "tahunBulan08", other.tahunBulan08);
                staticFramework.saveNVC(Fields, "tahunBulan09Kol", other.tahunBulan09Kol);
                staticFramework.saveNVC(Fields, "tahunBulan09Ht", other.tahunBulan09Ht);
                staticFramework.saveNVC(Fields, "tahunBulan09", other.tahunBulan09);
                staticFramework.saveNVC(Fields, "tahunBulan10Kol", other.tahunBulan10Kol);
                staticFramework.saveNVC(Fields, "tahunBulan10Ht", other.tahunBulan10Ht);
                staticFramework.saveNVC(Fields, "tahunBulan10", other.tahunBulan10);
                staticFramework.saveNVC(Fields, "tahunBulan11Kol", other.tahunBulan11Kol);
                staticFramework.saveNVC(Fields, "tahunBulan11Ht", other.tahunBulan11Ht);
                staticFramework.saveNVC(Fields, "tahunBulan11", other.tahunBulan11);
                staticFramework.saveNVC(Fields, "tahunBulan12Kol", other.tahunBulan12Kol);
                staticFramework.saveNVC(Fields, "tahunBulan12Ht", other.tahunBulan12Ht);
                staticFramework.saveNVC(Fields, "tahunBulan12", other.tahunBulan12);
                staticFramework.saveNVC(Fields, "tanggalMacet", other.tanggalMacet);
                staticFramework.saveNVC(Fields, "kodeSebabMacet", other.kodeSebabMacet);
                staticFramework.saveNVC(Fields, "sebabMacetDesc", other.sebabMacetDesc);
                staticFramework.saveNVC(Fields, "cabang", other.cabang);
                staticFramework.saveNVC(Fields, "cabangKet", other.cabangKet);
                staticFramework.saveNVC(Fields, "nilaiDalamMataUangAsal", other.nilaiDalamMataUangAsal);
                staticFramework.saveNVC(Fields, "tunggakan", other.tunggakan);
                staticFramework.saveNVC(Fields, "jumlahHariTunggakan", other.jumlahHariTunggakan);
                staticFramework.saveNVC(Fields, "tanggalUpdate", other.tanggalUpdate);

                staticFramework.save(Fields, Keys, "ideb_fasilitaslain", conn);

                if (other.agunan != null)
                    idebCollateralToDataTable(other.agunan, FileId, FileDetailId, id, conn);
                if (other.penjamin != null)
                    idebGuarantorToDataTable(other.penjamin, FileId, FileDetailId, id, conn);

            }
            #endregion

        }

        static private void idebCollateralToDataTable(List<Collateral> collaterals, string FileId, string FileDetailId, string FacilityId, DbConnection conn)
        {
            foreach (var collateral in collaterals)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", FacilityId);
                staticFramework.saveNVC(Keys, "CollateralId", id);

                staticFramework.saveNVC(Fields, "jenisAgunan", collateral.jenisAgunan);
                staticFramework.saveNVC(Fields, "jenisAgunanKet", collateral.jenisAgunanKet);
                staticFramework.saveNVC(Fields, "nilaiAgunanMenurutLJK", collateral.nilaiAgunanMenurutLJK);
                staticFramework.saveNVC(Fields, "tanggalUpdate", collateral.tanggalUpdate);
                staticFramework.saveNVC(Fields, "tanggalDibentuk", collateral.tanggalDibentuk);
                staticFramework.saveNVC(Fields, "nomorAgunan", collateral.nomorAgunan);
                staticFramework.saveNVC(Fields, "jenisPengikatan", collateral.jenisPengikatan);
                staticFramework.saveNVC(Fields, "jenisPengikatanKet", collateral.jenisPengikatanKet);
                staticFramework.saveNVC(Fields, "tanggalPengikatan", collateral.tanggalPengikatan);
                staticFramework.saveNVC(Fields, "namaPemilikAgunan", collateral.namaPemilikAgunan);
                staticFramework.saveNVC(Fields, "alamatAgunan", collateral.alamatAgunan);
                staticFramework.saveNVC(Fields, "kabKotaLokasiAgunan", collateral.kabKotaLokasiAgunan);
                staticFramework.saveNVC(Fields, "kabKotaLokasiAgunanKet", collateral.kabKotaLokasiAgunanKet);
                staticFramework.saveNVC(Fields, "tanggalPenilaianPenilaiIndependen", collateral.tanggalPenilaianPenilaiIndependen);
                staticFramework.saveNVC(Fields, "buktiKepemilikan", collateral.buktiKepemilikan);
                staticFramework.saveNVC(Fields, "nilaiAgunanNjop", collateral.nilaiAgunanNjop);
                staticFramework.saveNVC(Fields, "nilaiAgunanIndep", collateral.nilaiAgunanIndep);
                staticFramework.saveNVC(Fields, "namaPenilaiIndep", collateral.namaPenilaiIndep);
                staticFramework.saveNVC(Fields, "asuransi", collateral.asuransi);
                staticFramework.saveNVC(Fields, "tglPenilaianPelapor", collateral.tglPenilaianPelapor);
                staticFramework.saveNVC(Fields, "prosentaseParipasu", collateral.prosentaseParipasu);
                staticFramework.saveNVC(Fields, "peringkatAgunan", collateral.peringkatAgunan);
                staticFramework.saveNVC(Fields, "kodeLembagaPemeringkat", collateral.kodeLembagaPemeringkat);
                staticFramework.saveNVC(Fields, "lembagaPemeringkat", collateral.lembagaPemeringkat);
                staticFramework.saveNVC(Fields, "keterangan", collateral.keterangan);

                staticFramework.save(Fields, Keys, "ideb_agunan", conn);
            }
        }

        static private void idebGuarantorToDataTable(List<Guarantor> guarantors, string FileId, string FileDetailId, string FacilityId, DbConnection conn)
        {
            foreach (var guarantor in guarantors)
            {
                string id = Guid.NewGuid().ToString();
                NameValueCollection Keys = new NameValueCollection();
                NameValueCollection Fields = new NameValueCollection();
                staticFramework.saveNVC(Keys, "FileId", FileId);
                staticFramework.saveNVC(Keys, "FileDetailId", FileDetailId);
                staticFramework.saveNVC(Keys, "FacilityId", FacilityId);
                staticFramework.saveNVC(Keys, "GuarantorId", id);

                staticFramework.saveNVC(Fields, "namaPenjamin", guarantor.namaPenjamin);
                staticFramework.saveNVC(Fields, "nomorIdentitas", guarantor.nomorIdentitas);
                staticFramework.saveNVC(Fields, "tanggalUpdate", guarantor.tanggalUpdate);
                staticFramework.saveNVC(Fields, "tanggalBuat", guarantor.tanggalBuat);
                staticFramework.saveNVC(Fields, "kodeJenisPenjamin", guarantor.kodeJenisPenjamin);
                staticFramework.saveNVC(Fields, "keteranganJenisPenjamin", guarantor.keteranganJenisPenjamin);
                staticFramework.saveNVC(Fields, "alamatPenjamin", guarantor.alamatPenjamin);
                staticFramework.saveNVC(Fields, "keterangan", guarantor.keterangan);

                staticFramework.save(Fields, Keys, "ideb_penjamin", conn);
            }
        }

        public static Dictionary<string, TValue> ToDictionary<TValue>(object obj)
        {
            var serializerSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CustomContractResolver(false)
            };
            var settings = new JsonSerializerSettings { Error = (se, ev) => { ev.ErrorContext.Handled = true; } };
            var json = JsonConvert.SerializeObject(obj, serializerSettings);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, TValue>>(json, settings);
            return dictionary;
        }

        static private Ideb fixdatetime(Ideb ideb)
        {
            if (ideb.individual != null)
            {
                foreach (var debitur in ideb.individual.dataPokokDebitur)
                {
                    int len = debitur.tanggalUpdate.Length;
                    if (len == 8) debitur.tanggalUpdate += "000000";
                }
            }
            if (ideb.perusahaan != null)
            {
                foreach (var debitur in ideb.perusahaan.dataPokokDebitur)
                {
                    int len = debitur.tanggalUpdate.Length;
                    if (len == 8) debitur.tanggalUpdate += "000000";
                }
            }

            return ideb;
        }
    }
}
