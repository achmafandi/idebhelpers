﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace IdebHelper
{
    class IdebHTML
    {
        public static string GetHTMLString(Header header, Individual individual, Corporate corporate, FacilitySummary facilitySummary, Facility facility)
        {
            return GetHTMLString(header, individual, corporate, facilitySummary, facility, Path.Combine(Directory.GetCurrentDirectory(), "img"));
        }

        public static string GetHTMLString(Header header, Individual individual, Corporate corporate, FacilitySummary facilitySummary, Facility facility, string imgPath)
        {
            return GetHTMLString(header, individual, corporate, facilitySummary, facility, Path.Combine(Directory.GetCurrentDirectory(), "img"), false);
        }
        public static string GetHTMLString(Header header, Individual individual, Corporate corporate, FacilitySummary facilitySummary, Facility facility, string imgPath, bool addFooter)
        {
            CultureInfo culture = new CultureInfo("id-ID");
            string latestDataYearMonth = null;
            string latestDataYearMonthHeader = null;
            string reportNumber = null;
            if (individual != null)
            {
                latestDataYearMonth = DateTime.ParseExact(individual.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                latestDataYearMonthHeader = dateToLongDesc(individual.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = individual.nomorLaporan;
            }
            else
            {
                latestDataYearMonth = DateTime.ParseExact(corporate.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                latestDataYearMonthHeader = dateToLongDesc(corporate.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = corporate.nomorLaporan;
            }
            var sb = new StringBuilder();
            sb.Append(
@"
<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>

<head>
    <title></title>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
</head>

<body>
    <br />
    <table width='100%'>
        <tr>
            <td style='font-size: 27px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px;'><img src='logo-ideb.png' width='50px' /> Informasi Debitur</td>
            <td rowspan='2' align='right'><img src='logo-ojk.png' width='130px' /> </td>
        </tr>
        <tr><td style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px; color:#000;line-height:45px'>Sistem Layanan Informasi Keuangan</td style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px;'></tr>
    </table>
    <table width='100%'>
        <tr>
            <td colspan='2'>
                <table width='100%' style='background: #f2f2f2; border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px;' cellspacing='0' cellpadding='0'>
                    <tr>
                        <td rowspan='2' align='center' width='4%'><img src='info.png' width='20px' /></td>
                        <td style='font-size: 12px; font-family: CenturyGothic, AppleGothic, sans-serif; font-style: italic;' rowspan='2'>Informasi ini bersifat RAHASIA dan hanya digunakan untuk kepentingan pemohon informasi. <br />Akibat yang timbul dari penggunaan informasi ini bukan merupakan tanggung jawab Otoritas Jasa Keuangan.</td>
                        <td style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 25px; text-align:center' width='12%'>RAHASIA</td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>

        <tr>
            <td width='78%'>
                
                <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;'>
                    <tr style='font-size: 12px; font-family: CenturyGothic, AppleGothic, sans-serif; font-style: italic;'>
                        <td colspan='4'>Informasi diberikan berdasarkan laporan yang dikirimkan oleh pelapor ke dalam Sistem Layanan Informasi</td>
                    </tr>
                    <tr style='font-size: 12px; font-family: CenturyGothic, AppleGothic, sans-serif; font-style: italic;'>
                        <td colspan='4'>Keuangan dengan <b style='color: #f24f4f;'><u>kata kunci pencarian</u></b> sebagai berikut:</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
");
            if (individual != null)
            {
                sb.AppendFormat(
                @"

                    <tr style='color: #f24f4f;'>
                        <td colspan='3'>Nama</td>
                        <td>Jenis Kelamin</td>
                    </tr>
                    <tr>
                        <td colspan='3'>{0}</td>
                        <td>{1}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style='color: #f24f4f;'>
                        <td width='180px'>No. Identitas</td>
                        <td width='180px'>NPWP</td>
                        <td width='180px'>Tempat Lahir</td>
                        <td width='180px'>Tanggal Lahir</td>
                    </tr>
                    <tr>
                        <td>{2}</td>
                        <td>{3}</td>
                        <td>{4}</td>
                        <td>{5}</td>
                    </tr>
",
                individual.parameterPencarian.namaDebitur,
                individual.parameterPencarian.jenisKelaminKet,
                individual.parameterPencarian.noIdentitas,
                individual.parameterPencarian.npwp,
                individual.parameterPencarian.tempatLahir,
                individual.parameterPencarian.tanggalLahir.HasValue ? dateToLongDesc(individual.parameterPencarian.tanggalLahir.Value) : null
    );
            }
            else if (corporate != null)
            {
                sb.AppendFormat(
                @"

                    <tr style='color: #f24f4f;'>
                        <td colspan='4'>Nama</td>
                    </tr>
                    <tr>
                        <td colspan='4'>{0}</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style='color: #f24f4f;'>
                        <td width='180px'>NPWP</td>
                        <td width='180px'>Tempat Pendirian</td>
                        <td width='180px'>Tanggal Akte Pendirian</td>
                        <td width='180px'></td>
                    </tr>
                    <tr>
                        <td>{1}</td>
                        <td>{2}</td>
                        <td>{3}</td>
                        <td></td>
                    </tr>
",
                corporate.parameterPencarian.namaBadanUsaha,
                corporate.parameterPencarian.npwp,
                corporate.parameterPencarian.tempatPendirian,
                corporate.parameterPencarian.tanggalAktaPendirian.HasValue ? dateToLongDesc(corporate.parameterPencarian.tanggalAktaPendirian.Value) : null
    );
            }

            sb.AppendFormat(
            @"

                </table>
                
            </td>
            <td width='22%'>
                
                <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; background: #f2f2f2; padding:5px 10px;' cellspacing='0'>
");
            sb.AppendFormat(
            @"
                    <tr style='color: #f24f4f;'>
                        <td>Kode Ref. Pengguna</td>
                    </tr>
                    <tr>
                        <td>{0}</td>
                    </tr>
                    <tr style='color: #f24f4f;'>
                        <td>Nomor Laporan</td>
                    </tr>
                    <tr>
                        <td>{1}</td>
                    </tr>
                    <tr style='color: #f24f4f;'>
                        <td>Posisi Data Terakhir</td>
                    </tr>
                    <tr>
                        <td>{2}</td>
                    </tr>
                    <tr style='color: #f24f4f;'>
                        <td>Tanggal Permintaan</td>
                    </tr>
                    <tr>
                        <td>{3} {4}</td>
                    </tr>
",
            header.kodeReferensiPengguna,
            reportNumber,
            latestDataYearMonthHeader,
            header.tanggalPermintaan.HasValue ? dateToLongDesc(header.tanggalPermintaan.Value) : null,
            header.tanggalPermintaan.HasValue ? header.tanggalPermintaan.Value.ToString("HH:mm:ss") : null
            );

            sb.AppendFormat(
            @"
                </table>
                
            </td>
        </tr>
    </table>
    <br />
");

            #region Data Pokok Debitur
            sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px; margin-bottom:15px'>Data Pokok Debitur<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
    <table width='100%' cellspacing='0' cellpadding='0'>
        <tr>
            <td style='font-size: 12px; font-family: CenturyGothic, AppleGothic, sans-serif; font-style: italic;'>Penyajian informasi debitur pada Sistem Layanan Informasi Keuangan dikelompokkan berdasarkan nomor identitas debitur. Pengguna informasi diharapkan dapat meneliti kembali kemungkinan adanya debitur berbeda yang dilaporkan menggunakan nomor identitas yang sama</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    
");
            if (individual != null)
            {
                foreach (var debitur in individual.dataPokokDebitur)
                {
                    sb.AppendFormat(
                    @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2;' cellspacing='0' cellpadding='2px'>
        <tr style='color: #f24f4f;'>
            <td>Nama Sesuai Identitas</td>
            <td>Identitas</td>
            <td>Jenis Kelamin / NPWP</td>
            <td>Tempat / Tgl Lahir</td>
            <td>Pelapor / Tanggal Update</td>
        </tr>
        <tr style='background: #f2f2f2;'>
            <td>{0}<br />&nbsp;</td>
            <td>{1} /<br /> {2}&nbsp;</td>
            <td>{3} /<br /> {4}&nbsp;</td>
            <td>{5} /<br /> {6}&nbsp;</td>
            <td>{7} /<br /> {8}&nbsp;</td>
        </tr>
        <tr>
            <td colspan='5'>
                <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;' cellspacing='0' cellpadding='3px'>
                    <tr style='background: #d9d9d9;'>
                        <td width='30%'>Alamat</td>
                        <td width='15%'>Kelurahan</td>
                        <td width='15%'>Kecamatan</td>
                        <td width='15%'>Kabupaten / Kota</td>
                        <td width='10%'>Kode Pos</td>
                        <td width='15%'>Negara</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td>{9}</td>
                        <td>{10}</td>
                        <td>{11}</td>
                        <td>{12}</td>
                        <td>{13}</td>
                        <td>{14}</td>
                    </tr>
                    <tr style='background: #d9d9d9;'>
                        <td>Pekerjaan</td>
                        <td colspan='2'>Tempat Bekerja</td>
                        <td colspan='2'>Bidang Usaha</td>
                        <td>Status Gelar Debitur</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td>{15}</td>
                        <td colspan='2'>{16}</td>
                        <td colspan='2'>{17}</td>
                        <td>{18}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
",
                    debitur.namaDebitur,
                    debitur.identitasKet.Contains("KTP") ? "NIK" : debitur.identitasKet.Contains("Paspor") ? "Passport" : debitur.identitasKet,
                    debitur.noIdentitas,
                    debitur.jenisKelaminKet,
                    debitur.npwp,
                    debitur.tempatLahir,
                    debitur.tanggalLahir.HasValue ? dateToLongDesc(debitur.tanggalLahir.Value) : null,
                    debitur.pelaporKet,
                    (debitur.tanggalUpdate.Length>=8) ? dateToLongDesc(debitur.tanggalUpdate) : debitur.tanggalDibentuk.HasValue ? dateToLongDesc(debitur.tanggalDibentuk.Value) : null,
                    //debitur.tanggalUpdate,

                    debitur.alamat,
                    debitur.kelurahan,
                    debitur.kecamatan,
                    debitur.kabKotaKet,
                    debitur.kodePos,
                    debitur.negaraKet,

                    debitur.pekerjaanKet,
                    debitur.tempatBekerja,
                    debitur.bidangUsahaKet,
                    debitur.statusGelarDebitur
                    );
                }
            }
            else if (corporate != null)
            {
                foreach (var debitur in corporate.dataPokokDebitur)
                {
                    sb.AppendFormat(
                    @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2;' cellspacing='0' cellpadding='2px'>
        <tr style='color: #f24f4f;'>
            <td>Nama Debitur</td>
            <td>NPWP</td>
            <td>Bentuk BU / Go Public</td>
            <td>Tempat Pendirian</td>
            <td>No / Tgl Akta Pendirian</td>
            <td>Pelapor / Tanggal Update</td>
        </tr>
        <tr style='background: #f2f2f2;'>
            <td>{0}<br />&nbsp;</td>
            <td>{1}<br />&nbsp;</td>
            <td>{2} /<br /> {3}&nbsp;</td>
            <td>{4}<br />&nbsp;</td>
            <td>{5} /<br /> {6}&nbsp;</td>
            <td>{7} /<br /> {8}&nbsp;</td>
        </tr>
        <tr>
            <td colspan='6'>
                <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;' cellspacing='0' cellpadding='3px'>
                    <tr style='background: #d9d9d9;'>
                        <td width='30%'>Alamat</td>
                        <td width='15%'>Kelurahan</td>
                        <td width='15%'>Kecamatan</td>
                        <td width='15%'>Kabupaten / Kota</td>
                        <td width='10%'>Kode Pos</td>
                        <td width='15%'>Negara</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td>{9}</td>
                        <td>{10}</td>
                        <td>{11}</td>
                        <td>{12}</td>
                        <td>{13}</td>
                        <td>{14}</td>
                    </tr>
                    <tr style='background: #d9d9d9;'>
                        <td>No / Tanggal Akta Terakhir</td>
                        <td colspan='2'>Bidang Usaha</td>
                        <td>Pemeringkat</td>
                        <td>Peringkat</td>
                        <td>Tgl Pemeringkatan</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td>{15} /<br /> {16}</td>
                        <td colspan='2'>{17}</td>
                        <td>{18}</td>
                        <td>{19}</td>
                        <td>{20}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
",
                    debitur.namaDebitur,
                    debitur.npwp,
                    debitur.bentukBuKet,
                    debitur.goPublic == "Y" ? "Ya" : "Tidak",
                    debitur.tempatPendirian,
                    debitur.noAktaPendirian,
                    debitur.tglAktaPendirian.HasValue ? dateToLongDesc(debitur.tglAktaPendirian.Value) : null,
                    debitur.pelaporKet,
                    (debitur.tanggalUpdate.Length >= 8) ? dateToLongDesc(debitur.tanggalUpdate) : debitur.tanggalDibentuk.HasValue ? dateToLongDesc(debitur.tanggalDibentuk.Value) : null,
                    //debitur.tanggalUpdate,

                    debitur.alamat,
                    debitur.kelurahan,
                    debitur.kecamatan,
                    debitur.kabKotaKet,
                    debitur.kodePos,
                    debitur.negaraKet,

                    debitur.noAktaTerakhir,
                    debitur.tglAktaTerakhir.HasValue ? dateToLongDesc(debitur.tglAktaTerakhir.Value) : null,
                    debitur.sektorEkonomiKet,
                    debitur.pemeringkatKet,
                    debitur.peringkat,
                    debitur.tanggalPemeringkatan.HasValue ? dateToLongDesc(debitur.tanggalPemeringkatan.Value) : null
                    );
                }

                sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px; margin-bottom:15px;'>Pemilik / Pengurus<hr noshade></div>
    <br />
");

                foreach (var pengurusGroup in corporate.kelompokPengurusPemilik)
                {
                    sb.AppendFormat(
                    @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2;' cellspacing='0' cellpadding='2px'>
        <tr style='color: #f24f4f;'>
            <td>Pelapor / Tanggal Update</td>
        </tr>
        <tr style='background: #f2f2f2;'>
            <td>{0} / {1}&nbsp;</td>
        </tr>
    </table>
    <br />
",
                    pengurusGroup.kodeLJK + " - " + pengurusGroup.namaLJK,
                    pengurusGroup.tanggalUpdate.HasValue ? dateToLongDesc(pengurusGroup.tanggalUpdate.Value) : null
                    );

                    foreach (var pengurus in pengurusGroup.pengurusPemilik)
                    {
                        sb.AppendFormat(
                        @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2;' cellspacing='0' cellpadding='2px'>
        <tr>
            <td colspan='8'>
                <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;' cellspacing='0' cellpadding='3px'>
                    <tr style='background: #d9d9d9;'>
                        <td width='30%'>Nama</td>
                        <td width='15%'>No. Identitas</td>
                        <td width='15%'>Jenis Kelamin</td>
                        <td width='10%'>Jabatan</td>
                        <td width='10%'></td>
                        <td width='5%'>Status Pengurus / Pemilik</td>
                        <td width='5%'></td>
                        <td width='10%'>Pangsa Kepemilikan</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td>{0}</td>
                        <td>{1}</td>
                        <td>{2}</td>
                        <td colspan='2'>{3}</td>
                        <td colspan='2'>{4}</td>
                        <td>{5}</td>
                    </tr>
                    <tr style='background: #d9d9d9;'>
                        <td colspan='2'>Alamat</td>
                        <td colspan='2'>Kelurahan</td>
                        <td colspan='2'>Kecamatan</td>
                        <td colspan='2'>Kabupaten / Kota</td>
                    </tr>
                    <tr style='background: #f2f2f2;'>
                        <td colspan='2'>{6}</td>
                        <td colspan='2'>{7}</td>
                        <td colspan='2'>{8}</td>
                        <td colspan='2'>{9}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
",
                        pengurus.namaSesuaiIdentitas,
                        pengurus.nomorIdentitas,
                        pengurus.jenisKelamin,
                        pengurus.posisiPekerjaan,
                        pengurus.statusPengurusPemilik,
                        pengurus.prosentaseKepemilikan.HasValue ? pengurus.prosentaseKepemilikan.Value.ToString("###,##0.##########") + " %" : null,
                        pengurus.alamat,
                        pengurus.kelurahan,
                        pengurus.kecamatan,
                        pengurus.kota
                        );
                    }
                }
            }
            #endregion

            #region summary
            sb.AppendFormat(
            @"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; color: #f24f4f; line-height: 3px; margin-bottom:15px;'>Ringkasan Fasilitas<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
            if (individual != null)
            {
                sb.AppendFormat(
                @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;' cellspacing='0' cellpadding='3px border-collapse:collapse;'>
        <tr style='background: #d9d9d9; text-align:center'>
            <td>Fasilitas</td>
            <td>Kredit/Pembiayaan<br />(dalam IDR)</td>
            <td>Irrevocable L/C<br />(dalam IDR)</td>
            <td>Garansi Yang Diberikan<br />(dalam IDR)</td>
            <td>Fasilitas Lain<br />(dalam IDR)</td>
            <td>Total<br />(dalam IDR)</td>
            <td>Kualitas Terburuk<br />/ Bulan Data</td>
        </tr>
        <tr style='text-align:center; border-bottom: 1px solid #d9d9d9;'>
            <td style='text-align:left; border-bottom: 1px solid #d9d9d9;'>Plafon Efektif</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{0}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{1}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{2}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{3}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{4}</td>
            <td rowspan='3' valign='top' style='border-bottom: 1px solid #d9d9d9;'>{5} / {6}</td>
        </tr>
        <tr style='text-align:center;border-bottom: 1px solid #d9d9d9;'>
            <td style='text-align:left; border-bottom: 1px solid #d9d9d9;'>Baki Debet</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{7}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{8}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{9}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{10}</td>
            <td style='border-bottom: 1px solid #d9d9d9;'>{11}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; margin-top:10px;'>
        <tr>
            <td width='20%'>Jumlah Kreditur</td>
            <td width='8%'>Bank Umum</td>
            <td width='5%' style='border:1px solid #d9d9d9;text-align:center'>{12}</td>
            <td width='8%'>&nbsp;</td>
            <td width='8%'>BPR / BPRS</td>
            <td width='5%' style='border:1px solid #d9d9d9;text-align:center'>{13}</td>
            <td width='8%'>&nbsp;</td>
            <td width='15%'>Lembaga Pembiayaan</td>
            <td width='5%' style='border:1px solid #d9d9d9;text-align:center'>{14}</td>
            <td width='8%'>&nbsp;</td>
            <td width='5%'>Lainnya</td>
            <td width='5%' style='border:1px solid #d9d9d9;text-align:center'>{15}</td>
        </tr>
    </table>
    <br />
",
                facilitySummary.plafonEfektifKreditPembiayaan.HasValue ? facilitySummary.plafonEfektifKreditPembiayaan.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifLc.HasValue ? facilitySummary.plafonEfektifLc.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifGyd.HasValue ? facilitySummary.plafonEfektifGyd.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifLain.HasValue ? facilitySummary.plafonEfektifLain.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifTotal.HasValue ? facilitySummary.plafonEfektifTotal.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.kualitasTerburuk,
                !string.IsNullOrEmpty(facilitySummary.kualitasBulanDataTerburuk) ? DateTime.ParseExact(facilitySummary.kualitasBulanDataTerburuk + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) : null,

                facilitySummary.bakiDebetKreditPembiayaan.HasValue ? facilitySummary.bakiDebetKreditPembiayaan.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetLc.HasValue ? facilitySummary.bakiDebetLc.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetGyd.HasValue ? facilitySummary.bakiDebetGyd.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetLain.HasValue ? facilitySummary.bakiDebetLain.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetTotal.HasValue ? facilitySummary.bakiDebetTotal.Value.ToString("###,##0.00", culture) : null,

                facilitySummary.krediturBankUmum,
                facilitySummary.krediturBPRS,
                facilitySummary.krediturLp,
                facilitySummary.krediturLainnya
                );
            }
            else if (corporate != null)
            {
                sb.AppendFormat(
                @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='background: #d9d9d9; text-align:center'>
            <td>Fasilitas</td>
            <td>Kredit/Pembiayaan<br />(dalam IDR)</td>
            <td>Surat Berharga<br />(dalam IDR)</td>
            <td>Irrevocable L/C<br />(dalam IDR)</td>
            <td>Garansi Yang Diberikan<br />(dalam IDR)</td>
            <td>Fasilitas Lain<br />(dalam IDR)</td>
            <td>Total<br />(dalam IDR)</td>
        </tr>
        <tr style='text-align:center;border-bottom: 1px solid #d9d9d9;'>
            <td style='text-align:left'>Plafon Efektif</td>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
            <td>{5}</td>
        </tr>
        <tr style='text-align:center;border-bottom: 1px solid #d9d9d9;'>
            <td style='text-align:left'>Baki Debet</td>
            <td>{6}</td>
            <td>{7}</td>
            <td>{8}</td>
            <td>{9}</td>
            <td>{10}</td>
            <td>{11}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; margin-top:10px'>
        <tr>
            <td width='15%'>Jumlah Kreditur</td>
            <td width='8%'>Bank Umum</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{12}</td>
            <td width='3%'>&nbsp;</td>
            <td width='8%'>BPR / BPRS</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{13}</td>
            <td width='3%'>&nbsp;</td>
            <td width='8%'>Lembaga Pembiayaan</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{14}</td>
            <td width='3%'>&nbsp;</td>
            <td width='8%'>Lainnya</td>
            <td width='5%' style='border:1px solid #d9d9d9;text-align:center'>{15}</td>
            <td width='3%'>&nbsp;</td>
            <td width='15%'>Kualitas Terburuk<br />/ Bulan Data</td>
            <td width='15%'>{16}<br />/ {17}</td>
        </tr>
    </table>
    <br />
",

                facilitySummary.plafonEfektifKreditPembiayaan.HasValue ? facilitySummary.plafonEfektifKreditPembiayaan.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifSec.HasValue ? facilitySummary.plafonEfektifSec.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifLc.HasValue ? facilitySummary.plafonEfektifLc.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifGyd.HasValue ? facilitySummary.plafonEfektifGyd.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifLain.HasValue ? facilitySummary.plafonEfektifLain.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.plafonEfektifTotal.HasValue ? facilitySummary.plafonEfektifTotal.Value.ToString("###,##0.00", culture) : null,

                facilitySummary.bakiDebetKreditPembiayaan.HasValue ? facilitySummary.bakiDebetKreditPembiayaan.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetSec.HasValue ? facilitySummary.bakiDebetSec.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetLc.HasValue ? facilitySummary.bakiDebetLc.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetGyd.HasValue ? facilitySummary.bakiDebetGyd.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetLain.HasValue ? facilitySummary.bakiDebetLain.Value.ToString("###,##0.00", culture) : null,
                facilitySummary.bakiDebetTotal.HasValue ? facilitySummary.bakiDebetTotal.Value.ToString("###,##0.00", culture) : null,

                facilitySummary.krediturBankUmum,
                facilitySummary.krediturBPRS,
                facilitySummary.krediturLp,
                facilitySummary.krediturLainnya,

                facilitySummary.kualitasTerburuk,
                //DateTime.ParseExact(facilitySummary.kualitasBulanDataTerburuk + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"))
                !string.IsNullOrEmpty(facilitySummary.kualitasTerburuk) ? DateTime.ParseExact(facilitySummary.kualitasBulanDataTerburuk + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) : ""
                );
            }

            #endregion

            #region credit
            if (facility.kreditPembiayan.Count == 0)
            {
                sb.AppendFormat(
    @"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Kredit/Pembiayaan</div>
");
            }
            else
            {
                foreach (var credit in facility.kreditPembiayan)
                {
                    sb.AppendFormat(
        @"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Kredit/Pembiayaan</div>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding:5px 10px 5px 10px;' cellspacing='0'>
        <tr style='color: #f24f4f;'>
            <td width='23%'>Pelapor</td>
            <td width='31%'>Cabang</td>
            <td width='23%'>Baki Debet</td>
            <td width='23%'>Tanggal Update</td>
        </tr>
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{4}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{5}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{6}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{7}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{8}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{9}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{10}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{11}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{12}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{13}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{14}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{15}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>Kualitas / Jumlah<br />Hari Tunggakan</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{16}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{17}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{18}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{19}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{20}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{21}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{22}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{23}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{24}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{25}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{26}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{27}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{28}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{29}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{30}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{31}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{32}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{33}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{34}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{35}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{36}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{37}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{38}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{39}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{40}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{41}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{42}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{43}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{44}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{45}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{46}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{47}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{48}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{49}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{50}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{51}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>&nbsp<br />&nbsp</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{52}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{53}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{54}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{55}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{56}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{57}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{58}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{59}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{60}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{61}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{62}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{63}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{64}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{65}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{66}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{67}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{68}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{69}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{70}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{71}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{72}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{73}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{74}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{75}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>No Rekening</td>
            <td width='25%'>{76}</td>
            <td width='25%' style='background: #f2f2f2;'>Kualitas</td>
            <td width='25%'>{77}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Sifat Kredit/Pembiayaan</td>
            <td>{78}</td>
            <td style='background: #f2f2f2;'>Jumlah Hari Tunggakan</td>
            <td>{79}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Kredit/Pembiayaan</td>
            <td>{80}</td>
            <td style='background: #f2f2f2;'>Nilai Proyek </td>
            <td>{81}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Akad Kredit/Pembiayaan</td>
            <td>{82}</td>
            <td style='background: #f2f2f2;'>Plafon Awal </td>
            <td>{83}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Frekuensi Perpanjangan Kredit/Pembiayaan</td>
            <td>{84}</td>
            <td style='background: #f2f2f2;'>Plafon </td>
            <td>{85}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Awal</td>
            <td>{86}</td>
            <td style='background: #f2f2f2;'>Realisasi/Pencairan Bulan Berjalan</td>
            <td>{87}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Awal</td>
            <td>{88}</td>
            <td style='background: #f2f2f2;'>Nilai dalam Mata Uang Asal</td>
            <td>{89}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Akhir</td>
            <td>{90}</td>
            <td style='background: #f2f2f2;'>Sebab Macet</td>
            <td>{91}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Akhir</td>
            <td>{92}</td>
            <td style='background: #f2f2f2;'>Tanggal Macet</td>
            <td>{93}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Awal Kredit</td>
            <td>{94}</td>
            <td style='background: #f2f2f2;'>Tunggakan Pokok</td>
            <td>{95}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Mulai </td>
            <td>{96}</td>
            <td style='background: #f2f2f2;'>Tunggakan Bunga</td>
            <td>{97}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Jatuh Tempo</td>
            <td>{98}</td>
            <td style='background: #f2f2f2;'>Frekuensi Tunggakan</td>
            <td>{99}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Kategori Debitur </td>
            <td>{100}</td>
            <td style='background: #f2f2f2;'>Denda</td>
            <td>{101}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Penggunaan</td>
            <td>{102}</td>
            <td style='background: #f2f2f2;'>Frekuensi Restrukturisasi</td>
            <td>{103}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Sektor Ekonomi</td>
            <td>{104}</td>
            <td style='background: #f2f2f2;'>Tanggal Restrukturisasi Akhir</td>
            <td>{105}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Kredit Program Pemerintah</td>
            <td>{106}</td>
            <td style='background: #f2f2f2;'>Cara Restrukturisasi</td>
            <td>{107}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Kab/Kota Lokasi Proyek</td>
            <td>{108}</td>
            <td style='background: #f2f2f2;'>Kondisi</td>
            <td>{109}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Valuta</td>
            <td>{110}</td>
            <td style='background: #f2f2f2;'>Tanggal Kondisi</td>
            <td>{111}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Suku Bunga/Imbalan</td>
            <td>{112}</td>
            <td style='background: #f2f2f2;'>Jenis Suku Bunga/Imbalan</td>
            <td>{113}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{114}</td>
        </tr>
    </table>
    <br />
",
        credit.ljk + " - " + credit.ljkKet,
        credit.cabangKet,
        credit.bakiDebet.HasValue ? credit.bakiDebet.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.tanggalUpdate.HasValue ? dateToLongDesc(credit.tanggalUpdate.Value) : credit.tanggalDibentuk.HasValue ? dateToLongDesc(credit.tanggalDibentuk.Value) : null,

        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan01 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan02 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan03 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan04 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan05 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan06 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan07 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan08 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan09 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan10 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan11 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan12 + "01", "yyyyMMdd", null)),

        credit.tahunBulan01Kol,
        credit.tahunBulan01Ht.HasValue ? credit.tahunBulan01Ht.Value.ToString() : null,
        credit.tahunBulan02Kol,
        credit.tahunBulan02Ht.HasValue ? credit.tahunBulan02Ht.Value.ToString() : null,
        credit.tahunBulan03Kol,
        credit.tahunBulan03Ht.HasValue ? credit.tahunBulan03Ht.Value.ToString() : null,
        credit.tahunBulan04Kol,
        credit.tahunBulan04Ht.HasValue ? credit.tahunBulan04Ht.Value.ToString() : null,
        credit.tahunBulan05Kol,
        credit.tahunBulan05Ht.HasValue ? credit.tahunBulan05Ht.Value.ToString() : null,
        credit.tahunBulan06Kol,
        credit.tahunBulan06Ht.HasValue ? credit.tahunBulan06Ht.Value.ToString() : null,
        credit.tahunBulan07Kol,
        credit.tahunBulan07Ht.HasValue ? credit.tahunBulan07Ht.Value.ToString() : null,
        credit.tahunBulan08Kol,
        credit.tahunBulan08Ht.HasValue ? credit.tahunBulan08Ht.Value.ToString() : null,
        credit.tahunBulan09Kol,
        credit.tahunBulan09Ht.HasValue ? credit.tahunBulan09Ht.Value.ToString() : null,
        credit.tahunBulan10Kol,
        credit.tahunBulan10Ht.HasValue ? credit.tahunBulan10Ht.Value.ToString() : null,
        credit.tahunBulan11Kol,
        credit.tahunBulan11Ht.HasValue ? credit.tahunBulan11Ht.Value.ToString() : null,
        credit.tahunBulan12Kol,
        credit.tahunBulan12Ht.HasValue ? credit.tahunBulan12Ht.Value.ToString() : null,

        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan13 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan14 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan15 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan16 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan17 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan18 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan19 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan20 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan21 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan22 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan23 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(credit.tahunBulan24 + "01", "yyyyMMdd", null)),


        credit.tahunBulan13Kol,
        credit.tahunBulan13Ht.HasValue ? credit.tahunBulan13Ht.Value.ToString() : null,
        credit.tahunBulan14Kol,
        credit.tahunBulan14Ht.HasValue ? credit.tahunBulan14Ht.Value.ToString() : null,
        credit.tahunBulan15Kol,
        credit.tahunBulan15Ht.HasValue ? credit.tahunBulan15Ht.Value.ToString() : null,
        credit.tahunBulan16Kol,
        credit.tahunBulan16Ht.HasValue ? credit.tahunBulan16Ht.Value.ToString() : null,
        credit.tahunBulan17Kol,
        credit.tahunBulan17Ht.HasValue ? credit.tahunBulan17Ht.Value.ToString() : null,
        credit.tahunBulan18Kol,
        credit.tahunBulan18Ht.HasValue ? credit.tahunBulan18Ht.Value.ToString() : null,
        credit.tahunBulan19Kol,
        credit.tahunBulan19Ht.HasValue ? credit.tahunBulan19Ht.Value.ToString() : null,
        credit.tahunBulan20Kol,
        credit.tahunBulan20Ht.HasValue ? credit.tahunBulan20Ht.Value.ToString() : null,
        credit.tahunBulan21Kol,
        credit.tahunBulan21Ht.HasValue ? credit.tahunBulan21Ht.Value.ToString() : null,
        credit.tahunBulan22Kol,
        credit.tahunBulan22Ht.HasValue ? credit.tahunBulan22Ht.Value.ToString() : null,
        credit.tahunBulan23Kol,
        credit.tahunBulan23Ht.HasValue ? credit.tahunBulan23Ht.Value.ToString() : null,
        credit.tahunBulan24Kol,
        credit.tahunBulan24Ht.HasValue ? credit.tahunBulan24Ht.Value.ToString() : null,

        credit.noRekening,
        credit.kualitas + " - " + credit.kualitasKet,
        credit.sifatKreditPembiayaanKet,
        credit.jumlahHariTunggakan,

        credit.jenisKreditPembiayaanKet,
        credit.nilaiProyek.HasValue ? credit.nilaiProyek.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.akadKreditPembiayaanKet,
        credit.plafonAwal.HasValue ? credit.plafonAwal.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.frekPerpjganKreditPembiayaan, //
        credit.plafon.HasValue ? credit.plafon.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.noAkadAwal,
        credit.realisasiBulanBerjalan.HasValue ? credit.realisasiBulanBerjalan.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.tanggalAkadAwal.HasValue ? dateToLongDesc(credit.tanggalAkadAwal.Value) : null,
        credit.nilaiDalamMataUangAsal.HasValue ? credit.nilaiDalamMataUangAsal.Value.ToString("###,##0.00", culture) : null,
        credit.noAkadAkhir,
        credit.sebabMacetKet,
        credit.tanggalAkadAkhir.HasValue ? dateToLongDesc(credit.tanggalAkadAkhir.Value) : null,
        credit.tanggalMacet.HasValue ? dateToLongDesc(credit.tanggalMacet.Value) : null,
        credit.tanggalAwalKredit.HasValue ? dateToLongDesc(credit.tanggalAwalKredit.Value) : null,
        credit.tunggakanPokok.HasValue ? credit.tunggakanPokok.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.tanggalMulai.HasValue ? dateToLongDesc(credit.tanggalMulai.Value) : null,
        credit.tunggakanBunga.HasValue ? credit.tunggakanBunga.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.tanggalJatuhTempo.HasValue ? dateToLongDesc(credit.tanggalJatuhTempo.Value) : null,
        credit.frekuensiTunggakan,
        credit.kategoriDebiturKet,
        credit.denda.HasValue ? credit.denda.Value.ToString("Rp ###,##0.00", culture) : null,
        credit.jenisPenggunaanKet,
        credit.frekuensiRestrukturisasi,
        credit.sektorEkonomiKet,
        credit.tanggalRestrukturisasiAkhir.HasValue ? dateToLongDesc(credit.tanggalRestrukturisasiAkhir.Value) : null,
        credit.kreditProgramPemerintahKet,
        credit.restrukturisasiKet,
        credit.lokasiProyekKet,
        credit.kondisiKet,
        credit.kodeValuta,
        credit.tanggalKondisi.HasValue ? dateToLongDesc(credit.tanggalKondisi.Value) : null,
        credit.sukuBungaImbalan.HasValue ? credit.sukuBungaImbalan.Value.ToString("###,##0.##########") + " %" : null,
        credit.jenisSukuBungaImbalanKet,
        credit.keterangan
        );

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Agunan<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");

                    if (credit.agunan != null)
                    {
                        sb.Append(GetHTMLCollateral(credit.agunan));
                    }

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Penjamin<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                    if (credit.penjamin != null)
                    {
                        sb.Append(GetHTMLGuarantor(credit.penjamin));
                    }
                }
            }
            #endregion

            #region lcs
            if (facility.lcs.Count == 0)
            {
                sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Irrevocable L/C</div>
");
            }
            else
            {
                foreach (var lc in facility.lcs)
                {
                    sb.AppendFormat(@"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Irrevocable L/C</div>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding:5px 10px 5px 10px;' cellspacing='0'>
        <tr style='color: #f24f4f;'>
            <td width='23%'>Pelapor</td>
            <td width='31%'>Cabang</td>
            <td width='23%'>Nominal</td>
            <td width='23%'>Tanggal Update</td>
        </tr>
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td>{4}</td>
            <td width='1%'>&nbsp;</td>
            <td>{5}</td>
            <td width='1%'>&nbsp;</td>
            <td>{6}</td>
            <td width='1%'>&nbsp;</td>
            <td>{7}</td>
            <td width='1%'>&nbsp;</td>
            <td>{8}</td>
            <td width='1%'>&nbsp;</td>
            <td>{9}</td>
            <td width='1%'>&nbsp;</td>
            <td>{10}</td>
            <td width='1%'>&nbsp;</td>
            <td>{11}</td>
            <td width='1%'>&nbsp;</td>
            <td>{12}</td>
            <td width='1%'>&nbsp;</td>
            <td>{13}</td>
            <td width='1%'>&nbsp;</td>
            <td>{14}</td>
            <td width='1%'>&nbsp;</td>
            <td>{15}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>Kualitas</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{16}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{17}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{18}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{19}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{20}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{21}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{22}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{23}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{24}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{25}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{26}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{27}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td>{28}</td>
            <td width='1%'>&nbsp;</td>
            <td>{29}</td>
            <td width='1%'>&nbsp;</td>
            <td>{30}</td>
            <td width='1%'>&nbsp;</td>
            <td>{31}</td>
            <td width='1%'>&nbsp;</td>
            <td>{32}</td>
            <td width='1%'>&nbsp;</td>
            <td>{33}</td>
            <td width='1%'>&nbsp;</td>
            <td>{34}</td>
            <td width='1%'>&nbsp;</td>
            <td>{35}</td>
            <td width='1%'>&nbsp;</td>
            <td>{36}</td>
            <td width='1%'>&nbsp;</td>
            <td>{37}</td>
            <td width='1%'>&nbsp;</td>
            <td>{38}</td>
            <td width='1%'>&nbsp;</td>
            <td>{39}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>&nbsp<br />&nbsp</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{40}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{41}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{42}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{43}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{44}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{45}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{46}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{47}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{48}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{49}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{50}</td>
            <td width='1%'>&nbsp;</td>
            <td width='6%' style='border:1px solid #d9d9d9;text-align:center'>{51}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>No L/C</td>
            <td width='25%'>{52}</td>
            <td width='25%' style='background: #f2f2f2;'>Kualitas</td>
            <td width='25%'>{53}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis L/C</td>
            <td>{54}</td>
            <td style='background: #f2f2f2;'>Valuta</td>
            <td>{55}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Keluar </td>
            <td>{56}</td>
            <td style='background: #f2f2f2;'>Plafon</td>
            <td>{57}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Jatuh Tempo</td>
            <td>{58}</td>
            <td style='background: #f2f2f2;'>Tujuan L/C</td>
            <td>{59}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Awal</td>
            <td>{60}</td>
            <td style='background: #f2f2f2;'>Setoran Jaminan</td>
            <td>{61}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Awal</td>
            <td>{62}</td>
            <td style='background: #f2f2f2;'>Tanggal Wan Prestasi</td>
            <td>{63}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Akhir</td>
            <td>{64}</td>
            <td style='background: #f2f2f2;'>Kondisi</td>
            <td>{65}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Akhir</td>
            <td>{66}</td>
            <td style='background: #f2f2f2;'>Tanggal Kondisi</td>
            <td>{67}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Bank Beneficiary</td>
            <td colspan='3'>{68}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{69}</td>
        </tr>
    </table>
    <br />
",
        lc.ljk + " - " + lc.ljkKet,
        lc.cabangKet,
        lc.lcAmount.HasValue ? lc.lcAmount.Value.ToString("Rp ###,##0.00", culture) : null,
        lc.tanggalUpdate.HasValue ? dateToLongDesc(lc.tanggalUpdate.Value) : lc.tanggalDibentuk.HasValue ? dateToLongDesc(lc.tanggalDibentuk.Value) : null,

        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan01 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan02 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan03 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan04 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan05 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan06 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan07 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan08 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan09 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan10 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan11 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan12 + "01", "yyyyMMdd", null)),

        lc.tahunBulan01Kol,
        lc.tahunBulan02Kol,
        lc.tahunBulan03Kol,
        lc.tahunBulan04Kol,
        lc.tahunBulan05Kol,
        lc.tahunBulan06Kol,
        lc.tahunBulan07Kol,
        lc.tahunBulan08Kol,
        lc.tahunBulan09Kol,
        lc.tahunBulan10Kol,
        lc.tahunBulan11Kol,
        lc.tahunBulan12Kol,

        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan13 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan14 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan15 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan16 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan17 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan18 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan19 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan20 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan21 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan22 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan23 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(lc.tahunBulan24 + "01", "yyyyMMdd", null)),

        lc.tahunBulan13Kol,
        lc.tahunBulan14Kol,
        lc.tahunBulan15Kol,
        lc.tahunBulan16Kol,
        lc.tahunBulan17Kol,
        lc.tahunBulan18Kol,
        lc.tahunBulan19Kol,
        lc.tahunBulan20Kol,
        lc.tahunBulan21Kol,
        lc.tahunBulan22Kol,
        lc.tahunBulan23Kol,
        lc.tahunBulan24Kol,

        lc.noLc,
        lc.kualitas + " - " + lc.kualitasKet,
        lc.jenisLcKet,
        lc.kodeValuta,
        lc.tanggalKeluar.HasValue ? dateToLongDesc(lc.tanggalKeluar.Value) : null,
        lc.plafon.HasValue ? lc.plafon.Value.ToString("Rp ###,##0.00", culture) : null,
        lc.tanggalJthTempo.HasValue ? dateToLongDesc(lc.tanggalJthTempo.Value) : null,
        lc.tujuanLcKet,
        lc.noAkadAwal,
        lc.setoraJaminan.HasValue ? lc.setoraJaminan.Value.ToString("Rp ###,##0.00", culture) : null,
        lc.tanggalAkadAwal.HasValue ? dateToLongDesc(lc.tanggalAkadAwal.Value) : null,
        lc.tanggalWanPrestasi.HasValue ? dateToLongDesc(lc.tanggalWanPrestasi.Value) : null,
        lc.noAkadAkhir,
        lc.kondisiKet,
        lc.tanggalAkadAkhir.HasValue ? dateToLongDesc(lc.tanggalAkadAkhir.Value) : null,
        lc.tanggalKondisi.HasValue ? dateToLongDesc(lc.tanggalKondisi.Value) : null,
        lc.bankBeneficiary,
        lc.keterangan
        );
                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Agunan<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");

                    if (lc.agunan != null)
                    {
                        sb.Append(GetHTMLCollateral(lc.agunan));
                    }

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Penjamin<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");

                    if (lc.penjamin != null)
                    {
                        sb.Append(GetHTMLGuarantor(lc.penjamin));
                    }
                }
            }
            #endregion

            #region bg
            if (facility.garansiYgDiberikan.Count == 0)
            {
                sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Garansi Yang Diberikan</div>
");
            }
            else
            {
                foreach (var bg in facility.garansiYgDiberikan)
                {
                    sb.AppendFormat(@"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Garansi Yang Diberikan</div>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding:5px 10px 5px 10px;' cellspacing='0'>
        <tr style='color: #f24f4f;'>
            <td width='23%'>Pelapor</td>
            <td width='31%'>Cabang</td>
            <td width='23%'>Nominal</td>
            <td width='23%'>Tanggal Update</td>
        </tr>
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td>{4}</td>
            <td width='1%'>&nbsp;</td>
            <td>{5}</td>
            <td width='1%'>&nbsp;</td>
            <td>{6}</td>
            <td width='1%'>&nbsp;</td>
            <td>{7}</td>
            <td width='1%'>&nbsp;</td>
            <td>{8}</td>
            <td width='1%'>&nbsp;</td>
            <td>{9}</td>
            <td width='1%'>&nbsp;</td>
            <td>{10}</td>
            <td width='1%'>&nbsp;</td>
            <td>{11}</td>
            <td width='1%'>&nbsp;</td>
            <td>{12}</td>
            <td width='1%'>&nbsp;</td>
            <td>{13}</td>
            <td width='1%'>&nbsp;</td>
            <td>{14}</td>
            <td width='1%'>&nbsp;</td>
            <td>{15}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>Kualitas</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{16}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{17}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{18}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{19}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{20}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{21}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{22}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{23}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{24}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{25}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{26}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{27}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td>{28}</td>
            <td width='1%'>&nbsp;</td>
            <td>{29}</td>
            <td width='1%'>&nbsp;</td>
            <td>{30}</td>
            <td width='1%'>&nbsp;</td>
            <td>{31}</td>
            <td width='1%'>&nbsp;</td>
            <td>{32}</td>
            <td width='1%'>&nbsp;</td>
            <td>{33}</td>
            <td width='1%'>&nbsp;</td>
            <td>{34}</td>
            <td width='1%'>&nbsp;</td>
            <td>{35}</td>
            <td width='1%'>&nbsp;</td>
            <td>{36}</td>
            <td width='1%'>&nbsp;</td>
            <td>{37}</td>
            <td width='1%'>&nbsp;</td>
            <td>{38}</td>
            <td width='1%'>&nbsp;</td>
            <td>{39}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>&nbsp<br />&nbsp</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{40}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{41}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{42}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{43}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{44}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{45}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{46}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{47}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{48}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{49}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{50}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{51}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>No Rekening</td>
            <td width='25%'>{52}</td>
            <td width='25%' style='background: #f2f2f2;'>Kualitas</td>
            <td width='25%'>{53}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Garansi</td>
            <td>{54}</td>
            <td style='background: #f2f2f2;'>Valuta</td>
            <td>{55}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Diterbitkan </td>
            <td>{56}</td>
            <td style='background: #f2f2f2;'>Plafon</td>
            <td>{57}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Jatuh Tempo</td>
            <td>{58}</td>
            <td style='background: #f2f2f2;'>Tujuan Garansi</td>
            <td>{59}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Awal</td>
            <td>{60}</td>
            <td style='background: #f2f2f2;'>Setoran Jaminan</td>
            <td>{61}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Awal</td>
            <td>{62}</td>
            <td style='background: #f2f2f2;'>Tanggal Wan Prestasi</td>
            <td>{63}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>No Akad Akhir</td>
            <td>{64}</td>
            <td style='background: #f2f2f2;'>Kondisi</td>
            <td>{65}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Akad Akhir</td>
            <td>{66}</td>
            <td style='background: #f2f2f2;'>Tanggal Kondisi</td>
            <td>{67}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Nama Yang Dijamin</td>
            <td colspan='3'>{68}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{69}</td>
        </tr>
    </table>
    <br />
",
        bg.ljk + " - " + bg.ljkKet,
        bg.cabangKet,
        bg.nominalBg.HasValue ? bg.nominalBg.Value.ToString("Rp ###,##0.00", culture) : null,
        bg.tanggalUpdate.HasValue ? dateToLongDesc(bg.tanggalUpdate.Value) : bg.tanggalDibentuk.HasValue ? dateToLongDesc(bg.tanggalDibentuk.Value) : null,

        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan01 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan02 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan03 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan04 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan05 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan06 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan07 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan08 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan09 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan10 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan11 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan12 + "01", "yyyyMMdd", null)),

        bg.tahunBulan01Kol,
        bg.tahunBulan02Kol,
        bg.tahunBulan03Kol,
        bg.tahunBulan04Kol,
        bg.tahunBulan05Kol,
        bg.tahunBulan06Kol,
        bg.tahunBulan07Kol,
        bg.tahunBulan08Kol,
        bg.tahunBulan09Kol,
        bg.tahunBulan10Kol,
        bg.tahunBulan11Kol,
        bg.tahunBulan12Kol,

        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan13 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan14 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan15 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan16 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan17 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan18 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan19 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan20 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan21 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan22 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan23 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(bg.tahunBulan24 + "01", "yyyyMMdd", null)),

        bg.tahunBulan13Kol,
        bg.tahunBulan14Kol,
        bg.tahunBulan15Kol,
        bg.tahunBulan16Kol,
        bg.tahunBulan17Kol,
        bg.tahunBulan18Kol,
        bg.tahunBulan19Kol,
        bg.tahunBulan20Kol,
        bg.tahunBulan21Kol,
        bg.tahunBulan22Kol,
        bg.tahunBulan23Kol,
        bg.tahunBulan24Kol,

        bg.accountNo,
        bg.kualitas + " - " + bg.kualitasKet,
        bg.jenisGaransiKet,
        bg.kodeValuta,
        bg.tanggalDiterbitkan.HasValue ? dateToLongDesc(bg.tanggalDiterbitkan.Value) : null,
        bg.plafon.HasValue ? bg.plafon.Value.ToString("Rp ###,##0.00", culture) : null,
        bg.tanggalJatuhTempo.HasValue ? dateToLongDesc(bg.tanggalJatuhTempo.Value) : null,
        bg.tujuanGaransiKet,
        bg.noAkadAwal,
        bg.setoranJaminan.HasValue ? bg.setoranJaminan.Value.ToString("Rp ###,##0.00", culture) : null,
        bg.tanggalAkadAwal.HasValue ? dateToLongDesc(bg.tanggalAkadAwal.Value) : null,
        bg.tanggalWanPrestasi.HasValue ? dateToLongDesc(bg.tanggalWanPrestasi.Value) : null,
        bg.noAkadAkhir,
        bg.kondisiKet,
        bg.tanggalAkadAkhir.HasValue ? dateToLongDesc(bg.tanggalAkadAkhir.Value) : null,
        bg.tanggalKondisi.HasValue ? dateToLongDesc(bg.tanggalKondisi.Value) : null,
        bg.namaYangDijamin,
        bg.keterangan
        );

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Agunan<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                    if (bg.agunan != null)
                    {
                        sb.Append(GetHTMLCollateral(bg.agunan));
                    }

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Penjamin<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                    if (bg.penjamin != null)
                    {
                        sb.Append(GetHTMLGuarantor(bg.penjamin));
                    }
                }
            }
            #endregion

            #region security
            if (corporate != null)
            {
                if (facility.suratBerharga.Count == 0)
                {
                    sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Surat Berharga</div>
");
                }
                else
                {
                    foreach (var security in facility.suratBerharga)
                    {
                        sb.AppendFormat(@"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Surat Berharga</div>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding:5px 10px 5px 10px;' cellspacing='0'>
        <tr style='color: #f24f4f;'>
            <td width='23%'>Pelapor</td>
            <td width='31%'>Cabang</td>
            <td width='23%'>Nominal</td>
            <td width='23%'>Tanggal Update</td>
        </tr>
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{4}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{5}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{6}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{7}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{8}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{9}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{10}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{11}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{12}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{13}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{14}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{15}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>Kualitas / Jumlah<br />Hari Tunggakan</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{16}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{17}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{18}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{19}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{20}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{21}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{22}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{23}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{24}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{25}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{26}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{27}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{28}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{29}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{30}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{31}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{32}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{33}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{34}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{35}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{36}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{37}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{38}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{39}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{40}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{41}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{42}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{43}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{44}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{45}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{46}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{47}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{48}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{49}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{50}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{51}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>&nbsp<br />&nbsp</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{52}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{53}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{54}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{55}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{56}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{57}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{58}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{59}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{60}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{61}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{62}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{63}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{64}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{65}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{66}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{67}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{68}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{69}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{70}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{71}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{72}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{73}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{74}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{75}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>No Surat Berharga</td>
            <td width='25%'>{76}</td>
            <td width='25%' style='background: #f2f2f2;'>Kualitas</td>
            <td width='25%'>{77}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Surat Berharga</td>
            <td>{78}</td>
            <td style='background: #f2f2f2;'>Jumlah Hari Tunggakan</td>
            <td>{79}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Sovereign Rate</td>
            <td>{80}</td>
            <td style='background: #f2f2f2;'>Nilai Dalam Mata Uang Asal</td>
            <td>{81}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Listing</td>
            <td>{82}</td>
            <td style='background: #f2f2f2;'>Nilai Pasar</td>
            <td>{83}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Peringkat Surat Berharga</td>
            <td>{84}</td>
            <td style='background: #f2f2f2;'>Nilai Perolehan</td>
            <td>{85}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tujuan Kepemilikan</td>
            <td>{86}</td>
            <td style='background: #f2f2f2;'>Tunggakan</td>
            <td>{87}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Terbit</td>
            <td>{88}</td>
            <td style='background: #f2f2f2;'>Tanggal Macet</td>
            <td>{89}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Jatuh Tempo</td>
            <td>{90}</td>
            <td style='background: #f2f2f2;'>Sebab Macet</td>
            <td>{91}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Suku Bunga/Imbalan</td>
            <td>{92}</td>
            <td style='background: #f2f2f2;'>Kondisi</td>
            <td>{93}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Kode Valuta</td>
            <td>{94}</td>
            <td style='background: #f2f2f2;'>Tanggal Kondisi</td>
            <td>{95}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{96}</td>
        </tr>
    </table>
    <br />
",
            security.ljk + " - " + security.ljkKet,
            security.cabangKet,
            security.nominalSb.HasValue ? security.nominalSb.Value.ToString("Rp ###,##0.00", culture) : null,
            security.tanggalUpdate.HasValue ? dateToLongDesc(security.tanggalUpdate.Value) : security.tanggalDibentuk.HasValue ? dateToLongDesc(security.tanggalDibentuk.Value) : null,

            dateToShortDesc(DateTime.ParseExact(security.tahunBulan01 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan02 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan03 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan04 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan05 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan06 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan07 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan08 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan09 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan10 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan11 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan12 + "01", "yyyyMMdd", null)),

            security.tahunBulan01Kol,
            security.tahunBulan01Ht.HasValue ? security.tahunBulan01Ht.Value.ToString() : null,
            security.tahunBulan02Kol,
            security.tahunBulan02Ht.HasValue ? security.tahunBulan02Ht.Value.ToString() : null,
            security.tahunBulan03Kol,
            security.tahunBulan03Ht.HasValue ? security.tahunBulan03Ht.Value.ToString() : null,
            security.tahunBulan04Kol,
            security.tahunBulan04Ht.HasValue ? security.tahunBulan04Ht.Value.ToString() : null,
            security.tahunBulan05Kol,
            security.tahunBulan05Ht.HasValue ? security.tahunBulan05Ht.Value.ToString() : null,
            security.tahunBulan06Kol,
            security.tahunBulan06Ht.HasValue ? security.tahunBulan06Ht.Value.ToString() : null,
            security.tahunBulan07Kol,
            security.tahunBulan07Ht.HasValue ? security.tahunBulan07Ht.Value.ToString() : null,
            security.tahunBulan08Kol,
            security.tahunBulan08Ht.HasValue ? security.tahunBulan08Ht.Value.ToString() : null,
            security.tahunBulan09Kol,
            security.tahunBulan09Ht.HasValue ? security.tahunBulan09Ht.Value.ToString() : null,
            security.tahunBulan10Kol,
            security.tahunBulan10Ht.HasValue ? security.tahunBulan10Ht.Value.ToString() : null,
            security.tahunBulan11Kol,
            security.tahunBulan11Ht.HasValue ? security.tahunBulan11Ht.Value.ToString() : null,
            security.tahunBulan12Kol,
            security.tahunBulan12Ht.HasValue ? security.tahunBulan12Ht.Value.ToString() : null,

            dateToShortDesc(DateTime.ParseExact(security.tahunBulan13 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan14 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan15 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan16 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan17 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan18 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan19 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan20 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan21 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan22 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan23 + "01", "yyyyMMdd", null)),
            dateToShortDesc(DateTime.ParseExact(security.tahunBulan24 + "01", "yyyyMMdd", null)),

            security.tahunBulan13Kol,
            security.tahunBulan13Ht.HasValue ? security.tahunBulan13Ht.Value.ToString() : null,
            security.tahunBulan14Kol,
            security.tahunBulan14Ht.HasValue ? security.tahunBulan14Ht.Value.ToString() : null,
            security.tahunBulan15Kol,
            security.tahunBulan15Ht.HasValue ? security.tahunBulan15Ht.Value.ToString() : null,
            security.tahunBulan16Kol,
            security.tahunBulan16Ht.HasValue ? security.tahunBulan16Ht.Value.ToString() : null,
            security.tahunBulan17Kol,
            security.tahunBulan17Ht.HasValue ? security.tahunBulan17Ht.Value.ToString() : null,
            security.tahunBulan18Kol,
            security.tahunBulan18Ht.HasValue ? security.tahunBulan18Ht.Value.ToString() : null,
            security.tahunBulan19Kol,
            security.tahunBulan19Ht.HasValue ? security.tahunBulan19Ht.Value.ToString() : null,
            security.tahunBulan20Kol,
            security.tahunBulan20Ht.HasValue ? security.tahunBulan20Ht.Value.ToString() : null,
            security.tahunBulan21Kol,
            security.tahunBulan21Ht.HasValue ? security.tahunBulan21Ht.Value.ToString() : null,
            security.tahunBulan22Kol,
            security.tahunBulan22Ht.HasValue ? security.tahunBulan22Ht.Value.ToString() : null,
            security.tahunBulan23Kol,
            security.tahunBulan23Ht.HasValue ? security.tahunBulan23Ht.Value.ToString() : null,
            security.tahunBulan24Kol,
            security.tahunBulan24Ht.HasValue ? security.tahunBulan24Ht.Value.ToString() : null,

            security.noSuratBerharga,
            security.kualitas + " - " + security.kualitasKet,
            security.jenisSuratBerhargaKet,
            security.jumlahHariTunggakan,

            security.sovereignRate,
            security.nilaiDalamMataUangAsal.HasValue ? security.nilaiDalamMataUangAsal.Value.ToString("###,##0.00", culture) : null,
            security.listing == "Y" ? "Ya" : "Tidak",
            security.nilaiPasar.HasValue ? security.nilaiPasar.Value.ToString("Rp ###,##0.00", culture) : null,
            security.peringkatSuratBerharga,
            security.nilaiPerolehan.HasValue ? security.nilaiPerolehan.Value.ToString("Rp ###,##0.00", culture) : null,
            security.tujuanKepemilikanKet,
            security.tunggakan.HasValue ? security.tunggakan.Value.ToString("Rp ###,##0.00", culture) : null,
            security.tanggalTerbit.HasValue ? dateToLongDesc(security.tanggalTerbit.Value) : null,
            security.tanggalMacet.HasValue ? dateToLongDesc(security.tanggalMacet.Value) : null,
            security.tanggalJatuhTempo.HasValue ? dateToLongDesc(security.tanggalJatuhTempo.Value) : null,
            security.sebabMacetKet,
            security.sukuBungaImbalan.HasValue ? security.sukuBungaImbalan.Value.ToString("###,##0.##########") + " %" : null,
            security.kondisiKet,
            security.kodeValuta,
            security.tanggalKondisi.HasValue ? dateToLongDesc(security.tanggalKondisi.Value) : null,
            security.keterangan
            );

                        sb.AppendFormat(
                    @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Agunan<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                        if (security.agunan != null)
                        {
                            sb.Append(GetHTMLCollateral(security.agunan));
                        }

                        sb.AppendFormat(
                    @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Penjamin<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                        if (security.penjamin != null)
                        {
                            sb.Append(GetHTMLGuarantor(security.penjamin));
                        }
                    }
                }

            }
            #endregion  //

            #region other
            if (facility.fasilitasLain.Count == 0)
            {
                sb.AppendFormat(@"    
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Fasilitas Lain</div>
");
            }
            else
            {
                foreach (var other in facility.fasilitasLain)
                {
                    sb.AppendFormat(@"
    <div style='font-size: 16px; font-family: CenturyGothic, AppleGothic, sans-serif; font-weight: bold; color: #fff; background-color: #f24f4f; line-height: 35px; padding-left: 10px;margin-bottom:5px;'>Fasilitas Lain</div>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding:5px 10px 5px 10px;' cellspacing='0'>
        <tr style='color: #f24f4f;'>
            <td width='23%'>Pelapor</td>
            <td width='31%'>Cabang</td>
            <td width='23%'>Nominal</td>
            <td width='23%'>Tanggal Update</td>
        </tr>
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{4}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{5}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{6}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{7}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{8}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{9}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{10}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{11}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{12}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{13}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{14}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{15}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>Kualitas / Jumlah<br />Hari Tunggakan</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{16}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{17}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{18}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{19}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{20}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{21}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{22}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{23}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{24}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{25}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{26}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{27}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{28}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{29}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{30}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{31}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{32}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{33}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{34}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{35}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{36}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{37}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{38}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{39}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; text-align:center;margin-top:10px;'>
        <tr style='font-weight:bold;'>
            <td>&nbsp;</td>
            <td colspan='2'>{40}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{41}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{42}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{43}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{44}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{45}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{46}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{47}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{48}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{49}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{50}</td>
            <td width='1%'>&nbsp;</td>
            <td colspan='2'>{51}</td>
        </tr>
        <tr>
            <td width='17%' style='text-align:left'>&nbsp<br />&nbsp</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{52}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{53}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{54}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{55}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{56}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{57}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{58}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{59}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{60}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{61}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{62}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{63}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{64}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{65}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{66}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{67}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{68}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{69}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{70}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{71}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{72}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{73}</td>
            <td width='1%'>&nbsp;</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{74}</td>
            <td width='3%' style='border:1px solid #d9d9d9;text-align:center'>{75}</td>
            <td width='1%'>&nbsp;</td>
        </tr>
    </table>
    <br />
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>No Rekening</td>
            <td width='25%'>{76}</td>
            <td width='25%' style='background: #f2f2f2;'>Kualitas</td>
            <td width='25%'>{77}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Fasilitas</td>
            <td>{78}</td>
            <td style='background: #f2f2f2;'>Jumlah Hari Tunggakan</td>
            <td>{79}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Mulai </td>
            <td>{80}</td>
            <td style='background: #f2f2f2;'>Tanggal Macet</td>
            <td>{81}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Jatuh Tempo</td>
            <td>{82}</td>
            <td style='background: #f2f2f2;'>Sebab Macet</td>
            <td>{83}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Valuta</td>
            <td>{84}</td>
            <td style='background: #f2f2f2;'>Tunggakan</td>
            <td>{85}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Nilai Dalam Mata Uang Asal</td>
            <td>{86}</td>
            <td style='background: #f2f2f2;'>Kondisi</td>
            <td>{87}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Suku Bunga/Imbalan</td>
            <td>{88}</td>
            <td style='background: #f2f2f2;'>Tanggal Kondisi</td>
            <td>{89}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{90}</td>
        </tr>
    </table>
    <br />
",
        other.ljk + " - " + other.ljkKet,
        other.cabangKet,
        other.nominalJumlahKwajibanIDR.HasValue ? other.nominalJumlahKwajibanIDR.Value.ToString("Rp ###,##0.00", culture) : null,
        other.tanggalUpdate.HasValue ? dateToLongDesc(other.tanggalUpdate.Value) : other.tanggalDibentuk.HasValue ? dateToLongDesc(other.tanggalDibentuk.Value) : null,

        dateToShortDesc(DateTime.ParseExact(other.tahunBulan01 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan02 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan03 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan04 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan05 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan06 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan07 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan08 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan09 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan10 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan11 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan12 + "01", "yyyyMMdd", null)),

        other.tahunBulan01Kol,
        other.tahunBulan01Ht.HasValue ? other.tahunBulan01Ht.Value.ToString() : null,
        other.tahunBulan02Kol,
        other.tahunBulan02Ht.HasValue ? other.tahunBulan02Ht.Value.ToString() : null,
        other.tahunBulan03Kol,
        other.tahunBulan03Ht.HasValue ? other.tahunBulan03Ht.Value.ToString() : null,
        other.tahunBulan04Kol,
        other.tahunBulan04Ht.HasValue ? other.tahunBulan04Ht.Value.ToString() : null,
        other.tahunBulan05Kol,
        other.tahunBulan05Ht.HasValue ? other.tahunBulan05Ht.Value.ToString() : null,
        other.tahunBulan06Kol,
        other.tahunBulan06Ht.HasValue ? other.tahunBulan06Ht.Value.ToString() : null,
        other.tahunBulan07Kol,
        other.tahunBulan07Ht.HasValue ? other.tahunBulan07Ht.Value.ToString() : null,
        other.tahunBulan08Kol,
        other.tahunBulan08Ht.HasValue ? other.tahunBulan08Ht.Value.ToString() : null,
        other.tahunBulan09Kol,
        other.tahunBulan09Ht.HasValue ? other.tahunBulan09Ht.Value.ToString() : null,
        other.tahunBulan10Kol,
        other.tahunBulan10Ht.HasValue ? other.tahunBulan10Ht.Value.ToString() : null,
        other.tahunBulan11Kol,
        other.tahunBulan11Ht.HasValue ? other.tahunBulan11Ht.Value.ToString() : null,
        other.tahunBulan12Kol,
        other.tahunBulan12Ht.HasValue ? other.tahunBulan12Ht.Value.ToString() : null,

        dateToShortDesc(DateTime.ParseExact(other.tahunBulan13 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan14 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan15 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan16 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan17 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan18 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan19 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan20 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan21 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan22 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan23 + "01", "yyyyMMdd", null)),
        dateToShortDesc(DateTime.ParseExact(other.tahunBulan24 + "01", "yyyyMMdd", null)),

        other.tahunBulan13Kol,
        other.tahunBulan13Ht.HasValue ? other.tahunBulan13Ht.Value.ToString() : null,
        other.tahunBulan14Kol,
        other.tahunBulan14Ht.HasValue ? other.tahunBulan14Ht.Value.ToString() : null,
        other.tahunBulan15Kol,
        other.tahunBulan15Ht.HasValue ? other.tahunBulan15Ht.Value.ToString() : null,
        other.tahunBulan16Kol,
        other.tahunBulan16Ht.HasValue ? other.tahunBulan16Ht.Value.ToString() : null,
        other.tahunBulan17Kol,
        other.tahunBulan17Ht.HasValue ? other.tahunBulan17Ht.Value.ToString() : null,
        other.tahunBulan18Kol,
        other.tahunBulan18Ht.HasValue ? other.tahunBulan18Ht.Value.ToString() : null,
        other.tahunBulan19Kol,
        other.tahunBulan19Ht.HasValue ? other.tahunBulan19Ht.Value.ToString() : null,
        other.tahunBulan20Kol,
        other.tahunBulan20Ht.HasValue ? other.tahunBulan20Ht.Value.ToString() : null,
        other.tahunBulan21Kol,
        other.tahunBulan21Ht.HasValue ? other.tahunBulan21Ht.Value.ToString() : null,
        other.tahunBulan22Kol,
        other.tahunBulan22Ht.HasValue ? other.tahunBulan22Ht.Value.ToString() : null,
        other.tahunBulan23Kol,
        other.tahunBulan23Ht.HasValue ? other.tahunBulan23Ht.Value.ToString() : null,
        other.tahunBulan24Kol,
        other.tahunBulan24Ht.HasValue ? other.tahunBulan24Ht.Value.ToString() : null,

        other.noRekening,
        other.kualitas + " - " + other.kualitasKet,
        other.jenisFasilitasKet,
        other.jumlahHariTunggakan,
        other.tanggalMulai.HasValue ? dateToLongDesc(other.tanggalMulai.Value) : null,
        other.tanggalMacet.HasValue ? dateToLongDesc(other.tanggalMacet.Value) : null,
        other.tanggalJatuhTempo.HasValue ? dateToLongDesc(other.tanggalJatuhTempo.Value) : null,
        other.sebabMacetDesc,
        other.kodeValuta,
        other.tunggakan.HasValue ? other.tunggakan.Value.ToString("Rp ###,##0.00", culture) : null,
        other.nilaiDalamMataUangAsal.HasValue ? other.nilaiDalamMataUangAsal.Value.ToString("###,##0.00", culture) : null,
        other.kondisiKet,
        other.sukuBungaImbalan.HasValue ? other.sukuBungaImbalan.Value.ToString("###,##0.##########") + " %" : null,
        other.tanggalKondisi.HasValue ? dateToLongDesc(other.tanggalKondisi.Value) : null,
        other.keterangan
        );

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Agunan<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                    if (other.agunan != null)
                    {
                        sb.Append(GetHTMLCollateral(other.agunan));
                    }

                    sb.AppendFormat(
                @"
    <div style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; font-size:15px;margin-bottom:15px'>Penjamin<hr noshade style='width: 100%; border-color: #ffd1d1; border-width: 1px; background-color: ffd1d1;'></div>
");
                    if (other.penjamin != null)
                    {
                        sb.Append(GetHTMLGuarantor(other.penjamin));
                    }
                }
            }
            #endregion

            #region footer
            if (addFooter)
            {
                sb.Append(@"
    <div style='border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px;' ></div>
");
                sb.AppendFormat(@"
    <div>
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Nomor Laporan</span> &nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{0}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Operator</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{1}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Tanggal Permintaan</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{2}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Kode Ref. Pengguna</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{3}</span>
    </div>
    <div>
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Posisi data Terakhir</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{4}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>  LJK</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{5} - {6}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>  Peruntukan</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{7}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Tanggal Dibentuk</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{8}</span>&nbsp;&nbsp;
    </div>
"
    ,
                reportNumber,
                header.dibuatOleh,
                header.tanggalPermintaan.HasValue ? dateToLongDesc(header.tanggalPermintaan.Value) + " " + header.tanggalPermintaan.Value.ToString("HH:mm:ss") : null,
                header.kodeReferensiPengguna,
                latestDataYearMonth,
                header.kodeLJKPermintaan,
                header.kodeCabangPermintaan,
                header.kodeTujuanPermintaan,
                header.tanggalHasil.HasValue ? dateToLongDesc(header.tanggalHasil.Value) + " " + header.tanggalHasil.Value.ToString("HH:mm:ss") : null
    );
            }

            #endregion

            sb.AppendFormat(@"    
</body>
</html>
");

            string pathImg = imgPath;

            FileStream fs = new FileStream(Path.Combine(pathImg, "logo-ideb.png"), FileMode.Open, FileAccess.Read);
            byte[] filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string imgLogoIdeb = "data:image/png;base64," + Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
            fs.Close();
            fs = new FileStream(Path.Combine(pathImg, "logo-ojk.png"), FileMode.Open, FileAccess.Read);
            filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string imgLogoOjk = "data:image/png;base64," + Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
            fs.Close();
            fs = new FileStream(Path.Combine(pathImg, "info.png"), FileMode.Open, FileAccess.Read);
            filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string imgInfo = "data:image/png;base64," + Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
            fs.Close();

            sb = sb.Replace("logo-ideb.png", imgLogoIdeb);
            sb = sb.Replace("logo-ojk.png", imgLogoOjk);
            sb = sb.Replace("info.png", imgInfo);

            return sb.ToString();
        }

        public static string GetHTMLFooterString(Ideb ideb)
        {
            string latestDataYearMonth = null;
            string reportNumber = null;
            if (ideb.individual != null)
            {
                latestDataYearMonth = DateTime.ParseExact(ideb.individual.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                // latestDataYearMonth = dateToLongDesc(ideb.individual.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = ideb.individual.nomorLaporan;
            }
            else
            {
                latestDataYearMonth = DateTime.ParseExact(ideb.perusahaan.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                // latestDataYearMonth = dateToLongDesc(ideb.perusahaan.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = ideb.perusahaan.nomorLaporan;
            }
            var sb = new StringBuilder();
            sb.Append(@"
<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>

<head>
    <title></title>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
</head>
<body>
    <!-- 
    <div style='border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px;' ></div>
    -->
");
            sb.AppendFormat(@"
    <div>
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Nomor Laporan</span> &nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{0}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Operator</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{1}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Tanggal Permintaan</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{2}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Kode Ref. Pengguna</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{3}</span>
    </div>
    <div>
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Posisi data Terakhir</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{4}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>  LJK</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{5} - {6}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>  Peruntukan</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{7}</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(241,78,78); font-weight: normal; font-style: italic; text-decoration: none'>|</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: normal; font-style: italic; text-decoration: none'>Tanggal Dibentuk</span>&nbsp;&nbsp;
        <span style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: italic; text-decoration: none'>{8}</span>&nbsp;&nbsp;
    </div>
    <!-- 
	<div style='font-family: CenturyGothic, AppleGothic, sans-serif; font-size: 6px; color: rgb(75,71,60); font-weight: bolder; font-style: normal; text-decoration: none; text-align: right; vertical-align: top;'>
		<span >Halaman [page] dari [toPage]</span>
	</div>
    -->
"
,
            reportNumber,
            ideb.header.dibuatOleh,
            ideb.header.tanggalPermintaan.HasValue ? dateToLongDesc(ideb.header.tanggalPermintaan.Value) + " " + ideb.header.tanggalPermintaan.Value.ToString("HH:mm:ss") : null,
            ideb.header.kodeReferensiPengguna,
            latestDataYearMonth,
            ideb.header.kodeLJKPermintaan,
            ideb.header.kodeCabangPermintaan,
            ideb.header.kodeTujuanPermintaan,
            ideb.header.tanggalHasil.HasValue ? dateToLongDesc(ideb.header.tanggalHasil.Value) + " " + ideb.header.tanggalHasil.Value.ToString("HH:mm:ss") : null

);
            sb.Append(@"
</body>
</html>
");

            return sb.ToString();
        }

        public static string GetPlainFooterString(Ideb ideb)
        {
            string latestDataYearMonth = null;
            string reportNumber = null;
            if (ideb.individual != null)
            {
                latestDataYearMonth = DateTime.ParseExact(ideb.individual.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                //latestDataYearMonth = dateToLongDesc(ideb.individual.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = ideb.individual.nomorLaporan;
            }
            else
            {
                //latestDataYearMonth = DateTime.ParseExact(ideb.perusahaan.posisiDataTerakhir + "01", "yyyyMMdd", null).ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                latestDataYearMonth = dateToLongDesc(ideb.perusahaan.tanggalPermintaan.Value.AddDays(-1));
                reportNumber = ideb.perusahaan.nomorLaporan;
            }
            var sb = new StringBuilder();
            sb.AppendFormat(@"
 
Nomor Laporan {0}| Operator {1}| Tanggal Permintaan {2}| Kode Ref. Pengguna {3}
Posisi data Terakhir {4}| LJK {5} - {6}| Peruntukan {7}| Tanggal Dibentuk {8}"
,
            reportNumber,
            ideb.header.dibuatOleh,
            ideb.header.tanggalPermintaan.HasValue ? dateToLongDesc(ideb.header.tanggalPermintaan.Value) + " " + ideb.header.tanggalPermintaan.Value.ToString("HH:mm:ss") : null,
            ideb.header.kodeReferensiPengguna,
            latestDataYearMonth,
            ideb.header.kodeLJKPermintaan,
            ideb.header.kodeCabangPermintaan,
            ideb.header.kodeTujuanPermintaan,
            ideb.header.tanggalHasil.HasValue ? dateToLongDesc(ideb.header.tanggalHasil.Value) + " " + ideb.header.tanggalHasil.Value.ToString("HH:mm:ss") : null
);

            return sb.ToString();
        }

        private static string GetHTMLCollateral(List<Collateral> collaterals)
        {
            CultureInfo culture = new CultureInfo("id-ID");
            var sb = new StringBuilder();

            foreach (var agunan in collaterals)
            {
                sb.AppendFormat(
    @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding-bottom:10px;' cellspacing='0' cellpadding='2px'>
        <tr style='color: #f24f4f;'>
            <td width='30%'>Jenis Agunan</td>
            <td width='25%'>Nilai Agunan</td>
            <td width='20%'>Paripasu</td>
            <td width='25%'>Tanggal Update</td>
        </tr>
        <tr style='background: #f2f2f2;'>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;margin-top:-2px;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>Nomor Agunan</td>
            <td width='25%'>{4}</td>
            <td width='25%' style='background: #f2f2f2;'>Peringkat Agunan</td>
            <td width='25%'>{5}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Jenis Pengikatan</td>
            <td>{6}</td>
            <td style='background: #f2f2f2;'>Lembaga Pemeringkat</td>
            <td>{7}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Pengikatan</td>
            <td>{8}</td>
            <td style='background: #f2f2f2;'>Bukti Kepemilikan</td>
            <td>{9}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Nama Pemilik Agunan</td>
            <td>{10}</td>
            <td style='background: #f2f2f2;'>Nilai Agunan (NJOP) / Nilai Wajar</td>
            <td>{11}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Alamat Agunan</td>
            <td>{12}</td>
            <td style='background: #f2f2f2;'>Nilai Agunan Penilai Independen</td>
            <td>{13}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Kab/Kota Lokasi Agunan</td>
            <td>{14}</td>
            <td style='background: #f2f2f2;'>Nama Penilai Independen </td>
            <td>{15}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Tanggal Penilaian Pelapor </td>
            <td>{16}</td>
            <td style='background: #f2f2f2;'>Asuransi </td>
            <td>{17}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td>{18}</td>
            <td style='background: #f2f2f2;'>Tgl Penilaian Penilai Independen </td>
            <td>{19}</td>
        </tr>
    </table>
    <br />
",
    agunan.jenisAgunanKet,
    agunan.nilaiAgunanMenurutLJK.HasValue ? agunan.nilaiAgunanMenurutLJK.Value.ToString("Rp ###,##0.00", culture) : null,
    agunan.prosentaseParipasu.HasValue ? agunan.prosentaseParipasu.Value.ToString("###,##0.##########") + " %" : null, //paripasu, need to check wheather this is double or display as string
    agunan.tanggalUpdate.HasValue ? dateToLongDesc(agunan.tanggalUpdate.Value) : agunan.tanggalDibentuk.HasValue ? dateToLongDesc(agunan.tanggalDibentuk.Value) : null,

    agunan.nomorAgunan,
    agunan.peringkatAgunan,
    agunan.jenisPengikatanKet,
    agunan.lembagaPemeringkat,
    agunan.tanggalPengikatan.HasValue ? dateToLongDesc(agunan.tanggalPengikatan.Value) : null,
    agunan.buktiKepemilikan,
    agunan.namaPemilikAgunan,
    agunan.nilaiAgunanNjop.HasValue ? agunan.nilaiAgunanNjop.Value.ToString("Rp ###,##0.00", culture) : null,
    agunan.alamatAgunan,
    agunan.nilaiAgunanIndep.HasValue ? agunan.nilaiAgunanIndep.Value.ToString("Rp ###,##0.00", culture) : null,
    agunan.kabKotaLokasiAgunanKet,
    agunan.namaPenilaiIndep,
    agunan.tglPenilaianPelapor.HasValue ? dateToLongDesc(agunan.tglPenilaianPelapor.Value) : null,
    agunan.asuransi == "Y" ? "Ya" : "Tidak",
    agunan.keterangan,
    agunan.tanggalPenilaianPenilaiIndependen.HasValue ? dateToLongDesc(agunan.tanggalPenilaianPenilaiIndependen.Value) : null
    );
            }


            return sb.ToString();
        }

        private static string GetHTMLGuarantor(List<Guarantor> guarantors)
        {
            var sb = new StringBuilder();

            foreach (var penjamin in guarantors)
            {
                sb.AppendFormat(
    @"
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C;  border-style: solid; border-color: #f24f4f #fff #fff; border-width: 2px; background: #f2f2f2; padding-bottom:10px' cellspacing='0' cellpadding='2px'>
        <tr style='color: #f24f4f;'>
            <td width='30%'>Nama Penjamin</td>
            <td width='45%'>Nomor Identitas</td>
            <td width='25%'>Tanggal Update</td>
        </tr>
        <tr style='background: #f2f2f2;'>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
        </tr>
    </table>
    <table width='100%' style='font-size: 11px; font-family: CenturyGothic, AppleGothic, sans-serif; line-height: 16px; color: #544F4C; border-collapse:collapse;margin-top:-2px;' cellspacing='0' cellpadding='3px'>
        <tr style='border-bottom: 1px solid #cfcdcc; border-top: 1px solid #cfcdcc;'>
            <td width='25%' style='background: #f2f2f2;'>Golongan</td>
            <td width='25%'>{3}</td>
            <td width='25%' style='background: #f2f2f2;'>Alamat</td>
            <td width='25%'>{4}</td>
        </tr>
        <tr style='border-bottom: 1px solid #cfcdcc;'>
            <td style='background: #f2f2f2;'>Keterangan</td>
            <td colspan='3'>{5}</td>
        </tr>
    </table>
    <br />
",
    penjamin.namaPenjamin,
    penjamin.nomorIdentitas,
    penjamin.tanggalUpdate.HasValue ? dateToLongDesc(penjamin.tanggalUpdate.Value) : penjamin.tanggalBuat.HasValue ? dateToLongDesc(penjamin.tanggalBuat.Value) : null,

    penjamin.keteranganJenisPenjamin,
    penjamin.alamatPenjamin,
    penjamin.keterangan
    );
            }

            return sb.ToString();
        }

        private static string monthToLongDesc(int month)
        {
            var result = "";
            switch (month)
            {
                case 1:
                    result = "Januari";
                    break;
                case 2:
                    result = "Februari";
                    break;
                case 3:
                    result = "Maret";
                    break;
                case 4:
                    result = "April";
                    break;
                case 5:
                    result = "Mei";
                    break;
                case 6:
                    result = "Juni";
                    break;
                case 7:
                    result = "Juli";
                    break;
                case 8:
                    result = "Agustus";
                    break;
                case 9:
                    result = "September";
                    break;
                case 10:
                    result = "Oktober";
                    break;
                case 11:
                    result = "November";
                    break;
                case 12:
                    result = "Desember";
                    break;
                default:
                    break;
            }
            return result;
        }
        private static string monthToShortDesc(int month)
        {
            var result = "";
            switch (month)
            {
                case 1:
                    result = "Jan";
                    break;
                case 2:
                    result = "Feb";
                    break;
                case 3:
                    result = "Mar";
                    break;
                case 4:
                    result = "Apr";
                    break;
                case 5:
                    result = "Mei";
                    break;
                case 6:
                    result = "Jun";
                    break;
                case 7:
                    result = "Jul";
                    break;
                case 8:
                    result = "Agt";
                    break;
                case 9:
                    result = "Sep";
                    break;
                case 10:
                    result = "Okt";
                    break;
                case 11:
                    result = "Nov";
                    break;
                case 12:
                    result = "Des";
                    break;
                default:
                    break;
            }
            return result;
        }
        private static string dateToLongDesc(DateTime dt)
        {
            var date = dt.ToString("dd");
            var month = monthToLongDesc(dt.Month);
            var year = dt.ToString("yyyy");

            return date + " " + month + " " + year;
        }
        private static string dateToLongDesc(string dt)
        {
            var date = dt.Substring(6,2);
            var month = monthToLongDesc(int.Parse(dt.Substring(4, 2)));
            var year = dt.Substring(0,4);

            return date + " " + month + " " + year;
        }
        private static string dateToShortDesc(DateTime dt)
        {
            var month = monthToShortDesc(dt.Month);
            var year = dt.ToString("yy");

            return month + " " + year;
        }
    }
}
